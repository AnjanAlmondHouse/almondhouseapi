const statusTrue = true;
const statusFalse = false;
const systemError = "System error. Please contact your administrator"
const successStatusCode = 200;
const errorStatusCode = 500;
const keyMissStatusCode = 400;
const existsStatusCode = 409
const invalidStatusCode = 401;
const invalidUsername = "Invalid Username"
const invalidPassword = "Invalid Password";
const message = "message";
const statusCode = "statusCode"
const missingKeys = {
    user_name       : "Please enter a valid username.",
    password        : "Please enter a valid password.",
    confirm_password: "Your password and confirmation password do not match.",
    user_mobile     : "Please enter a valid mobile number.",
    user_email      : "Please enter a valid email address.",
    user_password   : "Please enter a valid password.",
    missingHeaders  : "Token or USER ID is missing in headers",
    olduser_password: "Old password does not matched.",
    emailormobile   : "Please Enter a valid email or mobile number",
    tokenexpired    : "Link or Token has expired"

}
const errorMsg = {
    user_exists             : "A user with this email or phone number already exists.",
    user_review             : "Uh oh! Looks like you have already reviewed this product.",
    token_expired           : "Your session has expired. Please login again.",
    productExistsInWishList : "This product already exists in your wishlist.",
    noProductFound          : "Oh no! Product not found.",
    noCartFound             : "Oh no! Cart not found.",
    noItemsFound            : "Oops! No such products found.",
    cartProductsNotFound    : "All products not found",
    couponExpired           : "Oh no! Your coupon has expired.",
    noCoupons               : "Oh no! Coupon not found.",
    minCartValue            : "Minimum cart value of INR 100 is required to apply the coupon.",
    couponAlreadyApplied    : "Oh no! You have already applied the coupon.",
    noCouponApplied         : "Oops! You have not applied any coupon.",
    noOrdersFound           : "Your order history looks empty!",
    notDeliverable          : "are not deliverable to the selected location",
    noAddrSelected          : "Address not selected",
    paymentTotalErr         : "Actual amount is differ from amount to be paid",
    addrNotFound            : "No address found",
    emailExistsInSubscribers: "An account with this mail already exists. Please use a different email id.",
    userNotFound            : "User not found",
    userProductsNotFound    : "Cannot review without purchasing the product",
    userEmailExist          : "User email already exist",
    userMobileExist         : "User mobile number already exist",
    userEmailMobileExist    : "User email & mobile number already exist",
    emailnotFound           : "Email address not found.",
    noPackageFound          : "Oh no! Package not found.",
    orderNotFound           : "No orders found",
    cantCancelOrder         : "Can't cancel item once order is shipped",
    cantReturnOrder         : "Cannot return undelivered order"

}
const successMsg = {
    addToWishList   : "Product has been added to your wishlist.",
    addToCart       : "Product has been added to your cart.",
    updateCart      : "Your cart has been updated.",
    wishListFound   : "Your wishlist products have been found.",
    removedWishlist : "Product has been removed from your wishlist.",
    cartFound       : "Cart products found",
    removeCart      : "Successfully removed products from your cart.",
    couponApplied   : "Hurray! Coupon applied successfully!",
    couponRemoved   : "Coupon has been removed",
    proceedToPay    : "Payment details matched, Proceed to pay",
    deliveryAddrAssigned : "Successfully assigned delivery address",
    paymentSuccess  : "Payment is successful",
    packagecartFound     : " Package Cart products found",
    suggestedProFound     : "Suggested products found",
    subscribeSuccess  : "Thank You, We will keep in touch with you.",
    userDetails     : "User Details updated",
    userdetailsSuccess : "Successfully updated user details",
    suggestedCartProFound     : "Suggested cart products are found",
    removeddeliveryaddress :   "User Delivery Address has been removed",
    changePassowrd  : "Your password has been changed successfully.use your new password to login",
    PassowrdResetLink  : "We have e-mailed your password reset link",
    removePackageCart      : "Successfully removed products from your Package.",
    orderItemCancelled     : "Successfully cancelled the product",
    orderCancelled         : "Successfully cancelled the order",
    newProductsFound : "New arrivals found",
    featuredProducts : "Featured products found",
    countriesFound : "Countries found"

}
const systemErrorCallback = {result:{"status":statusFalse, statusCode:errorStatusCode, message:systemError}}
const invalidCredentialsCallback = {result:{"status":statusFalse, statusCode:invalidStatusCode}};
const tokenExpiredcallback ={result: {"status":statusFalse, statusCode:keyMissStatusCode, message:"Token Expired"}}
const successCallback = {"status":statusTrue, "statusCode":successStatusCode};
const keyMissCallBack = {result:{"status":statusFalse, statusCode:keyMissStatusCode}};
const resultCallbacks = {
    existsCallBack      : {"status":statusFalse, statusCode:existsStatusCode},
    systemErrorCallback : {"status":statusFalse, statusCode:errorStatusCode, message:systemError},
    invalidCredentialsCallback : {"status":statusFalse, statusCode:invalidStatusCode}
}
const errorCallbacks = {
    headerError             : {result:{"status":statusFalse, statusCode:keyMissStatusCode, message:missingKeys.missingHeaders}},
    tokenExpiredcallback    : {result:{"status":statusFalse, statusCode:keyMissStatusCode, message:errorMsg.token_expired}}
}
const keyMissingCallback = {result:{"status":statusFalse, "statusCode":keyMissStatusCode}}
const successMsgCallback = {"status":statusTrue, "statusCode":successStatusCode}
module.exports =  {systemErrorCallback,statusTrue,statusFalse,systemError,successStatusCode,errorStatusCode,
                    keyMissStatusCode,invalidStatusCode,invalidCredentialsCallback,invalidUsername ,invalidPassword,
                    tokenExpiredcallback,successCallback,keyMissCallBack,missingKeys,message,statusCode,errorMsg,
                    resultCallbacks, keyMissingCallback,successMsgCallback,errorCallbacks,successMsg,existsStatusCode}




