addToCartValidation = (requestData,cb)=>{
    if(requestData.product_id && requestData.user_id && requestData.user_name 
        && requestData.product_name && requestData.product_slug && requestData.ordered_quantity
         && requestData.uom && requestData.price && requestData.discount_percentage){
            cb(null, true)
    }else{
        let missing_key;
        if(!requestData.product_id)
            missing_key = "product_id"
        if(!requestData.user_id)
            missing_key = "user_id"
        if(!requestData.user_name)
            missing_key = "user_name"
        if(!requestData.product_name)
            missing_key = "product_name"
        if(!requestData.product_slug)
            missing_key = "product_slug"
        if(!requestData.ordered_quantity)
            missing_key = "ordered_quantity"
        if(!requestData.uom)
            missing_key = "uom"
        if(!requestData.price)
            missing_key = "price"
        if(!requestData.discount_percentage)
            missing_key = "discount_percentage"
        // let message = missing_key + " is missing"
        let message = "Oops something went wrong, please refresh your page.";
        cb(message,null)
        
    }
}

validations = (requestData,schema, cb)=>{
    let keys = Object.keys(requestData)
    let temp_array = [];
    for(var i in schema ){
        if(!keys.includes(schema[i]) || requestData[schema[i]] == null || requestData[schema[i]] == undefined )
            temp_array.push(schema[i].toUpperCase().replace(/_/g, " "))
    }
    if(temp_array.length > 0){
        let adverb = temp_array.length > 1 ? " are": " is"
        // let message = temp_array + adverb + " missing"
        let message = "Oops something went wrong, please refresh your page.";
        cb(message)
    }else
        cb(null, true)
}
// const Joi = require('@hapi/joi');
// const objectSchema = Joi.object({
//     source: Joi.string().min(1).required(),
//     path: Joi.string().min(1).required()
//   }).required();
  
//   const arraySchema = Joi.array().items(objectSchema).min(1).unique()
//   .required()
// const schema = Joi.object({
//     username: Joi.string()
//         .alphanum()
//         .min(3)
//         .max(30)
//         .required(),

//     password: Joi.string()
//         .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

//     repeat_password: Joi.ref('password'),

//     access_token: Joi.alternatives().try(objectSchema, arraySchema).required()
//     ,

//     birth_year: Joi.number()
//         .integer()
//         .min(1900)
//         .max(2013),

//     email: Joi.string()
//         .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
// })
//     .with('username', 'birth_year')
//     .xor('password', 'access_token')
//     .with('password', 'repeat_password');


// let res = schema.validate({username:"sai12",birth_year:1994, access_token:[{source:"sai",path:"sai"},{source:"si",path:"sai"}]  });
// let {value, error} = res

// console.log(JSON.stringify(res))
//   if (!error) { 
//     console.log("success")
//   } else { 
//     const { details } = error; 
//       console.log(details,"--------->")
//     const message = details.map(i => i.message).join(',');
//     console.log("error", message); 
//  }   
// -> { value: { username: 'abc', birth_year: 1994 } }

// schema.validate({});
// -> { value: {}, error: '"username" is required' }

// Also -

// try {
//     const value = await schema.validateAsync({ username: 'abc', birth_year: 1994 });
// }
// catch (err) { }
validateheaders = (requestData,schema, cb)=>{
    let keys = Object.keys(requestData)
    let temp_array = [];
    for(var i in schema ){
        if(!keys.includes(schema[i]) || requestData[schema[i]] == null || requestData[schema[i]] == undefined )
            temp_array.push(schema[i].toUpperCase().replace(/_/g, " "))
    }       
    if(temp_array.length > 0){
        let adverb = temp_array.length > 1 ? " are": " is"
        // let message = temp_array + adverb + " missing in headers"
        let message = "Oops something went wrong, please refresh your page.";
        cb(message)
    }else
        cb(null, true)
}

cartValidations = (requestData,schema, cb)=>{
    let temp_array = [];
    if(requestData.length > 0){
        for(var index in requestData){
            let keys = Object.keys(requestData[index])
            for(var i in schema ){
                if(!keys.includes(schema[i]) || requestData[index][schema[i]] == null || requestData[index][schema[i]] == undefined )
                    temp_array.push(schema[i].toUpperCase().replace(/_/g, " "))
            }       
        }
        if(temp_array.length > 0){
            console.log(temp_array)
            let adverb = temp_array.length > 1 ? " are": " is"
            // let message = temp_array + adverb + " missing"
            let message = "Oops something went wrong, please refresh your page.";
            cb(message)
        }else
            cb(null, true)
    }else{
        cb("Empty array")
    }
}

module .exports = {addToCartValidation,validations,validateheaders,cartValidations}
