const jwt = require('jsonwebtoken');
const JWT_SECRET='addjsonwebtokensecretherelikeQuiscustodietipsoscustodes';
const options = { expiresIn: '60d', issuer: 'https://vasu.life' };
var bcrypt;
let app = require('../server/server');
var db = app.datasources.postgresql.connector;
try {
    // Try the native module first
    bcrypt = require('bcrypt');
    // Browserify returns an empty object
    if (bcrypt && typeof bcrypt.compare !== 'function') {
      bcrypt = require('bcryptjs');
    }
  } catch (err) {
    // Fall back to pure JS impl
    bcrypt = require('bcryptjs');
  };
hashPassword = (password, cb)=>{
  bcrypt.genSalt(10, function(err, salt) {
      if (err)
        cb(err);
      bcrypt.hash(password, salt, function(err, hash) {
          if(err)
          cb(err,{message:"error occured",password:''});
          else
          cb(null, {message: "successful", password:hash})
      })
  })
}

bcryptCompare = (userPassword, hashPassword)=>{
    bcrypt.compare(userPassword, hashPassword)
    .then(function(res) {
        return res
    })
    .catch((resp)=>{
        return false
    })
}

function verifyToken(token, callback){
  jwt.verify(token, JWT_SECRET, options, function(err,res){
      callback(err,res)
  })
}
const token = (payload)=>{
  return jwt.sign(payload, JWT_SECRET, options);
}

function jsonCopy(data){
  try {
    return JSON.parse(JSON.stringify(data))
  } catch (error) {
    
  }
  return data
}

function response(status, statusCode, message){
  return ({result:{status, statusCode,message}})
}

const toFloat = (number)=>{ return Number(Number(number).toFixed(2))}

var moment = require('moment');
var time = moment();
const time_format = time.format('YYYY-MM-DD HH:mm:ss Z');

const utsTime = (timestamp)=>{
  if(timestamp){
    console.log(new Date(timestamp))
    return new Date(timestamp)
  }else{
    return new Date()
  }
}

sendEmail = ( emailInfo, emailDesign, cb)=>{
  let Email = app.models.email;
  Email.send({
      to: emailInfo.to_email,
      from: "info@vedik.com",
      subject: emailInfo.subject,
      html: emailDesign,
  },
  async function (err, result) {
    console.log(err, await result,"err, result");
    
      if(err) {
          console.log('[!] Error', err);
          cb(err, null)
      }else
          cb(null, result)
  });
}

triggerEmail = (emailTriggerPoint, emailInfo, emailDesign, cb)=>{
  let TriggerEmail = app.models.email_triggers;
  TriggerEmail.findOne({where:{trigger_point:emailTriggerPoint }})
  .then((found)=>{
    console.log(found);
    
    if(found){
      sendEmail(emailInfo, emailDesign,function(mailErr, mailSuccess) {
        if(mailErr){
          cb(mailErr, null)
        }else{
          cb(null, "Mail Sent")
        }
      })
    }else{
      cb(null, "mail not send")
    }
  })
  .catch(err=>{
    console.log(err,"err")
    cb(err, null)
  })
}

module.exports= {hashPassword,bcryptCompare,bcrypt,JWT_SECRET,options,verifyToken,token,jsonCopy,response,toFloat,time_format,utsTime,sendEmail,triggerEmail}