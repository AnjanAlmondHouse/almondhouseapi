'use strict';
let messages = require('../messages');
module.exports = function(Countries) {
    Countries.beforeRemote('getCountries',function(ctx, result, next){
        Countries.find({where:{is_active:1},
            fields:['country_name',"_id","country_code","country_slug","country_currency","currency"]})
        .then(countries=>{
            let result = {
                status:true,
                status_code :messages.successStatusCode,
                message: messages.successMsg.countriesFound,
                countries : countries
            }
            ctx.res.json({result})
        })
        .catch(error=>{
            console.log(error,"error")
            ctx.res.json(messages.systemErrorCallback)
        })
    });
    Countries.remoteMethod("getCountries",{
        http:{path: '/getCountries', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Countries list in json format',
        returns:{arg:"result", type:"object" }
    });
};
