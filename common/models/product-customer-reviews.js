'use strict';
let messages = require('../messages');
let { verifyToken, response } = require('../utils')
let app = require('../../server/server')
module.exports = function (Productcustomerreviews) {
    //* Write Product Customer Review Start *//
    Productcustomerreviews.remoteMethod("writeProductcustomerreview", {
        http: { path: '/writeProductcustomerreview', verb: "post" },
        accepts: [
            { arg: "Writeproductreview", type: "object", http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Write Product Customer Review in json format',
        returns: { arg: "result", type: "object" }
    });
    Productcustomerreviews.beforeRemote('writeProductcustomerreview', function (ctx, result, next) {
        let schema = ["products_id", 'products_name', 'products_slug', 'user_name', 'user_email', "user_id", "rating", "user_review", "review_title"];
        let requestData = ctx.req.body
        let headerSchema = ['token', 'userid']
        validateheaders(ctx.req.headers, headerSchema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        messages.tokenExpiredcallback['error'] = err
                        ctx.res.json(messages.tokenExpiredcallback)
                    } else {
                        validations(ctx.req.body, schema, (err, res) => {
                            if (err) {
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }
                            else {
                                let Products = app.models.products
                                Products.findOne({
                                    where: {
                                        product_slug: requestData.products_slug,
                                    }
                                })
                                .then((productResult) => {
                                    if (!productResult){
                                        ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.noItemsFound))
                                    }else{
                                        let Users = app.models.users
                                        Users.findOne({
                                            where:{"id":requestData.user_id},
                                            fields:['user_id',"user_name","user_email",'user_first_name','user_last_name','user_gender','user_dob',"user_mobile"]
                                        })
                                        .then(function(updateduserdetails){
                                            if(updateduserdetails != null){
                                                let Transcationitems = app.models.transcation_items
                                                Transcationitems.findOne({
                                                    where:{"users_id":requestData.user_id, "product_slug" : requestData.products_slug, "master_status_name" :"delivered"},
                                                })
                                                .then(function(transactionitems){
                                                    if(transactionitems){
                                                        next()
                                                    }else{
                                                        ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.userProductsNotFound))
                                                    }
                                                })
                                                .catch(function (error) {
                                                    ctx.res.json(response(false, messages.errorStatusCode,messages.systemError))
                                                })

                                            }else{
                                                ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.userNotFound))
                                            }
                                        })
                                        .catch(function (error) {
                                            ctx.res.json(response(false, messages.errorStatusCode,messages.systemError))
                                        })
                                    }
                                })
                                .catch((error) => {
                                    console.log(error)
                                    ctx.res.json(response(false, messages.errorStatusCode, messages.systemError))
                                })
                            }
                        })
                    }
                })
            }
        })
    });
    Productcustomerreviews.writeProductcustomerreview = function (Writeproductreview, req, res, cb) {
        Productcustomerreviews.findOrCreate({
            where: { "user_id": Writeproductreview.user_id, "products_slug": Writeproductreview.products_slug, "is_active": 1 }
        },
            {
                "products_id": Writeproductreview.products_id,
                "products_name": Writeproductreview.products_name,
                "products_slug": Writeproductreview.products_slug,
                "rating": Writeproductreview.rating,
                "review_title": Writeproductreview.review_title,
                "user_review": Writeproductreview.user_review,
                "user_id": Writeproductreview.user_id,
                "user_name": Writeproductreview.user_name,
                "user_email": Writeproductreview.user_email,
                "is_active": 1
            }
        )
            .then(function ([addreview, created]) {
                if (created) {
                    Productcustomerreviews.find({
                        fields: ['products_slug', 'user_id', "rating"],
                        where: {
                            is_active: 1,
                            "products_slug": Writeproductreview.products_slug
                        }
                    })
                        .then((allreviews) => {
                            SumuserReviews(allreviews, Writeproductreview.products_slug, function (err, deliveryResp) {
                                if (err) {
                                    res.json(response(false, messages.invalidStatusCode, err))
                                } else {
                                    let result = { "status": true, "statusCode": messages.successStatusCode };
                                    result["message"] = "Review Added Successfully"
                                    cb(null, result)
                                }
                            })
                        })
                        .catch((error) => {
                            console.log(error)
                            ctx.res.json(response(false, messages.errorStatusCode, messages.systemError))
                        })
                } else {
                    messages.resultCallbacks.existsCallBack[messages.message] = messages.errorMsg.user_review;
                    cb(null, messages.resultCallbacks.existsCallBack)
                }
            })
            .catch(function (error) {
                console.log(error)
                cb(null, messages.systemErrorCallback)
            })
    }
    //* Write Product Customer Review End *//
    //* Show Product Customer Reviews Start *//
    Productcustomerreviews.remoteMethod("getProductcustomerreviews", {
        http: { path: '/getProductcustomerreviews', verb: "get" },
        accepts: [
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Product Customer Reviews in json format',
        returns: { arg: "result", type: "object" }
    });
    Productcustomerreviews.beforeRemote('getProductcustomerreviews', function (ctx, result, next) {
        let schema = ['products_slug'];
        validations(ctx.req.query, schema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                if (!ctx.req.query.limit) {
                    ctx.req.query.limit = 20
                }
                next();
            }
        })
    });
    Productcustomerreviews.getProductcustomerreviews = function (req, res, cb) {
        Productcustomerreviews.find({
            where: { "products_slug": req.query.products_slug, is_active: 1 },
            fields: ['products_slug', 'products_name', 'user_id', 'user_name', 'user_email', "review_title", "rating", "user_review", "created_at"],
            limit: req.query.limit, order: 'created_at DESC'
        })
            .then(function (reviews) {
                if (reviews.length > 0) {
                    var newArray = reviews.reduce(function (acc, curr) {
                        var findIfNameExist = acc.findIndex(function (item) {
                            return item.products_slug === curr.products_slug;
                        })
                        var data = {
                            "user_id": curr.user_id,
                            "user_name": curr.user_name,
                            "user_email": curr.user_email,
                            "review_title": curr.review_title,
                            "rating": curr.rating,
                            "user_review": curr.user_review,
                            "created_at": curr.created_at
                        }
                        if (findIfNameExist === -1) {
                            let obj = {
                                'products_slug': curr.products_slug,
                                'products_name': curr.products_name,
                                "user_reviews": [data]
                            }
                            acc.push(obj)
                        } else {
                            acc[findIfNameExist].user_reviews.push(data)
                        }
                        return acc;
                    }, []);
                    let result = { "status": true, "statusCode": messages.successStatusCode };
                    result["message"] = `Reviews found for ${newArray[0].products_name}`
                    result["reviews"] = newArray[0]
                    cb(null, result)
                }
                else {
                    let result = { "status": true, "statusCode": messages.successStatusCode };
                    result["message"] = `No Reviews Found`
                    result["reviews"] = {}
                    cb(null, result)
                }
            })
            .catch(function (error) {
                console.log(error)
                cb(null, messages.systemErrorCallback)
            })
    }
    //* Show Product Customer Reviews End *//
    function SumuserReviews(allreviews, product_slud, cb) {
        const reviewavg = (allreviews.reduce((a, b) => a + Number(b.rating), 0) / allreviews.length).toFixed(1)
        console.log(reviewavg, product_slud)
        let Products = app.models.products
        Products.update(
            { "product_slug": product_slud }, {
            "rating": reviewavg
        })
            .then(function (addrating) {
                cb(null, addrating)
            })
            .catch(function (error) {
                console.log(error)
                cb(null, messages.systemErrorCallback)
            })
    }
};
