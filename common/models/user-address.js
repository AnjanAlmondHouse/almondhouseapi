'use strict';
let messages = require('../messages');
let { verifyToken, response } = require('../utils')
let app = require("../../server/server");
module.exports = function (Useraddress) {
    //* Add User Address Start //*
    Useraddress.remoteMethod("addUseraddress", {
        http: { path: '/addUseraddress', verb: "post" },
        accepts: [
            { arg: "addaddress", type: "object", http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add User Address in json format',
        returns: { arg: "result", type: "object" }
    });
    Useraddress.beforeRemote('addUseraddress', function (ctx, result, next) {
        // let headerSchema = ['userid']
        // validateheaders(ctx.req.headers, headerSchema, (err, res) => {
        //     if (err) {
        //         messages.keyMissCallBack.result['message'] = err;
        //         ctx.res.json(messages.keyMissCallBack)
        //     }
        //     else {
        //         verifyToken(ctx.req.headers.token, function (err, res) {
        //             if (err) {
        //                 messages.tokenExpiredcallback['error'] = err
        //                 ctx.res.json(messages.tokenExpiredcallback)
        //             } else {
            console.log(ctx.req.body)
                        let schema = ["user_name", "address", "mobile", "landmark", 'address_name', 'city', 'state', 'country', 'address_type', 'is_primary',"first_name"];
                        validations(ctx.req.body, schema, (err, res) => {
                            if (err) {
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }
                            else {
                                if (ctx.req.body.is_guest_user) {
                                    let Users = app.models.users
                                    Users.findOne({ where: { or:[{user_email: ctx.req.body.email },{user_mobile: ctx.req.body.guest_mobile}]}} )
                                    .then((userResult) => {
                                        if (userResult) {
                                            return ctx.res.json(response(false, 400, "User already exists on this email or phone number please login and place order"))
                                        }
                                        next();
                                    })
                                    .catch(usersErr=>{
                                        console.log("usersErr",usersErr);
                                        ctx.res.json(messages.systemErrorCallback)
                                    })
                                }else{
                                    next();
                                }
                                
                            }
                        })
                //     }
                // })
        //     }
        // })
    });
    Useraddress.addUseraddress = function (addaddress, req, res, cb) {
        console.log(addaddress,"SelectAddress")
        console.log(typeof(addaddress.user_id),typeof(addaddress.user_id) == 'string')
        var userId = addaddress.user_id
        Useraddress.findOne({
            where:{
                "user_id": userId,
                "address_name": addaddress.address_name
            }
        })
        .then(checkaddrResp=>{
            if(checkaddrResp){
                let update_address = {
                    "user_name": addaddress.user_name,
                    "address": addaddress.address,
                    "mobile": addaddress.mobile,
                    "landmark": addaddress.landmark,
                    "address_name": addaddress.address_name,
                    "city": addaddress.city,
                    "state": addaddress.state,
                    "country": addaddress.country,
                    "address_type": addaddress.address_type,
                    "is_primary": addaddress.is_primary,
                    "alternative_mobile": addaddress.alternative_mobile,
                    "zip_code": addaddress.pincode,
                    "first_name":addaddress.first_name,
                    "last_name":addaddress.last_name,
                    "email":addaddress.email
                }
                Useraddress.update({
                    id : checkaddrResp.id
                },update_address)
                    .then(function (adduseraddress) {
                        update_address['id'] = checkaddrResp.id
                        update_address['address_id'] = checkaddrResp.id;
                        let result = { "status": true, "statusCode": messages.successStatusCode, "message" : "Address Saved","user_address":update_address }
                        if(req.body.is_primary == 1){
                            updataIsprimary(checkaddrResp.id, userId, function (err, is_primary) {
                                if (err) {
                                    res.json(response(false, messages.systemErrorCallback, err))
                                } else {
                                    console.log("1st resp");
                                    
                                    cb(null, result)
                                }
                            })
                        }else{
                            cb(null, result)
                        } 
                    })
                    .catch(function (error) {
                        console.log(error)
                        cb(null, messages.systemErrorCallback)
                    })
            }
            else{
                Useraddress.create({
                    "user_id": userId,
                    "user_name": addaddress.user_name,
                    "address": addaddress.address,
                    "mobile": addaddress.mobile,
                    "landmark": addaddress.landmark,
                    "address_name": addaddress.address_name,
                    "city": addaddress.city,
                    "state": addaddress.state,
                    "country": addaddress.country,
                    "address_type": addaddress.address_type,
                    "is_primary": addaddress.is_primary,
                    "alternative_mobile": addaddress.alternative_mobile,
                    "is_active": 1,
                    "zip_code": addaddress.pincode,
                    "first_name":addaddress.first_name,
                    "last_name":addaddress.last_name,
                    "email":addaddress.email
                })
                    .then(function (adduseraddress) {
                        console.log("created");
                        
                        adduseraddress['address_id'] = adduseraddress['id'];
                        console.log(adduseraddress,"adduseraddress");
                        
                        let result = { "status": true, "statusCode": messages.successStatusCode, "message" : "Address Saved","user_address":adduseraddress }
                        if(req.body.is_primary == 1){
                            updataIsprimary(adduseraddress.id, userId, function (err, is_primary) {
                                if (err) {
                                    res.json(response(false, messages.systemErrorCallback, err))
                                } else {
                                    console.log("2nd response");
                                    
                                    cb(null, result)
                                }
                            })
                        }else{
                            cb(null, result)
                        } 
                    })
                    .catch(function (error) {
                        console.log(error)
                        cb(null, messages.systemErrorCallback)
                    })
            }
        })
    }
    //* Add User Address End //*

    //* Edit User Address Start //*
    Useraddress.remoteMethod("editUseraddress", {
        http: { path: '/editUseraddress', verb: "post" },
        accepts: [
            { arg: "editaddress", type: "object", http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add User Address in json format',
        returns: { arg: "result", type: "object" }
    });
    Useraddress.beforeRemote('editUseraddress', function (ctx, result, next) {
        let headerSchema = ['token', 'userid']
        validateheaders(ctx.req.headers, headerSchema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        messages.tokenExpiredcallback['error'] = err
                        ctx.res.json(messages.tokenExpiredcallback)
                    } else {
                        let schema = ["address_id", "user_id", "address", "mobile", "landmark", 'address_name', 'city', 'state', 'country', 'address_type', 'is_primary'];
                        validations(ctx.req.body, schema, (err, res) => {
                            if (err) {
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }
                            else {
                                next();
                            }
                        })
                    }
                })
            }
        })
    });
    Useraddress.editUseraddress = function (editaddress, req, res, cb) {
        console.log(JSON.stringify(editaddress));
        
        Useraddress.update(
            { "id": editaddress.address_id, "user_id": editaddress.user_id }, {
            is_active : 0
        })
            .then(function (adduseraddress) {
                Useraddress.create({
                "user_id" : editaddress.user_id,
                "user_name": editaddress.user_name,
                "address": editaddress.address,
                "mobile": editaddress.mobile,
                "landmark": editaddress.landmark,
                "address_name": editaddress.address_name,
                "city": editaddress.city,
                "state": editaddress.state,
                "country": editaddress.country,
                "address_type": editaddress.address_type,
                "is_primary": editaddress.is_primary,
                "alternative_mobile": editaddress.alternative_mobile,
                "email" : editaddress.email,
                "first_name": editaddress.first_name,
                "last_name": editaddress.last_name,
                "is_active":1,
                "zip_code": editaddress.pincode})

                .then((inputResp)=>{
                    console.log(inputResp,"inputResp");
                    
                    let result = { "status": true, "statusCode": messages.successStatusCode, "message" : "Address Edited Successfully" }
                    if(req.body.is_primary == 1){
                        updataIsprimary(inputResp.id, req.body.user_id, function (err, is_primary) {
                            if (err) {
                                res.json(response(false, messages.systemErrorCallback, err))
                            } else {
                                cb(null, result)
                            }
                        })
                    }else{
                        cb(null, result)
                    }
                })
                .catch((inputErr)=>{
                    cb(null, messages.systemErrorCallback)
                })
                    
            })
            .catch(function (error) {
                console.log(error)
                cb(null, messages.systemErrorCallback)
            })
    }
    //* Edit User Address End //*

    //* Get All User Address Start //*
    Useraddress.remoteMethod("getUseraddress", {
        http: { path: '/getUseraddress', verb: "get" },
        accepts: [
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add User Address in json format',
        returns: { arg: "result", type: "object" }
    });
    Useraddress.beforeRemote('getUseraddress', function (ctx, result, next) {
        let headerSchema = ['token', 'userid']
        validateheaders(ctx.req.headers, headerSchema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        messages.tokenExpiredcallback['error'] = err
                        ctx.res.json(messages.tokenExpiredcallback)
                    } else {
                        let schema = ["user_id"];
                        validations(ctx.req.query, schema, (err, res) => {
                            if (err) {
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }
                            else {
                                next();
                            }
                        })
                    }
                })
            }
        })
    });
    Useraddress.getUseraddress = function (req, res, cb) {
        Useraddress.find({
            where: { "user_id": req.query.user_id, is_active: 1 },
            fields: ["address_id","zip_code", "user_name", "user_id", "address", "mobile", "landmark", 'address_name', 'city', 'state', 'country', 'address_type', 'is_primary', "alternative_mobile","first_name","last_name","email"]
        })
            .then(function (userdetails) {
                if (userdetails.length > 0) {
                    var userArray = userdetails.reduce(function (acc, curr) {
                        var findIfNameExist = acc.findIndex(function (item) {
                            return item.user_id === curr.user_id;
                        })
                        var data = {
                            "address_id": curr.address_id,
                            "user_name": curr.user_name,
                            "address": curr.address,
                            "mobile": curr.mobile,
                            "landmark": curr.landmark,
                            "address_name": curr.address_name,
                            "city": curr.city,
                            "state": curr.state,
                            "country": curr.country,
                            "address_type": curr.address_type,
                            "is_primary": curr.is_primary,
                            "alternative_mobile": curr.alternative_mobile,
                            "first_name":curr.first_name,
                            "last_name":curr.last_name,
                            "email":curr.email,
                            "zip_code": curr.zip_code
                        }
                        if (findIfNameExist === -1) {
                            let obj = {
                                'user_id': curr.user_id,
                                'user_name': curr.user_name,
                                "address_list": [data]
                            }
                            acc.push(obj)
                        } else {
                            acc[findIfNameExist].address_list.push(data)
                        }
                        return acc;
                    }, []);
                    let result = { "status": true, "statusCode": messages.successStatusCode };
                    result["message"] = `${userdetails[0].user_name}! Your Address are found`
                    result["User"] = userArray[0]
                    cb(null, result)
                }
                else {
                    cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.errorMsg.addrNotFound, User: {} })
                }
            })
            .catch(function (error) {
                console.log(error)
                cb(null, messages.systemErrorCallback)
            })
    }
    //* Get All User Address End //*

    //* Delete User Delivery Address Start //*
    Useraddress.remoteMethod("removedeliveryAddress",{
        http:{path: '/removedeliveryAddress', verb:"post"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Remove User Delivery Address json format',
        returns:{arg:"result", type:"object" }
    });
    Useraddress.beforeRemote('removedeliveryAddress', function(ctx, result, next){
        let headerSchema = ['token', 'userid']
        validateheaders(ctx.req.headers, headerSchema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        messages.tokenExpiredcallback['error'] = err
                        ctx.res.json(messages.tokenExpiredcallback)
                    } else {
                        let schema = ["user_address_id","user_id"];
                        validations(ctx.req.body, schema, (err, res) => {
                            if (err) {
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }
                            else {
                                next();
                            }
                        })
                    }
                })
            }
        })    
    });
    Useraddress.removedeliveryAddress = function(req, res, cb){
        Useraddress.update({"id":req.body.user_address_id,"user_id" : req.body.user_id},{is_active: 0})
        .then((result)=>{
            console.log(result,"address deleted");
            
            res.json(response(true, messages.successStatusCode,messages.successMsg.removeddeliveryaddress))
        })
        .catch((error)=>{
            res.json(response(false, messages.errorStatusCode, "Address already in use"))
        })
     }
    //* Delete User Delivery Address End //*
    function updataIsprimary(id, userid, cb) {
        let Useraddress = app.models.user_address;
        Useraddress.findOne({
            where: {
                id: id,
                is_active: 1
            }
        })
        .then((addressResp) => {
            if (addressResp) {
                let Cartaddress = app.models.cart_address;
                var data = {
                    "user_name": addressResp.user_name,
                    "address": addressResp.address,
                    "mobile": addressResp.mobile,
                    "landmark": addressResp.landmark,
                    "address_name": addressResp.address_name,
                    "city": addressResp.city,
                    "state": addressResp.state,
                    "country": addressResp.country,
                    "address_type": addressResp.address_type,
                    "is_active": addressResp.is_active,
                    "is_primary": addressResp.is_primary,
                    "alternative_mobile": addressResp.alternative_mobile,
                    "first_name":addressResp.first_name,
                    "last_name":addressResp.last_name,
                    "email":addressResp.email,
                    "zip_code": addressResp.zip_code
                }
                Cartaddress.create(data)
                .then((cartAddressresp)=>{
                    if(cartAddressresp){
                        let Usercart = app.models.user_cart
                        Usercart.update({ users_id: userid, is_active: 1 }, { user_address_id: cartAddressresp.id })
                        .then(cartAddUpdate => {
                            let Useraddress = app.models.user_address
                            Useraddress.update({
                                "user_id":  userid, "id": { neq: id}}, {
                                "is_primary": 0,
                            })
                            .then(function (userdetails) {
                                cb(null, userdetails)
                            })
                            .catch(function (error) {
                                console.log(error,"----")
                                cb(messages.systemErrorCallback, null)
                            })
                        })
                        .catch(cartAddErr => {
                            console.log(cartAddErr,"cartAddErr");
                            cb(messages.systemErrorCallback, null)
                        })
                    }
                    else{
                        cb(messages.errorMsg.addrNotFound, null)
                    }
                })
                .catch((cartAddresserr) => {
                    cb(cartAddresserr, null)
                })
            } else {
                cb(messages.errorMsg.addrNotFound, null)
            }
        })
        .catch((addressErr) => {
            cb(addressErr, null)
        })
    }


    //* Edit User address phone number start //*

    Useraddress.beforeRemote('editAddressPhoneNumber', function (ctx, result, next) {
        let headerSchema = ['token', 'userid']
        validateheaders(ctx.req.headers, headerSchema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        messages.tokenExpiredcallback['error'] = err
                        ctx.res.json(messages.tokenExpiredcallback)
                    } else {
                        let schema = ["mobile","address_id","user_id"];
                        validations(ctx.req.body, schema, (err, res) => {
                            if (err) {
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }
                            else {
                                next();
                            }
                        })
                    }
                })
            }
        })
    });
    Useraddress.editAddressPhoneNumber = function (editaddress, req, res, cb) {
        Useraddress.update(
            { "id": editaddress.address_id, "user_id": editaddress.user_id }, {
            "mobile": editaddress.mobile})
            .then(function (adduseraddress) {
                let result = { "status": true, "statusCode": messages.successStatusCode, "message" : "Mobile number Edited Successfully" }
                cb(null, result)
            })
            .catch(function (error) {
                console.log(error)
                cb(null, messages.systemErrorCallback)
            })
    }

    Useraddress.remoteMethod("editAddressPhoneNumber", {
        http: { path: '/editAddressPhoneNumber', verb: "post" },
        accepts: [
            { arg: "editaddress", type: "object", http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add User Address in json format',
        returns: { arg: "result", type: "object" }
    });

    //* Edit User address phone number end //*

    // check user exists
    Useraddress.beforeRemote('checkUser', function (ctx, result, next) {
        console.log(ctx.req.body)
        let schema = ["email","mobile","user_name"];
        validations(ctx.req.body, schema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                let Users = app.models.users
                Users.findOne({ where: { or:[{user_email: ctx.req.body.email },{user_mobile: ctx.req.body.mobile}]}} )
                .then((userResult) => {
                    if (userResult) {
                        return ctx.res.json(response(false, 400, "User already exists on this email or phone number please login and place order"))
                    }
                    else{
                        Users.create({
                            user_name: ctx.req.body.user_name,
                            user_email: ctx.req.body.mobile,
                            user_mobile: ctx.req.body.email,
                        })
                        .then(userResp=>{
                            console.log(userResp,"userResp");
                            ctx.res.json(response(true, 200, "No user found"))
                        })
                        .catch(userInsErr=>{
                            console.log(userInsErr,"userInsErr");
                            ctx.res.json(messages.systemErrorCallback)
                        })
                    }
                })
                .catch((userErr) => {
                    console.log("userErr",userErr);
                    return ctx.res.json(messages.systemErrorCallback)
                })
            }
        })
    });
    Useraddress.remoteMethod("checkUser", {
        http: { path: '/checkUser', verb: "post" },
        accepts: [
            { arg: "userData", type: "object", http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        returns: { arg: "result", type: "object" }
    });
};
