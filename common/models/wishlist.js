'use strict';
const {verifyToken,jsonCopy,response} = require("../utils")
const {validations} = require("../validations")
const messages     =   require("../messages")
let {invalidCredentialsCallback,invalidUsername,invalidPassword,tokenExpiredcallback,
    successCallback,keyMissCallBack, missingKeys,message,systemErrorCallback,errorMsg,
    resultCallbacks} = require('../messages')
let app = require('../../server/server');

module.exports = function(Wishlist) {
    //* Add Wishlist //*
    Wishlist.beforeRemote('addToWishlist', function(ctx, result, next){
        let requestData = ctx.req.body
        if(ctx.req.headers.token && ctx.req.headers.userid){
            let schema = ["products_id","user_id","user_name","product_slug","product_name",
            "uom","discount_percentage"]
            verifyToken(ctx.req.headers.token, function(err, res){
                if(err) {
                    ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.token_expired))
                }else{
                    validations(requestData,schema, (err, res)=>{
                        if(err){
                            ctx.res.json(response(false, messages.keyMissStatusCode,err))
                        }
                        else
                            next()
                    })
                }
            })
        }else{
            ctx.res.json(response(false, messages.keyMissStatusCode,messages.missingKeys.missingHeaders))
        }       
    });
    Wishlist.addToWishlist = function(requestData, req, res, cb){
        Wishlist.findOrCreate({where:{'user_id':requestData.user_id,"product_slug":requestData.product_slug}},
                                requestData)
        .then(([wishListResp, created])=>{
            if(!created){
                res.json(response(false,messages.existsStatusCode,messages.errorMsg.productExistsInWishList ))
            }else{
                res.json(jsonCopy(response(true,messages.successStatusCode,messages.successMsg.addToWishList )))
            }
        })
        .catch((wishlistErr)=>{
            cb(null,jsonCopy(messages.systemErrorCallback))
        })
    }
    Wishlist.remoteMethod("addToWishlist",{
        http:{path: '/addToWishlist', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can add items to wishlist if he is logged in',
        returns:{arg:"result", type:"object" }
    });
    //* Add Wishlist //*

    //* User Wishlist //*
    Wishlist.beforeRemote('userWishList', function(ctx, result, next){
        let requestData = ctx.req.query
        if(ctx.req.headers.token && ctx.req.headers.userid){
            let schema = ["user_id"]
            verifyToken(ctx.req.headers.token, function(err, res){
                if(err) {
                    ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.token_expired))
                }else{
                    validations(requestData,schema, (err, res)=>{
                        if(err){
                            ctx.res.json(response(false, messages.keyMissStatusCode,err))
                        }
                        else
                            next()
                    })
                }
            })
        }else{
            ctx.res.json(response(false, messages.keyMissStatusCode,messages.missingKeys.missingHeaders))
        }         
    });
    Wishlist.userWishList = function(user_id, req, res, cb){
       Wishlist.find({where:{user_id:user_id },
                        order: 'created_at DESC',
                      fields:{created_at: false,updated_at:false, products_id:false}},(err, resp)=>{
            if(err)
                res.json(response(false, messages.systemError,messages.successMsg.removedWishlist))
            else{
                cb(null, jsonCopy({status:true, statusCode:messages.successStatusCode,
                                                message:messages.successMsg.wishListFound,wishlist:resp}))
            }
       })
    }
    Wishlist.remoteMethod("userWishList",{
        http:{path: '/userWishList', verb:"get"},
        accepts:[
            { arg: 'user_id', type: 'number', required: true, http:{source: 'query'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can add items to cart if he is logged in',
        returns:{arg:"result", type:"object" }
    });

    //* User Wishlist //*

    //* Remove User Wishlist //*
    Wishlist.beforeRemote('removeItemFromWishlist', function(ctx, result, next){
        let requestData = ctx.req.body
        if(ctx.req.headers.token && ctx.req.headers.userid){
            let schema = ["user_id"]
            verifyToken(ctx.req.headers.token, function(err, res){
                if(err) {
                    ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.token_expired))
                }else{
                    validations(requestData,schema, (err, res)=>{
                        if(err){
                            ctx.res.json(response(false, messages.keyMissStatusCode,err))
                        }
                        else
                            next()
                    })
                }
            })
        }else{
            ctx.res.json(response(false, messages.keyMissStatusCode,messages.missingKeys.missingHeaders))
        }    
    });
    Wishlist.removeItemFromWishlist = function(requestData, req, res, cb){
        Wishlist.destroyAll({"product_slug": requestData.product_slug, user_id: requestData.user_id})
        .then((result)=>{
            console.log(result,"----",requestData.user_id)
            res.json(response(true, messages.successStatusCode,messages.successMsg.removedWishlist))
        })
        .catch((error)=>{
            res.json(response(false, messages.systemError,messages.successMsg.removedWishlist))
        })
     }
    Wishlist.remoteMethod("removeItemFromWishlist",{
        http:{path: '/removeItemFromWishlist', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can add items to cart if he is logged in',
        returns:{arg:"result", type:"object" }
    });
    //* Remove User Wishlist //*

    //* Orders & Wishlist Count Start //*
    Wishlist.remoteMethod("countOrdersWishlist",{
        http:{path: '/countOrdersWishlist', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User orders & user wishlist count json format',
        returns:{arg:"result", type:"object" }
    });
    Wishlist.beforeRemote('countOrdersWishlist', function(ctx, result, next){
        let headerSchema = ['token', 'userid']
        // validateheaders(ctx.req.headers, headerSchema, (err, res) => {
        //     if (err) {
        //         messages.keyMissCallBack.result['message'] = err;
        //         ctx.res.json(messages.keyMissCallBack)
        //     }
        //     else {
        //         verifyToken(ctx.req.headers.token, function (err, res) {
        //             if (err) {
        //                 messages.tokenExpiredcallback['error'] = err
        //                 ctx.res.json(messages.tokenExpiredcallback)
        //             } else { 
            let Users = app.models.users
            Users.findOne({
                where:{id:ctx.req.headers.userid}
            })
            .then(userResp=>{
                if(userResp){
                    ctx.req.query.user_mobile = userResp.user_mobile
                    ctx.req.query.user_email = userResp.user_email
                    console.log(userResp,"userResp");
                    next()
                }else{
                    ctx.res.json(response(false, messages.keyMissStatusCode, "No user found"))
                }
            })
            .catch(userErr=>{
                ctx.res.json(messages.systemErrorCallback)
            }) 
        //             }
        //         }) 
        //     }
        // })
    });
    Wishlist.countOrdersWishlist = function( req, res, cb){
        Wishlist.find({
            where:{"user_id":req.headers.userid},
        })
        .then(function(userwishlist){
            let Finaltranscation = app.models.final_transcation;
            Finaltranscation.find({
                where: { 
                    or:[{"users_id": req.headers.userid},
                    {
                        and:[{"users_id":0},
                        {or:[{email:req.query.user_email},{number : req.query.user_mobile}]}
                        ]}
                    ] },
                include: {
                    "relation": "ordered_items",
                }
            })
            .then(function(orderitems){
                let count = {
                    "wishlist_count" : userwishlist.length,
                    "user_order_count" : orderitems.length,
                    "user_id" : req.headers.userid
                }
                cb(null, {status: true, statusCode: messages.successStatusCode, message: messages.successMsg.userDetails, user_order: count })
            })
            .catch(function (error) {
                console.log(error)
                cb(null,systemErrorCallback)
            })
        })
        .catch(function (error) {
            console.log(error)
            cb(null,systemErrorCallback)
        })
    }
    //* Orders & Wishlist Count End //*
};
