'use strict';
let messages = require('../messages');
let app = require('../../server/server');
let db = app.datasources.mongovedik.connector
let loopback = app.datasources.sendgrid.connector
let {jsonCopy, response} = require("../utils")
let {validations,validateheaders} = require('../validations')
var elasticsearch = require('elasticsearch');		
let client = new elasticsearch.Client({			
    host:'http://localhost:9200/',
    elasticsearchIndices:{
        Products :{
          index: "products",
          type :"Products",
          collectionName:"products"
        }
      }		
});
module.exports = function(Products) {
    //* GET CATEGORY PRODUCTS List START *//
    // Products.remoteMethod("getcategoryProducts",{
    //     http:{path: '/getcategoryProducts', verb:"post"},
    //     accepts:[
    //         { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
    //         { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
    //     ],
    //     description: 'Get Category Products list in json format',
    //     returns:{arg:"result", type:"object" }
    // });
    // Products.beforeRemote('getcategoryProducts', function(ctx, result, next){	
    //     let headerSchema = ['region']
    //     validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
    //         if(err){
    //             messages.keyMissCallBack.result['message'] = err;
    //             ctx.res.json(messages.keyMissCallBack)
    //         }
    //         else{
    //             let schema = ["category_slug"];
    //             validations(ctx.req.body,schema, (err, res)=>{
    //                 if(err){
    //                     messages.keyMissCallBack.result['message'] = err;
    //                     ctx.res.json(messages.keyMissCallBack)
    //                 }
    //                 else{
    //                     ctx.req.body.user_id =ctx.req.body.user_id ?ctx.req.body.user_id :null
    //                     ctx.req.body.limit= ctx.req.body.limit? ctx.req.body.limit: 12
    //                     ctx.req.body.sorting_type= ctx.req.body.sorting_type? ctx.req.body.sorting_type: "DESC"
    //                     next();
    //                 }
    //             })
    //         }
    //     })
    // });
    // Products.getcategoryProducts = function( req, res, cb){
    //     let catdata 
    //     if(req.body.category_slug != "all"){
    //         catdata ={
    //             "product_cat.cat_slug" : req.body.category_slug,
    //             "region.name" : req.headers.region,
    //             "is_active":1
    //         }
    //     }else{
    //         catdata ={
    //             "region.name" : req.headers.region,
    //             "is_active":1
    //         }
    //     }
    //     Products.find({ 
    //         limit: req.body.limit,
    //         order: ['product_name '+req.body.sorting_type+''],
    //         fields:['id','product_name','product_slug','discount_percentage','product_images',
    //                 'region','product_cat'],
    //         where:catdata,
    //         skip: req.body.limit * (req.body.page_no?req.body.page_no:0),
    //         include:{"relation": "Wishlist",
    //             scope:{
    //                 where:{"user_id" : req.body.user_id}
    //             }
    //         }
    //     })
    //     .then(function(product_list){
    //         let Usercart = app.models.user_cart;
    //         Usercart.findOne({
    //             where: { users_id:  req.body.user_id, is_active: 1 },
    //                 include: [{
    //                     "relation": "cart_list",
    //                     "scope": {
    //                         fields:['id','ordered_quantity','user_id','product_slug','product_name',"product_uom"],
    //                     },
    //                     "where":{"user_id" : req.body.user_id}
    //                 }] 
    //             })
    //             .then(function(cartitemslist){
    //                 let product_lists = jsonCopy(product_list)
    //                 let cartitems = jsonCopy(cartitemslist)                        
    //                 product_lists.map(function(categorys, index){
    //                     if(categorys.Wishlist.length >0){
    //                         categorys.added_to_wish_list =true
    //                     }else{
    //                         categorys.added_to_wish_list =false
    //                     }
    //                     categorys.product_category = categorys.product_cat.find((a)=>{if(a.cat_slug == req.body.category_slug) return a })
    //                     if(req.body.category_slug == "all"){
    //                         categorys.product_category = categorys.product_cat[0]
    //                     }
    //                     categorys.regions = categorys.region.find((a)=>{if(a.name == req.headers.region) return a })
    //                         cartitems?cartitems.cart_list.map((obj)=>{
    //                             if(obj.product_slug == categorys.product_slug){
    //                                 categorys.regions.uoms.find((uuom)=>{ if(uuom.uom == obj.product_uom) {uuom.cart_quantity = obj.ordered_quantity}  }) 
    //                             }
    //                         }):null
    //                     delete categorys.region
    //                     delete categorys.product_cat
    //                     delete categorys.Wishlist
    //                 }) 
    //                 if(req.body.category_slug != "all"){
    //                     let Categories = app.models.categories;
    //                     Categories.findOne({fields:['id','cat_name','cat_code','cat_slug','cat_image',"cat_meta_data"],
    //                     "where":{"cat_slug" : req.body.category_slug,is_active:1}}, function(err, billResult){
    //                         if(err){
    //                             cb(null,messages.systemErrorCallback);
    //                         }
    //                         else if(billResult){
    //                             let result = {"status":true, "statusCode":messages.successStatusCode};
    //                             result["message"] =  `${billResult.cat_name} Items`;
    //                             result['categories'] = billResult
    //                             result['categories']['product_list'] = product_lists
    //                             cb(null, result)    
    //                         }else{
    //                             let result = {"status":true, "statusCode":messages.successStatusCode};
    //                             result["message"] = messages.errorMsg.noItemsFound;
    //                             result['productslist'] = product_lists;
    //                             cb(null,result)
    //                         }
    //                     })
    //                 }else{
    //                     let result = {"status":true, "statusCode":messages.successStatusCode};
    //                     result["message"] = `Products are found`;
    //                     result['categories'] = {
    //                         "cat_name":"all",
    //                         "cat_slug": "all",
    //                         "cat_code" :"all",
    //                         "cat_image": {
    //                             "thumbnail": null,
    //                             "image": null
    //                         },
    //                         "cat_meta_data": {
    //                             "meta_description": "All Meta Description",
    //                             "meta_title": "All Meta Title",
    //                             "meta_keywords": "Vedik,ALL Categories,Ecommerce"
    //                         },
    //                         "id":null
    //                     }
    //                     result['categories']['product_list'] = product_lists;
    //                     cb(null,result)           
    //                 }
    //             })
    //             .catch(function (error) {
    //                 console.log(error)
    //                 cb(null,messages.systemErrorCallback)
    //             })
    //     })
    //     .catch(function (error) {
    //         console.log(error)
    //         cb(null,messages.systemErrorCallback)
    //     })
    // }
    //* GET CATEGORY PRODUCTS List END *//

    //* PRODUCT DETAILS START *//
    Products.remoteMethod("getProductdetails",{
        http:{path: '/getProductdetails', verb:"post"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Products Details in json format',
        returns:{arg:"result", type:"object" }
    });
    Products.beforeRemote('getProductdetails', function(ctx, result, next){	
        let headerSchema = ['region']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                let schema = ["product_slug"];
                validations(ctx.req.body,schema, (err, res)=>{
                    if(err){
                        messages.keyMissCallBack.result['message'] = err;
                        ctx.res.json(messages.keyMissCallBack)
                    }
                    else{
                        ctx.req.body.user_id =ctx.req.body.user_id ?ctx.req.body.user_id :null
                        next();
                    }
                })
            }
        })
    });
    Products.getProductdetails = function( req, res, cb){
        Products.findOne({ 
            fields:['id','product_name','product_slug','product_short_description','product_description',
            'discount_percentage','video','rating','product_images','primary_image','region',
            'pincodes','product_meta_data','product_cat','taxes','attributes','ingredients'],
            where:{'product_slug':req.body.product_slug,"region.name":req.headers.region},
            include:[{"relation": "Relatedproducts",
                scope:{
                    fields:['id','products_id','product_slug','relatedproducts']
                }
            },{"relation": "Wishlist",
            scope:{
                where:{"user_id" : req.body.user_id}
            }
        }]
        })
        .then(function(productdetailss){
            productdetailss = JSON.parse(JSON.stringify(productdetailss))
            if(productdetailss != null){
                if(productdetailss.Wishlist.length > 0){
                    productdetailss['added_to_wish_list'] = true
                }else{
                    productdetailss['added_to_wish_list'] = false
                }
                let Usercart = app.models.user_cart;
                Usercart.findOne({
                where: { users_id:  req.body.user_id, is_active: 1 },
                    include: [{
                        "relation": "cart_list",
                        "scope": {
                            fields:['id','ordered_quantity','user_id','product_slug','product_name',"product_uom"],
                        },
                        "where":{"user_id" : req.body.user_id}
                    }] 
                })
                .then(function(cartitemslist){
                    let productdetails = jsonCopy(productdetailss);
                    let cartitems = jsonCopy(cartitemslist)                        

                    productdetails.Relatedproducts = productdetails.Relatedproducts?productdetails.Relatedproducts.relatedproducts:[]
                    productdetails.regions=productdetails.region.find((a)=>{if(a.name == req.headers.region) return a })
                    cartitems?cartitems.cart_list.map((obj)=>{
                        if(obj.product_slug == productdetails.product_slug){
                            productdetails.regions.uoms.find((uuom)=>{ if(uuom.uom == obj.product_uom) {uuom.cart_quantity = obj.ordered_quantity}  }) 
                        }
                    }):null
                    productdetails.Relatedproducts.map(function(categorys, index){ 
                        categorys.regions=categorys.region.find((a)=>{if(a.name == req.headers.region) return a })
                        delete categorys.region
                    })
                    delete productdetails.region
                        let result = {"status":true, "statusCode":messages.successStatusCode};
                        result["message"] = `${productdetails.product_name} Item Details`;
                        result['product_details'] = productdetails
                        cb(null, result);
                })
                .catch(function (error) {
                    console.log(error)
                    cb(null,messages.systemErrorCallback)
                })
            }
            else{
                let result = {"status":true, "statusCode":messages.successStatusCode};
                result["message"] = messages.errorMsg.noItemsFound;
                result['product_details'] = {}
                cb(null, result) 
            }
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
    //* PRODUCT DETAILS END *//

    //* Best Selling Products Start //*
    Products.remoteMethod("bestSellingProducts",{
        http:{path: '/bestSellingProducts', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Best selling products details in json format',
        returns:{arg:"result", type:"object" }
    });
    Products.beforeRemote('bestSellingProducts', function(ctx, result, next){
        let headerSchema = ['region']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                next()
            }
        }) 
    });
    Products.bestSellingProducts = function(req, res, cb){
        let Transcationitems = app.models.transcation_items
        Transcationitems.find({
            fields:['product_slug',"product_id"],
            include:[{"relation":"Products",
                scope:{
                    fields:['product_slug',"id","region","product_name","discount_percentage"],
                    where:{
                        "region.name" : req.headers.region,
                    },
                },
            }]
        })
        .then(function(bestsellingproducts){
            if(bestsellingproducts.length>0){
                jsonCopy(bestsellingproducts)
                let arr = jsonCopy(bestsellingproducts), mySet = new Set();
                let countObj = {};
                CheckProducts(arr,mySet,countObj,function(err, deliveryResp){
                    if(err){
                        res.json(response(false, messages.invalidStatusCode, err))
                    }
                    else{
                        arr= arr.sort(function(a, b) {
                            return (countObj[a.product_slug] - countObj[b.product_slug]);
                        });
                        var bestselleing_products = arr.reverse().slice(0,15).reduce((unique, o) => {
                            if(!unique.some(obj => obj.product_slug === o.product_slug)) {
                                unique.push(o);
                            }
                        return unique;
                        },[]);
                        bestsellingproducts = jsonCopy(bestselleing_products)
                        bestsellingproducts.map(function(products, index){ 
                            products.product_name = products.Products.product_name
                            products.productid = products.Products.id
                            products.discount_percentage = products.Products.discount_percentage
                            products.regions = products.Products.region.find((a)=>{if(a.name == req.headers.region) return a })
                            delete products.Products
                            delete products.product_id 
                        }) 
                   
                        let result = {"status":true, "statusCode":messages.successStatusCode};
                        result["message"] = `Best Selling Products`
                        result["best_selling_products"] = bestsellingproducts
                        cb(null, result)
                    }
                })
            }else{
                let result = {"status":true, "statusCode":messages.successStatusCode};
                result["message"] = messages.errorMsg.noItemsFound;
                result['best_selling_products'] = bestsellingproducts;
                cb(null,result)
            }
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }

    function CheckProducts(arr,mySet,countObj, cb){
        let temp =[];
        let missing_product = [];
        for (let obj of arr) {
            if(obj.Products){
                temp.push(true)
            }else{
                temp.push(false);
                missing_product.push(obj.product_slug.toUpperCase())
            }
            if (mySet.has(obj.product_slug))
                countObj[obj.product_slug]++;
            else {
                countObj[obj.product_slug] = 1;
                mySet.add(obj.product_slug);
            }
        }
        if(temp.includes(false))
        {
            // console.log(temp,"---",missing_product)
            cb("region missing for " + missing_product, null)  
        }else
        {
            // console.log(temp,"temp")
            cb(null, true)  
        }
    }
    //* Best Selling Products End //*

    //* Global Products Search Start//*
    Products.remoteMethod('searchproducts', {			
        http:{path: '/searchproducts', verb:"post"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Search product details in json format',
        returns:{arg:"result", type:"object" }		
    });
    Products.beforeRemote('searchproducts', function(ctx, result, next){
        let headerSchema = ['region']
        console.log(ctx.req.headers,'ctx.req.headers')
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                ctx.req.body.limit= ctx.req.body.size? ctx.req.body.size: 12
                next()
            }
        }) 
    });
    Products.searchproducts = function(req, res, cb) {
        console.log((req.body),'--product name--',req.body.size)
        client.search({index:'products', type:'Products',
        body: {
            _source: ["_id",'product_name','product_slug','discount_percentage','region','product_cat','product_meta_data','is_active'],
            size: req.body.size,
            from: req.body.size * (req.body.from?req.body.from:0),
            query: {
            //   bool: {
            //     filter: [
            //       { match: { "product_name": req.body.product_name }},
            //     //   { match: { "product_cat.cat_name": req.body.product_name }},
            //     ],
            //   },
            // match_phrase_prefix: {
            //     "product_name": req.body.product_name
            // }
                // match: {
                //     product_name: {
                //     query : req.body.product_name,
                //     fuzziness: "AUTO",
                //     //   operator: "and"
                //     }
                // }
                multi_match: {
                    query: req.body.product_name,
                    fields: ['product_name',"product_cat.cat_name","product_meta_data.meta_keywords"],
                    // fuzziness: "AUTO",
                    lenient: true,
                    analyzer : "standard",
                    type: "phrase_prefix"
                },
                
            },
          },
        }).then(function(resp) {
            let Usercart = app.models.user_cart;
            Usercart.findOne({
                where: { users_id:  req.body.user_id, is_active: 1 },
                    include: [{
                        "relation": "cart_list",
                        "scope": {
                            fields:['_id','ordered_quantity','user_id','product_slug','product_name',"product_uom"],
                        },
                        "where":{"user_id" : req.body.user_id}
                    }] 
                })
                .then(function(cartitemslist){
                    let Wishlist = app.models.wishlist
                    Wishlist.find({where:{user_id:req.body.user_id },
                        fields:{created_at: false,updated_at:false, products_id:false}},(err, wish_list)=>{
                        if(err){
                            res.json(response(false, messages.systemError,messages.successMsg.removedWishlist))
                        }else{
                            var listslugs=[]
                            wish_list.map(function(products_slugs) {
                                listslugs.push(products_slugs.product_slug)
                            })
                            let cartitems = jsonCopy(cartitemslist) 
                            let results = [];
                            resp.hits.hits.forEach(function(h) {
                                h._source['product_id'] = h._id	
                                results.push(h._source);   
                            });	
                            var data = results.filter((item) => item.is_active == '1');
                            // console.log(data)
                            let Categories = app.models.categories;
                            Categories.find({
                                fields:['cat_slug','cat_name'],
                                where: { is_active:1 },
                            })
                            .then(function(cat_list){
                                var catlist = cat_list.map((resp) => {return resp.cat_slug})
                                data.map(function(categorys, index){ 
                                    categorys.product_cat.filter(function(productcat, index){
                                        if(catlist.includes(productcat.cat_slug)){
                                            categorys.cat_name =categorys.product_cat[0].cat_name
                                            categorys.cat_slug = categorys.product_cat[0].cat_slug
                                        }
                                    })
                                    categorys.regions=categorys.region.find((a)=>{if(a.name == req.headers.region) return a })
                                   
                                    cartitems?cartitems.cart_list.map((obj)=>{
                                        if(obj.product_slug == categorys.product_slug){
                                            categorys.regions.uoms.find((uuom)=>{ if(uuom.uom == obj.product_uom) {uuom.cart_quantity = obj.ordered_quantity}  }) 
                                        }
                                    }):null
                                    if( listslugs.includes(categorys.product_slug)){
                                        categorys.added_to_wish_list =true
                                    }else{
                                        categorys.added_to_wish_list =false
                                    }
                                    delete categorys.region
                                    delete categorys.product_cat
                                    return categorys
                                })

                                let result = {"status":true, "statusCode":messages.successStatusCode};
                                result["message"] = "Search Products"
                                result["search_products"] = data.filter((item) => item.cat_slug != undefined);
                                cb(null, result)
                            })
                            .catch(function (error) {
                                console.log(error)
                                cb(null,messages.systemErrorCallback)
                            })
                        }
                    })
                })
                .catch(function (error) {
                    console.log(error)
                    cb(null,messages.systemErrorCallback)
                })
        }, function(err) {
            console.log(err)
            cb(null,messages.systemErrorCallback)
        });	
    }
    //* Global Products Search End //*
    Products.beforeRemote('getNewArrivals', function(ctx, result, next){	
        let headerSchema = ['region']
        let {req} = ctx
        validateheaders(ctx.req.headers,headerSchema,async (err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                try {
                    let product = await Products.find({
                        where:{
                            "region.name" : req.headers.region,
                            "is_active":1,
                            "is_new_arrival":1
                        },
                        fields:['id','product_name','product_slug','discount_percentage','product_images',
                        'region','product_cat',"is_out_of_stock","min_order_qty","max_order_qty","is_new_arrival"],
                    })
                    let newArrivals = product.map(el=>{
                        let obj ={
                            "id" :el.id,
                            "product_name":el.product_name,
                            "product_slug":el.product_slug,
                            "discount_percentage":el.discount_percentage,
                            "product_images":el.product_images,
                            "is_out_of_stock":el.is_out_of_stock,
                            "min_order_qty":el.min_order_qty,
                            "max_order_qty":el.max_order_qty,
                            "is_new_arrival":el.is_new_arrival
                        }
                        obj['category'] = el.product_cat.find(cat=>{
                            if(cat){
                                return cat
                            }
                        })
                        obj['region'] = el.region.find(e=>e.name == req.headers.region)
                        return obj
                    })
                    let latest = newArrivals.filter(e=>e.region && e.region.uoms && e.region.uoms.length > 0)
                    let result = {
                        status:true,
                        status_code :messages.successStatusCode,
                        message: messages.successMsg.newProductsFound,
                        products : latest
                    }
                    ctx.res.json({result})
                } catch (error) {
                    console.log("error",error)
                    ctx.res.json(messages.systemErrorCallback)
                }
            }
        })
    });
    Products.remoteMethod("getNewArrivals",{
        http:{path: '/getNewArrivals', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get New Arrivals Products list in json format',
        returns:{arg:"result", type:"object" }
    });

    Products.beforeRemote('featuredProducts', function(ctx, result, next){	
        let headerSchema = ['region']
        let {req} = ctx
        validateheaders(ctx.req.headers,headerSchema,async (err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                try {
                    let product = await Products.find({
                        where:{
                            "region.name" : req.headers.region,
                            "is_active":1,
                            "is_feature_product":1
                        },
                        fields:['id','product_name','product_slug','discount_percentage','product_images',
                        'region','product_cat',"is_new_arrival"],
                        include:{"relation": "Wishlist",
                            scope:{
                                where:{"user_id" : req.query.user_id}
                            }
                        }
                    })
                    // console.log(JSON.stringify(product))
                   
                    
                    let Usercart = app.models.user_cart;
                    let cartitemslist = await Usercart.findOne({
                    where: { users_id:  req.query.user_id, is_active: 1 },
                        include: [{
                            "relation": "cart_list",
                            "scope": {
                                fields:['id','ordered_quantity','user_id','product_slug','product_name',"product_uom"],
                            },
                            "where":{"user_id" : req.query.user_id}
                        }] 
                    })
                    product = jsonCopy(product)
                    let cartitems = jsonCopy(cartitemslist)                        
                    product.map(function(categorys, index){
                        categorys.regions = categorys.region.find((a)=>{if(a.name == req.headers.region) return a })
                            cartitems?cartitems.cart_list.map((obj)=>{
                                if(obj.product_slug == categorys.product_slug){
                                    categorys.regions.uoms.find((uuom)=>{ if(uuom.uom == obj.product_uom) {uuom.cart_quantity = obj.ordered_quantity}  }) 
                                }
                            }):null
                    })
                    let newArrivals = product.map(el=>{
                        el = JSON.parse(JSON.stringify(el))
                        let obj ={
                            "id" :el.id,
                            "product_name":el.product_name,
                            "product_slug":el.product_slug,
                            "discount_percentage":el.discount_percentage,
                            "product_images":el.product_images,
                            "is_new_arrival":el.is_new_arrival
                        }
                        if(el.Wishlist.length > 0){
                            obj['added_to_wish_list'] = true
                        }else{
                            obj['added_to_wish_list'] = false
                        }
                        obj['category'] = el.product_cat.find(cat=>{
                            if(cat){
                                return cat
                            }
                        })
                        obj['region'] = el.region.find(e=>e.name == req.headers.region)
                        obj['region'].uoms.map(e=>{
                            e.max_order_qty = Number(e.max_order_qty);
                            e.min_order_qty = Number(e.min_order_qty);
                            e.available_qty = Number(e.available_qty);

                        })
                        return obj
                    })

                    let latest = newArrivals.filter(e=>e.region && e.region.uoms && e.region.uoms.length > 0)
                    let result = {
                        status:true,
                        status_code :messages.successStatusCode,
                        message: messages.successMsg.featuredProducts,
                        products : latest
                    }
                    ctx.res.json({result})
                } catch (error) {
                    console.log("error",error)
                    ctx.res.json(messages.systemErrorCallback)
                }
            }
        })
    });
    Products.remoteMethod("featuredProducts",{
        http:{path: '/featuredProducts', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get featuredProducts list in json format',
        returns:{arg:"result", type:"object" }
    });


    Products.beforeRemote('sample',function(ctx, result, cb){	
        // console.log(db._models)
        // ctx.res.json({"db":"db"})
        // db.collection('products').find({})
        // .then(resp=>{
        //     ctx.res.json(resp)
        // })
        // .catch(err=>{
        //     ctx.res.json(err)
        // })
        // Products.getDataSource().connector.connect(function (err, db) {
        //     var collection = db.collection("products");
        //     collection.find({}, function (err, res) { // define whichever query you need
        //         console.log("collection find res:"+res);
        //         console.log("collection find err:"+err);
    
        //         if(err) {
        //         return cb(err);
        //         // return;
        //       }
        //       res.toArray(function (err, realRes) { // this part is crucial
        //       // otherwise if you try to print res you will get a dump of the db object
        //         if(err) {
        //          return  cb(err);
        //         //   return;
        //         }
        //         console.log("documnet result:"+realRes);
        //         console.log("document err:"+err);
        //         // ctx.res.json({realRes})
        //         // cb(null,realRes)
        //       })
        //   })
        // }
        // );
            Products.update({
                product_slug:"chinna-rasalu",
                "region.uoms.uom":"grams"
            },
            {$set:{
                "region.$[].uoms.$.available_qty":30
            }},{arrayFilters: [{"name":"usa"}],allowExtendedOperators: true  }) 
            .then(resp=>{
                console.log(resp)
            })
            .catch(err=>{
                console.log(err)
            })
                // ctx.res.json(resp)      
            // ctx.res.json({"abc":"abx",resp})
            // console.log(updateresp)
    })
    Products.remoteMethod("sample",{
        http:{path: '/sample', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get sample list in json format',
        returns:{arg:"result", type:"object" }
    });


    Products.remoteMethod("getcategoryProducts",{
        http:{path: '/getcategoryProducts', verb:"post"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Category Products list in json format',
        returns:{arg:"result", type:"object" }
    });
    Products.beforeRemote('getcategoryProducts', function(ctx, result, next){	
        let headerSchema = ['region']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                let schema = ["category_slug"];
                validations(ctx.req.body,schema, (err, res)=>{
                    if(err){
                        messages.keyMissCallBack.result['message'] = err;
                        ctx.res.json(messages.keyMissCallBack)
                    }
                    else{
                        ctx.req.body.user_id =ctx.req.body.user_id ?ctx.req.body.user_id :null
                        ctx.req.body.limit= ctx.req.body.limit? ctx.req.body.limit: 12
                        ctx.req.body.sorting_type= ctx.req.body.sorting_type? ctx.req.body.sorting_type: "DESC"
                        next();
                    }
                })
            }
        })
    });
    Products.getcategoryProducts = function( req, res, cb){
        let catdata 
        if(req.body.category_slug.length > 0 && req.body.category_slug[0] != "all"){
            catdata ={
                "product_cat.cat_slug" : {inq:req.body.category_slug},
                "region.name" : req.headers.region,
                "is_active":1
            }
        }else{
            catdata ={
                "region.name" : req.headers.region,
                "is_active":1
            }
        }
        Products.find({ 
            limit: req.body.limit,
            order: ['product_name '+req.body.sorting_type+''],
            fields:['id','product_name','product_slug','discount_percentage','product_images',
                    'region','product_cat'],
            where:catdata,
            skip: req.body.limit * (req.body.page_no?req.body.page_no:0),
            include:{"relation": "Wishlist",
                scope:{
                    where:{"user_id" : req.body.user_id}
                }
            }
        })
        .then(function(product_list){
            let Usercart = app.models.user_cart;
            Usercart.findOne({
                where: { users_id:  req.body.user_id, is_active: 1 },
                    include: [{
                        "relation": "cart_list",
                        "scope": {
                            fields:['id','ordered_quantity','user_id','product_slug','product_name',"product_uom"],
                        },
                        "where":{"user_id" : req.body.user_id}
                    }] 
                })
                .then(function(cartitemslist){
                    let product_lists = jsonCopy(product_list)
                    let cartitems = jsonCopy(cartitemslist)                        
                    product_lists.map(function(categorys, index){
                        if(categorys.Wishlist.length >0){
                            categorys.added_to_wish_list =true
                        }else{
                            categorys.added_to_wish_list =false
                        }
                        categorys.product_category = categorys.product_cat.find((a)=>{if(req.body.category_slug.includes(a.cat_slug)) return a })
                        if(req.body.category_slug.length == 0){
                            categorys.product_category = categorys.product_cat[0]
                        }
                        categorys['cat_name'] = categorys.product_category.cat_name
                        categorys['cat_slug'] = categorys.product_category.cat_slug
                        categorys.regions = categorys.region.find((a)=>{if(a.name == req.headers.region) return a })
                            cartitems?cartitems.cart_list.map((obj)=>{
                                if(obj.product_slug == categorys.product_slug){
                                    categorys.regions.uoms.find((uuom)=>{ if(uuom.uom == obj.product_uom) {uuom.cart_quantity = obj.ordered_quantity}  }) 
                                }
                            }):null
                        delete categorys.region
                        delete categorys.product_cat
                        delete categorys.Wishlist
                    }) 
                    res.json({
                        result:{
                            status : true,
                            statusCode:200,
                            message :"Products for your selection found",
                            product_list :product_lists
                        }
                    })
                    // if(req.body.category_slug != "all"){
                    //     let Categories = app.models.categories;
                    //     Categories.findOne({fields:['id','cat_name','cat_code','cat_slug','cat_image',"cat_meta_data"],
                    //     "where":{"cat_slug" : req.body.category_slug,is_active:1}}, function(err, billResult){
                    //         if(err){
                    //             cb(null,messages.systemErrorCallback);
                    //         }
                    //         else if(billResult){
                    //             let result = {"status":true, "statusCode":messages.successStatusCode};
                    //             result["message"] =  `${billResult.cat_name} Items`;
                    //             result['categories'] = billResult
                    //             result['categories']['product_list'] = product_lists
                    //             cb(null, result)    
                    //         }else{
                    //             let result = {"status":true, "statusCode":messages.successStatusCode};
                    //             result["message"] = messages.errorMsg.noItemsFound;
                    //             result['productslist'] = product_lists;
                    //             cb(null,result)
                    //         }
                    //     })
                    // }else{
                    //     let result = {"status":true, "statusCode":messages.successStatusCode};
                    //     result["message"] = `Products are found`;
                    //     result['categories'] = {
                    //         "cat_name":"all",
                    //         "cat_slug": "all",
                    //         "cat_code" :"all",
                    //         "cat_image": {
                    //             "thumbnail": null,
                    //             "image": null
                    //         },
                    //         "cat_meta_data": {
                    //             "meta_description": "All Meta Description",
                    //             "meta_title": "All Meta Title",
                    //             "meta_keywords": "Vedik,ALL Categories,Ecommerce"
                    //         },
                    //         "id":null
                    //     }
                    //     result['categories']['product_list'] = product_lists;
                    //     cb(null,result)           
                    // }
                })
                .catch(function (error) {
                    console.log(error)
                    cb(null,messages.systemErrorCallback)
                })
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
};

