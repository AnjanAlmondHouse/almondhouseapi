'use strict';
let messages = require('../messages');
let { verifyToken,response } = require('../utils')
let app = require('../../server/server')

module.exports = function(Sitesubscribers) {
    //* sitesub Scribers Start *//
    Sitesubscribers.remoteMethod("sitesubscribers",{
        http:{path: '/sitesubscribers', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add Site Subscribers in json format',
        returns:{arg:"result", type:"object" }
    });
    Sitesubscribers.beforeRemote('sitesubscribers', function (ctx, result, next) {
        let schema = ["email"];
        validations(ctx.req.body, schema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                next();
            }
        })
    });
    Sitesubscribers.sitesubscribers = function (requestData, req, res, cb) {
        Sitesubscribers.findOrCreate({
            where: { "email": requestData.email}},
            {
                "email": requestData.email
            }
        )
        .then(function ([addemail, created]) {
            if (created) {        
                res.json(response(true, messages.successStatusCode, messages.successMsg.subscribeSuccess))
            
            } else {
                messages.resultCallbacks.existsCallBack[messages.message] = messages.errorMsg.emailExistsInSubscribers;
                cb(null, messages.resultCallbacks.existsCallBack)
            }
        })
        .catch(function (error) {
            console.log(error)
            cb(null, messages.systemErrorCallback)
        })
    }
    //* Sitesub Scribers End *//
};
