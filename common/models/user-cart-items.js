'use strict';
let { successMsg } = require('../messages');
const messages = require('../messages');
let { verifyToken, response, toFloat, triggerEmail } = require('../utils');
let { validations, cartValidations } = require('../validations');
var async = require('async');
let app = require("../../server/server");
var db = app.datasources.postgresql.connector;

module.exports = function (Usercartitems) {

    function InsertItemsInCart(requestData, callback) {
        let insertData = {
            users_id: requestData.users_id, user_name: requestData.user_name,
            master_status_id: 1, status_name: "pending", is_active: 1,
            session_id: requestData.session_id, location: requestData.location , currency: requestData.currency
        }
        const userCart = app.models.user_cart;
        userCart.findOrCreate({ where: { 'users_id': requestData.users_id,  is_active:1 } }, insertData)
            .then((cartRes) => {

                async.forEach(requestData.product_list, function (products, cb) {
                    let users_cart_id = cartRes[0].id;
                    let discount_amount = Number(products.discount_percentage * products.price * products.ordered_quantity) / 100;
                    let price_after_discount = (products.price * products.ordered_quantity) - discount_amount
                    getTaxOfProduct(products.product_slug, products.product_selling_price,products.discount_percentage).then((tax_value) => {
                        let total_tax = tax_value * products.ordered_quantity
                        let cartItemsInsertData = {
                            users_cart_id: users_cart_id,
                            product_slug: products.product_slug,
                            category_slug: products.category_slug,
                            category_name: products.category_name,
                            user_id: requestData.users_id,
                            product_uom: products.uom,
                            product_id:  products.product_id,
                            product_name: products.product_name,
                            product_price: products.price,
                            discount_percentage: products.discount_percentage,
                            ordered_quantity: products.ordered_quantity,
                            discount_amount: discount_amount,
                            price_after_discount: price_after_discount,
                            tax_total: total_tax,
                            total_price: price_after_discount,
                            thumbnail_product_image: products.thumbnail_product_image,
                            product_image: products.thumbnail_product_image,
                            product_selling_price: products.product_selling_price
                        }
                        // cb(null, true)
                        console.log(cartItemsInsertData,"cartItemsInsertData")
                        Usercartitems.findOrCreate({
                            where: {
                                users_cart_id: users_cart_id,
                                product_slug: products.product_slug,
                                user_id: products.user_id,
                                product_uom: products.uom
                            }
                        }, cartItemsInsertData)
                            .then((cartItemsInsertResp) => {
                                if (!cartItemsInsertResp[1]) {
                                    if(products.ordered_quantity == 0){
                                        DeleteItemFromCart(cartRes[0], cartItemsInsertResp[0], products, function (delErr, delResp) {
                                            cb(delErr, delResp)
                                        })
                                    }else{
                                        updateItemsInCart(cartRes[0], cartItemsInsertResp[0], products, function (err, resp) {
                                            console.log(err,resp, "update cart items")
                                            if (err) {
                                                cb(err)
                                            } else {
                                                cb(false,resp )
                                            }
                                        })
                                    }
                                } else {
                                    updateCartValue(cartRes[0], null, cartItemsInsertData)
                                    cb(false,cartItemsInsertData )
                                }
                            })
                            .catch((cartItemsInsertErr) => {
                                console.log("errot",cartItemsInsertErr)
                                cb(cartItemsInsertErr)
                            })
                    })
                    .catch((taxErr)=>{
                        console.log("taxErr")
                        console.log(taxErr)
                    })
                }, function(err, response){
                    console.log(err, response)
                    callback(err, {message:successMsg.addToCart})
                })
            })
            .catch((cartErr) => {
                console.log(cartErr)
                callback(cartErr)
            })
    }

    function getTaxOfProduct(product_slug, price,discount_percentage) {
        return new Promise((resolve)=>{
            resolve(0)
        })
        // try {
        //     let products = app.models.products;
        //     let total_tax = 1;
        //     return new Promise(function (resolve, reject) {
        //         products.findOne({ where: { product_slug: product_slug } }, (err, productResp) => {
        //             if (err) {
        //                 reject(0)
        //             }
        //             else {
        //                 total_tax = productResp?productResp.taxes.reduce((a, b) => a + (Number(b.tax_percentage) * (Number(price)- (Number(price)*Number(discount_percentage)/100) ) / 100), 0):0
        //                 resolve(0)
        //             }

        //         })
        //         //   return 2;

        //     })
        // } catch (error) {
        //     console.log("error",error)
        //     return 0
        // }


    }
    function updateItemsInCart(users_cart, oldProductData, requestData, cb) {
        let discount_amount = Number(requestData.discount_percentage * requestData.price * requestData.ordered_quantity) / 100;
        let price_after_discount = (requestData.price * requestData.ordered_quantity) - discount_amount
        getTaxOfProduct(requestData.product_slug, requestData.product_selling_price, requestData.discount_percentage).then((tax_value) => {
            let total_tax = tax_value * requestData.ordered_quantity
            let cartItemsUpdateData = {
                ordered_quantity: requestData.ordered_quantity,
                discount_amount: discount_amount,
                price_after_discount: price_after_discount,
                tax_total: total_tax,
                total_price: price_after_discount,
            }
            Usercartitems.update({ users_cart_id: users_cart.users_cart_id, id: oldProductData.id },
                cartItemsUpdateData)
                .then((updateRes) => {
                    updateCartValue(users_cart, oldProductData, cartItemsUpdateData)
                    cb(null, { message: successMsg.updateCart })
                })
                .catch((updateResp) => {
                    console.log("updateResp",updateResp)
                    cb(updateResp, null)
                })
        })
    }

    function updateCartValue(cartData, oldProductData, newProductData) {
        console.log(`update user_cart set total_tax = total_tax + ${Number(newProductData.tax_total) - Number(oldProductData ? oldProductData.tax_total : 0)}, cart_total = cart_total + ${Number(newProductData.price_after_discount) - Number(oldProductData ? oldProductData.price_after_discount : 0)} where id = ${cartData.id} and users_id = '${cartData.users_id}' and session_id = '${cartData.session_id}'`)
        db.query(`update user_cart set total_tax = total_tax + ${Number(newProductData.tax_total) - Number(oldProductData ? oldProductData.tax_total : 0)}, cart_total = cart_total + ${Number(newProductData.price_after_discount) - Number(oldProductData ? oldProductData.price_after_discount : 0)}, coupon_code = null, coupon_value = 0.00 where id = ${cartData.id} and users_id = '${cartData.users_id}' and session_id = '${cartData.session_id}' `, (err, resp)=>{
            console.log("update cart", err, resp);
            return true
        })
    }
    function DeleteItemFromCart(users_cart, oldProductData, requestData, cb){
        let discount_amount = Number(requestData.discount_percentage * requestData.price * requestData.ordered_quantity) / 100;
        let price_after_discount = (requestData.price * requestData.ordered_quantity) - discount_amount
        getTaxOfProduct(requestData.product_slug, requestData.product_selling_price,requestData.discount_percentage).then((tax_value) => {
            let total_tax = tax_value * requestData.ordered_quantity
            let cartItemsUpdateData = {
                ordered_quantity: requestData.ordered_quantity,
                discount_amount: discount_amount,
                price_after_discount: price_after_discount,
                tax_total: total_tax,
                total_price: price_after_discount,
            }
            Usercartitems.destroyAll({ users_cart_id: users_cart.users_cart_id, id: oldProductData.id })
                .then((deleteResp) => {
                    console.log("deleteResp",deleteResp)
                    updateCartValue(users_cart, oldProductData, cartItemsUpdateData)
                    cb(null, { message: "removed from cart" })
                })
                .catch((deleteErr) => {
                    console.log("updateResp",deleteErr)
                    cb(deleteErr, null)
                })
        })
    }
    //* adding or updating items to cart //*
    Usercartitems.beforeRemote('addToCart', function (ctx, result, next) {
        let requestData = ctx.req.body;
        let {req} = ctx
        console.log(requestData,ctx.req.headers,"-----body")
        // if ( ctx.req.headers.userid && ctx.req.headers.location && ctx.req.headers.currency) {
            let schema = ["product_id", "user_id", "user_name", "product_slug", "product_name",
                "ordered_quantity", "uom", "price", "discount_percentage", "thumbnail_product_image","product_selling_price", "category_slug", "category_name"]
            // verifyToken(ctx.req.headers.token, function (err, res) {
            //     if (err) {
            //         ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
            //     } else {
                    cartValidations(requestData, schema, async (err, res) => {
                        if (err) {
                            ctx.res.json(response(false, messages.keyMissStatusCode, err))
                        }
                        else{
                            let Products = app.models.products
                            let product = await Products.findOne({
                                where:{
                                    product_slug : requestData[0].product_slug,
                                    is_active : 1
                                },
                            })
                            if(!product){
                                return ctx.res.json(response(false, messages.keyMissStatusCode, "No product found"))
                            }
                            let regionUoms = product.region.find(el=>el.name == req.headers.region)
                            let uom = regionUoms.uoms.find(e=>e.uom == requestData[0].uom)
                            
                            if(Number(requestData[0].ordered_quantity) > Number(uom.available_qty)){
                                return ctx.res.json(response(false, messages.keyMissStatusCode,  "Requested quantity not available in stock"))
                            }
                            if(Number(requestData[0].ordered_quantity) < Number(uom.min_order_qty)){
                               
                                return ctx.res.json(response(false, messages.keyMissStatusCode,  "Minimum order quantity should be "+uom.min_order_qty))
                            }
                            if(Number(requestData[0].ordered_quantity) > Number(uom.max_order_qty)){
                                return ctx.res.json(response(false, messages.keyMissStatusCode, "Maximum allowed quantity is "+uom.max_order_qty))
                            }
                            next()
                            // if(requestData[0].category_slug  != 'mangoes' ){
                            //     if(requestData[0].ordered_quantity <= 200 ){
                            //         next()
                            //     }else{
                            //         ctx.res.json(response(false, messages.keyMissStatusCode, "Maximum allowed quantity is 200"))
                            //     }
                            // }else{
                            //     if(requestData[0].category_slug  == 'mangoes' && requestData[0].ordered_quantity > 4){
                            //         if(requestData[0].ordered_quantity <= 200 ){
                            //             next()
                            //         }else{
                            //             ctx.res.json(response(false, messages.keyMissStatusCode, "Maximum allowed quantity is 200"))
                            //         }
                            //     }else{
                            //         ctx.res.json(response(false, messages.keyMissStatusCode, "Minimum order quantity should be 5 Kgs"))
                            //     }
                            // } 
                        }
                    })
                // }
            // })
        // } else {
        //     ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        // }
    });
    Usercartitems.addToCart = function (requestData, req, res, cb) {
        // triggerEmail("add_to_cart",{to_email:"saikrishna.k@axlrdata.com", subject:"test mail", message: "Sai nuv thop ra"},`<h1>This is the best thing you can do</h1>`, (err, resp)=>{
        //     console.log(err, resp, "email triggering")
        // })
        // console.log(requestData, "Request Data",req.headers)
        let cartInsertData = {
            location: req.headers.region,
            currency: (req.headers.region == "india")? 'rupee' : 'dollar',
            session_id: req.headers.sessionid,
            users_id: requestData[0].user_id,
            user_name: requestData[0].user_name,
            product_list: requestData
        }
        
        InsertItemsInCart(cartInsertData, (err, resp) => {
            if (err) {
                res.json(messages.systemErrorCallback)
            }
            else {
                res.json(response(true, messages.successStatusCode, resp.message))
            }

        })
    }
    Usercartitems.remoteMethod("addToCart", {
        http: { path: '/addToCart', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'array', required: true, http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can add items to cart if he is logged in',
        returns: { arg: "result", type: "object" }
    });

    //* adding or updating items to cart //*
    //* remove items from cart //*
    Usercartitems.beforeRemote('removeProductFromCart', function (ctx, result, next) {
        let requestData = ctx.req.body
        if (ctx.req.headers.userid) {
            let schema = ["user_cart_product_id", "user_id", "users_cart_id"]
            // verifyToken(ctx.req.headers.token, function (err, res) {
            //     if (err) {
            //         ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
            //     } else {
                    validations(requestData, schema, (err, res) => {
                        if (err) {
                            ctx.res.json(response(false, messages.keyMissStatusCode, err))
                        }
                        else {
                            let userCart = app.models.user_cart;
                            userCart.findOne({ where: { user_id: requestData.user_id, id: requestData.users_cart_id } })
                                .then((cartResult) => {
                                    if (!cartResult)
                                        ctx.res.json(response(false, messages.errorMsg.noCartFound))
                                    else {
                                        ctx.req.body.cart_total = Number(cartResult.cart_total)
                                        Usercartitems.find({
                                            where: {
                                                id: { inq: requestData.user_cart_product_id },
                                                user_id: requestData.user_id,
                                                users_cart_id: requestData.users_cart_id
                                            }
                                        })
                                            .then((cartItemResp) => {
                                                console.log(cartItemResp.length)
                                                if (cartItemResp.length == requestData.user_cart_product_id.length) {
                                                    let sum = cartItemResp.reduce(function (accumulator, currentValue) {
                                                        return accumulator + toFloat(currentValue.total_price)
                                                    }, 0)
                                                    ctx.req.body.remove_items_total = sum
                                                    next()
                                                } else {
                                                    ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.cartProductsNotFound))
                                                }
                                            })
                                            .catch((cartItemsErr) => {
                                                console.log(cartItemsErr)
                                                ctx.res.json(messages.systemErrorCallback)
                                            })
                                    }
                                })
                                .catch((carterr) => {
                                    console.log(carterr)
                                    ctx.res.json(messages.systemErrorCallback)
                                })
                        }
                    })
            //     }
            // })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        }
    });
    Usercartitems.removeProductFromCart = function (requestData, req, res, cb) {
        Usercartitems.destroyAll({
            id: { inq: requestData.user_cart_product_id },
            user_id: requestData.user_id
        })
            .then(result => {
                let userCart = app.models.user_cart;
                Usercartitems.find({ where: { users_cart_id: requestData.users_cart_id, user_id: requestData.user_id } })
                    .then((cartItemsResult) => {
                        let price_obj = { "total_price": 0, "discount_amount": 0, "tax_total": 0 }
                        cartItemsResult.map((b) => {
                            price_obj["total_price"] += Number(b.price_after_discount)
                            price_obj["tax_total"] += Number(b.tax_total)
                            price_obj["discount_amount"] += Number(b.discount_amount)
                        })
                        userCart.update(
                            { id: requestData.users_cart_id, user_id: requestData.user_id },
                            { "cart_total": price_obj.total_price, "tax_total": price_obj.tax_total,
                            "coupon_code":null, "coupon_value":0.00}
                        )
                            .then((cartUpdResp) => {
                                res.json(response(true, messages.successStatusCode, messages.successMsg.removeCart))
                            })
                            .catch((cartUpdErr) => {
                                res.json(messages.systemErrorCallback)
                            })
                    })
                    .catch((cartItemsErr) => {
                        res.json(messages.systemErrorCallback)
                    })
            })
            .catch(error => {
                res.json(messages.systemErrorCallback)
            })
    }
    Usercartitems.remoteMethod("removeProductFromCart", {
        http: { path: '/removeProductFromCart', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'object', required: true, http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can remove item from cart',
        returns: { arg: "result", type: "object" }
    });
    //* remove items from cart //*
};
