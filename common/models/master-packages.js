'use strict';
let messages = require('../messages')
const {response,jsonCopy} = require("../utils")
const {getProductsRegionwise} = require("../exportfunction")
let app = require('../../server/server');

module.exports = function(Masterpackages) {
    //* Get Packages list Start *//
    Masterpackages.remoteMethod("getPackageslist",{
        http:{path: '/getPackageslist', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get packages list in json format',
        returns:{arg:"result", type:"object"}
    });
    Masterpackages.beforeRemote('getPackageslist', function(ctx, result, next){	
        if(!ctx.req.query.limit){ 
            ctx.req.query.limit = 3
        }
        if(!ctx.req.query.sorting_type){ 
            ctx.req.query.sorting_type ="DESC"
        }
		next();
    });
    Masterpackages.getPackageslist = function( req, res, cb){
        Masterpackages.find({fields:['id','package_cycle','package_name','package_image','package_slug','package_code'],limit: req.query.limit,order: 'package_name '+req.query.sorting_type+''})
        .then(function(packages_list){
            let result = {"status":true, "statusCode":messages.successStatusCode};
            result["message"] = "Packages List"
            result["packages_list"] = packages_list
            cb(null, result)
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
    // *Get Packages list End *//

    // *Get Packages Products List Start *//
    Masterpackages.remoteMethod("getPackageproducts",{
        http:{path: '/getPackageproducts', verb:"post"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get package products in json format',
        returns:{arg:"result", type:"object" }
    });
    Masterpackages.beforeRemote('getPackageproducts', function(ctx, result, next){	
        let schema = ["packages_slug"];
        validations(ctx.req.body,schema, (err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                ctx.req.body.user_id =ctx.req.body.user_id ?ctx.req.body.user_id :null
                ctx.req.body.sorting_type= ctx.req.body.sorting_type? ctx.req.body.sorting_type: "DESC"
                ctx.req.body.limit= ctx.req.body.limit? ctx.req.body.limit: 10
                next();
            }
        }) 
    });
    Masterpackages.getPackageproducts = function(req, res, cb){
        Masterpackages.find({
            fields:['id','package_name','package_image',"package_slug","package_cycle","package_code","package_meta_data","offers"],
            where:{"package_slug":req.body.packages_slug},
            include:{"relation": "Packageproducts",
                scope:{
                    fields:['id','master_packages_id','master_packages_slug',"products_id","products_name","products_slug","product_sku","discount_percentage","region"],
                    limit: req.body.limit,
                    skip: req.body.limit * (req.body.page_no?req.body.page_no:0)
                }
            }
        })
        .then(function(packageproducts){
            packageproducts = jsonCopy(packageproducts)
            let Wishlist = app.models.wishlist
            Wishlist.find({where:{user_id:req.body.user_id },
                fields:{created_at: false,updated_at:false, products_id:false}},(err, wish_list)=>{
                if(err){
                    res.json(response(false, messages.systemError,messages.successMsg.removedWishlist))
                }else{
                    let packageproductsdetails = packageproducts[0].Packageproducts
                    if(packageproductsdetails.length>0){
                        var listslugs=[]
                        wish_list.map(function(products_slugs) {
                            listslugs.push(products_slugs.product_slug)
                        })
                        packageproductsdetails.map(function(products, index){ 
                            products.regions = products.region.find((a)=>{if(a.name == req.headers.region) return a })
                            if( listslugs.includes(products.products_slug)){
                                products.added_to_wish_list =true
                            }else{
                                products.added_to_wish_list =false
                            }
                            delete products.region
                        }) 
                        let result = {"status":true, "statusCode":messages.successStatusCode};
                        result["message"] = `${packageproducts[0].package_name} products`
                        result["packagedetails"] = packageproducts
                        cb(null, result)
                    }else{
                        let result = {"status":true, "statusCode":messages.successStatusCode};
                        result["message"] = messages.errorMsg.noItemsFound;
                        result['packagedetails'] = packageproductsdetails;
                        cb(null,result)
                    }
                }
            })
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
    //* Get Packages Products List End //*
};
