'use strict';

module.exports = function(Customers) {
    //* User Loging //*
    Customers.beforeRemote('userLogin', function(ctx, result, next){
        console.log(ctx.req.body)	
		next();
    });

    Customers.userLogin = function( requestData, cb){
        Customers.find()
        .then((res)=>{
            console.log(res)
        })
        .catch((error)=>{
            console.log(error)
        })
        console.log(requestData)
        
    }
    Customers.remoteMethod("userLogin",{
        http:{path: '/userLogin', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can login here by giving his credentials and get jwt token in response',
        returns:{arg:"result", type:"object" }
    });
    //* User Loging //*
};
