'use strict';
let messages = require('../messages')
let {validations,validateheaders} = require('../validations')
const {response,jsonCopy} = require("../utils")
const {getProductsRegionwise} = require("../exportfunction")

module.exports = function(Siteoffers) {
    //* Get Home Page Offers Start //*
    Siteoffers.remoteMethod("getSiteOffers",{
        http:{path: '/getSiteOffers', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get show offers in json format',
        returns:{arg:"result", type:"object" }
    });
    Siteoffers.beforeRemote('getSiteOffers', function(ctx, result, next){	
		next();
    });
    Siteoffers.getSiteOffers = function( req, res, cb){
        Siteoffers.find({
            fields:['id','offer_name','offer_slug','offer_description','offer_image','offer_code',"button_name","background_image"],
            where:{
                "is_active":1
            },
        })
        .then(function(offer_categories){
            let result = {"status":true, "statusCode":messages.successStatusCode};
            result["message"] = "Home Page Offers"
            result["offer_categories"] = offer_categories
            cb(null, result)
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
    //* Get  Home Page Offers End *//

    //* Offer Product List Start *//
    Siteoffers.remoteMethod("getofferProductDetails",{
        http:{path: '/getofferProductDetails', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get site offers product details in json format',
        returns:{arg:"result", type:"object" }
    });
    Siteoffers.beforeRemote('getofferProductDetails', function(ctx, result, next){
        let headerSchema = ['region']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                let schema = ["offer_code","offer_slug"];
                validations(ctx.req.query,schema, (err, res)=>{
                    if(err){
                        messages.keyMissCallBack.result['message'] = err;
                        ctx.res.json(messages.keyMissCallBack)
                    }else{
                        ctx.req.query.limit= ctx.req.query.limit? ctx.req.query.limit: 5
                        next();
                    }
                }) 
            }
        }) 
    });
    Siteoffers.getofferProductDetails = function(req, res, cb){
        Siteoffers.find({
            fields:['id','offer_name','offer_slug','offer_description','offer_image','offer_code'],
            where:{
                'offer_code':req.query.offer_code,
                "offer_slug" :req.query.offer_slug,
                "is_active":1
            },
            include:{"relation": "Siteoffersproducts",
                scope:{
                    fields:['products_slug','products_id','discount_percentage','site_offers_slug',"products_name","region"],
                    limit: req.query.limit,
                    skip: req.query.limit * (req.query.page_no?req.query.page_no:0)
                },
            }
        })
        .then(function(offerproducts){
            let offer_productslist = jsonCopy(offerproducts)
            offer_productslist.map(function(products, index){ 
                getProductsRegionwise(products.Siteoffersproducts,req.headers.region)
            }) 
            if(offer_productslist.length>0){
                let result = {"status":true, "statusCode":messages.successStatusCode};
                result["message"] = `${offer_productslist[0].offer_name} products`
                result["offerproductdetails"] = offer_productslist[0]
                cb(null, result)
            }else{
                let result = {"status":true, "statusCode":messages.successStatusCode};
                result["message"] = messages.errorMsg.noItemsFound;
                result['offerproductdetails'] = offer_productslist;
                cb(null,result)
            }
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
    //* Offers Product List End *//
};
