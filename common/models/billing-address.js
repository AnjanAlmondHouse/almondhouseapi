'use strict';
let messages = require('../messages');
let { verifyToken, response } = require('../utils')
let app = require("../../server/server");
module.exports = function(Billingaddress) {
    Billingaddress.beforeRemote('addBillingAddress', function(ctx, result, next){
        let schema = ["first_name", "company", "address", 'city', 'pin_code', 'phone_number',"user_id","cart_id","country","state"];
        validations(ctx.req.body, schema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                next();
            }
        })
    });

    Billingaddress.addBillingAddress = function( requestData, req, res, cb){
        Billingaddress.create(requestData)
        .then(billingResp=>{
            let cart = app.models.user_cart
            cart.update({ id: requestData.cart_id}, {billing_address_id : billingResp.id })
            .then(cartAddUpdate => {
                cb(null,{
                    status : true,
                    statusCode: 200,
                    message: "Billing address added",
                    billingResp : billingResp
                })
            })
            .catch(cartAddErr => {
                console.log("cartAddErr",cartAddErr);
            cb(null,messages.systemErrorCallback)
            })
        })
        .catch(billingErr=>{
            console.log("billingErr",billingErr);
            cb(null,messages.systemErrorCallback)
        })
        
    }
    Billingaddress.remoteMethod("addBillingAddress",{
        http:{path: '/addBillingAddress', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add billing address',
        returns:{arg:"result", type:"object" }
    });
};
