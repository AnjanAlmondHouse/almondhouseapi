'use strict';
let { successMsg } = require('../messages');
const messages = require('../messages');
let { verifyToken, response, toFloat } = require('../utils');
let { validations, cartValidations } = require('../validations');
var async = require('async');
let app = require("../../server/server");
var db = app.datasources.postgresql.connector;
module.exports = function(Userpackageproducts) {
    //* remove items from packages Start //*
    Userpackageproducts.remoteMethod("deletePackageProductsFromCart", {
        http: { path: '/deletePackageProductsFromCart', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'object', required: true, http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can remove products from packages',
        returns: { arg: "result", type: "object" }
    });
     Userpackageproducts.beforeRemote('deletePackageProductsFromCart', function (ctx, result, next) {
        let requestData = ctx.req.body
        if (ctx.req.headers.userid) {
            let schema = ["user_package_product_id", "user_id", "user_has_packages_id"]
            verifyToken(ctx.req.headers.token, function (err, res) {
                if (err) {
                    ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
                } else {
                    validations(requestData, schema, (err, res) => {
                        if (err) {
                            ctx.res.json(response(false, messages.keyMissStatusCode, err))
                        }
                        else {
                            let userHasPackages = app.models.user_has_packages;
                            userHasPackages.findOne({ where: { user_id: requestData.user_id, id: requestData.user_has_packages_id } })
                                .then((cartResult) => {
                                    if (!cartResult)
                                        ctx.res.json(response(false, messages.errorMsg.noPackageFound))
                                    else {
                                        ctx.req.body.cart_total = Number(cartResult.cart_total)
                                        Userpackageproducts.find({
                                            where: {
                                                id: { inq: requestData.user_package_product_id },
                                                user_id: requestData.user_id,
                                                user_has_packages_id: requestData.user_has_packages_id
                                            }
                                        })
                                            .then((cartItemResp) => {
                                                // console.log(cartItemResp.length)
                                                if (cartItemResp.length == requestData.user_package_product_id.length) {
                                                    let sum = cartItemResp.reduce(function (accumulator, currentValue) {
                                                        return accumulator + toFloat(currentValue.total_price)
                                                    }, 0)
                                                    ctx.req.body.remove_items_total = sum
                                                    next()
                                                } else {
                                                    ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.cartProductsNotFound))
                                                }
                                            })
                                            .catch((cartItemsErr) => {
                                                console.log(cartItemsErr)
                                                ctx.res.json(messages.systemErrorCallback)
                                            })
                                    }
                                })
                                .catch((carterr) => {
                                    console.log(carterr)
                                    ctx.res.json(messages.systemErrorCallback)
                                })
                        }
                    })
                }
            })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        }
    });
    Userpackageproducts.deletePackageProductsFromCart = function (requestData, req, res, cb) {
        Userpackageproducts.destroyAll({
            id: { inq: requestData.user_package_product_id },
            user_id: requestData.user_id
        })
        .then(result => {
            let userHasPackages = app.models.user_has_packages;
            Userpackageproducts.find({ where: { user_has_packages_id: requestData.user_has_packages_id, user_id: requestData.user_id } })
                .then((cartItemsResult) => {
                    let price_obj = { "total_price": 0, "discount_amount": 0, "tax_total": 0 }
                    cartItemsResult.map((b) => {
                        price_obj["total_price"] += Number(b.price_after_discount)
                        price_obj["tax_total"] += Number(b.tax_total)
                        price_obj["discount_amount"] += Number(b.discount_amount)
                    })
                    userHasPackages.update(
                        { id: requestData.user_has_packages_id, user_id: requestData.user_id },
                        { "package_total": price_obj.total_price, "package_tax": price_obj.tax_total,
                        "coupon_code":null, "coupon_value":0.00}
                    )
                        .then((cartUpdResp) => {
                            res.json(response(true, messages.successStatusCode, messages.successMsg.removePackageCart))
                        })
                        .catch((cartUpdErr) => {
                            res.json(messages.systemErrorCallback)
                        })
                })
                .catch((cartItemsErr) => {
                    res.json(messages.systemErrorCallback)
                })
        })
        .catch(error => {
            res.json(messages.systemErrorCallback)
        })
    }
    //* remove items from packages End //*
};
