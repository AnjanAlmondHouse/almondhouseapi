'use strict';

module.exports = function(Email) {
    Email.remoteMethod('sendEmail', {			
        http:{path: '/sendEmail', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Search product details in json format',
        returns:{arg:"result", type:"object" }		
    });
    Email.sendEmail = function(req, res, cb) { 
        Email.send({
            to: "phani.d@axlrdata.com",
            from: "info@axlrdata.com",
            subject: "vedik Send Grid Email testing",
            text: "Some text",
            html: "<b>vedik Send Grid Email testing</b>",
        },
        function (err, result) {
        if(err) {
            console.log('[!] Error', err);
            return;
        }
        console.log(result,'--');
        });
    }
};
