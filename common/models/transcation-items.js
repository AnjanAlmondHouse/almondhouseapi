'use strict';
const {verifyToken,response,jsonCopy} = require("../utils")
const {validations} = require("../validations")
const messages     =   require("../messages")
let app = require('../../server/server');
let db = app.datasources.mongovedik.connector
let {getProductsRegionwise} = require("./site-offers")
module.exports = function(Transcationitems) {
    //* Suggested item details Start //*
    Transcationitems.remoteMethod("suggesteditemDetails",{
        http:{path: '/suggesteditemDetails', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Suggested Item details in json format',
        returns:{arg:"result", type:"object" }
    });

    Transcationitems.beforeRemote('suggesteditemDetails', function(ctx, result, next){
        let headerSchema = ['token','userid',"region"]
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                let schema = ["order_id"]
                verifyToken(ctx.req.headers.token, function(err, res){
                    if(err) {
                        ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.token_expired))
                    }else{
                        validations(ctx.req.query,schema, (err, res)=>{
                            if(err){
                                ctx.res.json(response(false, messages.keyMissStatusCode,err))
                            }
                            else
                                next()
                        })
                    }
                }) 
            }
        })  
    });
    Transcationitems.suggesteditemDetails = function(req, res, cb){
        Transcationitems.find({
            fields:['product_slug',"user_order_product_id"],
            where:{"user_order_product_id":req.query.order_id},
        })
        .then(function(userorderitems){
            var productslugs=[]
            userorderitems.map(function(row) {
                productslugs.push(row.product_slug)
            })
            let Products = app.models.products;
            Products.find({ 
                fields:['id','product_name','product_slug','product_cat'],
                where:{"product_slug" : {"inq" : productslugs},
                    "is_active":1
                },
            })
            .then(function(product_list){
                var catslugs=[]
                product_list.map(function(categoryslugs) {
                    categoryslugs.product_cat.map(function(cat_slugs) {
                        catslugs.push(cat_slugs.cat_slug)
                    })
                })
                var finalcat_slugs=[...new Set(catslugs)]
                Products.find({ 
                    fields:['id','product_name','product_slug',"product_images","region","product_sku","discount_percentage"],
                    where:{
                        "product_cat.cat_slug" : {"inq" : finalcat_slugs},
                        "product_slug":{"nin": productslugs},
                        "is_active":1,
                    },
                    limit :15,
                    include:{"relation": "Wishlist"}
                })
                .then(function(final_product_list){
                    let final_product_lists = jsonCopy(final_product_list)
                    final_product_lists.map(function(categorys, index){ 
                        if(categorys.Wishlist.length >0){
                            categorys.added_to_wish_list =true
                        }else{
                            categorys.added_to_wish_list =false
                        }
                        categorys.regions = categorys.region.find((a)=>{if(a.name == req.headers.region) return a })
                        delete categorys.region
                        delete categorys.Wishlist
                    }) 
                    if(final_product_lists.length>0){
                        let result = {"status":true, "statusCode":messages.successStatusCode};
                        result["message"] = messages.successMsg.suggestedProFound;
                        result["suggested_items"] = final_product_lists
                        cb(null, result)
                    }else{
                        let result = {"status":true, "statusCode":messages.successStatusCode};
                        result["message"] = messages.errorMsg.noItemsFound;
                        result['suggested_items'] = final_product_lists;
                        cb(null,result)
                    }
                })
                .catch(function (error) {
                    console.log(error)
                    cb(null,messages.systemErrorCallback)
                })
            })
            .catch(function (error) {
                console.log(error)
                cb(null,messages.systemErrorCallback)
            })
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
    //* Suggested item details End //*

    //* Cancel the item //*
    Transcationitems.beforeRemote('cancelItem', function(ctx, result, next){
        let headerSchema = ['token','userid']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                let schema = ["order_id","order_item_id","cancel_reason"]
                verifyToken(ctx.req.headers.token, function(err, res){
                    if(err) {
                        ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.token_expired))
                    }else{
                        validations(ctx.req.body,schema, (err, res)=>{
                            if(err){
                                ctx.res.json(response(false, messages.keyMissStatusCode,err))
                            }
                            else
                                next()
                        })
                    }
                }) 
            }
        })  
    });

    Transcationitems.cancelItem = function(cancelData, req, res, cb){
        console.log(req.headers);
        
        let Transactions = app.models.final_transcation
        Transactions.findOne({where:{id:cancelData.order_id, users_id: req.headers.userid},
            include: [{
                "relation": "ordered_items",
                scope: {
                    fields: ["product_id", 'product_slug', 'ordered_quantity', 'product_uom', "thumbnail_product_image", "product_name","final_transcation_id", "user_order_product_id", "product_price","total_price","master_status_id","master_status_name"],
                },
            },{
                "relation":"user_details"
            }
        ]
        })
        .then((orderResponse)=>{
            if(orderResponse){
                orderResponse = (orderResponse).toJSON()
                if(orderResponse.master_status_id == '2'){
                    console.log(cancelData.order_item_id,JSON.stringify(orderResponse));
                    Transcationitems.update({id:cancelData.order_item_id},{master_status_id:6,master_status_name:"cancelled",cancel_reason:cancelData.cancel_reason})
                    .then(async (deleteResp)=>{
                        console.log(deleteResp);
                        if(deleteResp.count > 0){
                            let total_orders_count = orderResponse.ordered_items.length;
                            let canceled_items = await orderResponse.ordered_items.reduce(( accumulater,obj)=>{
                                if(obj.master_status_id == 6)
                                    accumulater ++
                                return accumulater
                            },0)
                            console.log((total_orders_count == canceled_items + 1), total_orders_count,canceled_items + 1 );
                            if(total_orders_count == canceled_items + 1){
                                console.log("in if");
                                updateTransactionStatus(cancelData.order_id,'6', 'cancelled',cancelData.cancel_reason)
                                triggerEmail("cancelOrder",{to_email:orderResponse.user_details.user_email, subject:"Order Cancelled"},cancelOrderMail(orderResponse), (err, resp)=>{
                                    console.log(err, resp, "email triggering")
                                } )
                            }
                            triggerEmail("cancelItem",{to_email:orderResponse.user_details.user_email, subject:"Item Cancelled"},cancelItemMail(orderResponse), (err, resp)=>{
                                console.log(err, resp, "email triggering")
                            } )
                            console.log(canceled_items,total_orders_count );
                            res.json(response(true, messages.successStatusCode,messages.successMsg.orderItemCancelled))
                        }else{
                            res.json(response(false, messages.successStatusCode,"cannot cancel product"))
                        }
                    })
                    .catch((deleteErr)=>{
                        console.log(deleteErr);
                        
                        res.json(messages.systemErrorCallback)
                    })
                }else{
                    res.json(response(false, messages.invalidStatusCode,messages.errorMsg.cantCancelOrder))
                }
            }else{
                res.json(response(false, messages.invalidStatusCode,messages.errorMsg.noOrdersFound))
            }
        })
        .catch((orderErr)=>{
            console.log(orderErr);
            res.json(messages.systemErrorCallback)
        })

    }

    Transcationitems.remoteMethod("cancelItem",{
        http:{path: '/cancelItem', verb:"post"},
        accepts:[
            { arg: 'cancelData', type: 'object', required: true, http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Send valid json to cancel the item',
        returns:{arg:"result", type:"object" }
    });
    // End of cancel item

    //* Cancel the order begins //*
    Transcationitems.beforeRemote('cancelOrder', function(ctx, result, next){
        let headerSchema = ['token','userid']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                let schema = ["order_id","cancel_reason"]
                verifyToken(ctx.req.headers.token, function(err, res){
                    if(err) {
                        ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.token_expired))
                    }else{
                        validations(ctx.req.body,schema, (err, res)=>{
                            if(err){
                                ctx.res.json(response(false, messages.keyMissStatusCode,err))
                            }
                            else
                                next()
                        })
                    }
                }) 
            }
        })  
    });

    Transcationitems.cancelOrder = function(cancelData, req, res, cb){
        console.log(req.headers);
        
        let Transactions = app.models.final_transcation
        Transactions.findOne({where:{id:cancelData.order_id, users_id: req.headers.userid},
            include: [{
                "relation": "ordered_items",
                scope: {
                    fields: ["product_id", 'product_slug', 'ordered_quantity', 'product_uom', "thumbnail_product_image", "product_name","final_transcation_id", "user_order_product_id", "product_price","total_price","master_status_id","master_status_name"],
                },
            },{
                "relation":"user_details"
            }
        ]
        })
        .then((orderResponse)=>{
            if(orderResponse){
                orderResponse = (orderResponse).toJSON()
                if(orderResponse.master_status_id == '2'){
                    console.log(cancelData.order_id,JSON.stringify(orderResponse));
                    updateTransactionStatus(cancelData.order_id,6,'cancelled',cancelData.cancel_reason,(err, resp)=>{
                        console.log(resp);
                        if(resp.status){
                            updateTransactionItems(cancelData.order_id,6,'cancelled',cancelData.cancel_reason,(err, response)=>{
                                console.log(err, response);
                            })
                            triggerEmail("cancelOrder",{to_email:orderResponse.user_details.user_email, subject:"Order Cancelled"},cancelOrderMail(orderResponse), (err, resp)=>{
                                console.log(err, resp, "email triggering")
                            } )
                            res.json(response(true, messages.successStatusCode,messages.successMsg.orderCancelled))
                        }else{
                            res.json(response(false, messages.invalidStatusCode,resp.result))
                        }
                    })
                }else{
                    res.json(response(false, messages.invalidStatusCode,messages.errorMsg.cantCancelOrder))
                }
            }else{
                res.json(response(false, messages.invalidStatusCode,messages.errorMsg.noOrdersFound))
            }
        })
        .catch((orderErr)=>{
            console.log(orderErr);
            res.json(messages.systemErrorCallback)
        })

    }

    Transcationitems.remoteMethod("cancelOrder",{
        http:{path: '/cancelOrder', verb:"post"},
        accepts:[
            { arg: 'cancelData', type: 'object', required: true, http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Send valid json to cancel the item',
        returns:{arg:"result", type:"object" }
    });
    // End of cancel Order

    //* Return the order begins //*
    Transcationitems.beforeRemote('returnOrder', function(ctx, result, next){
        let headerSchema = ['token','userid']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                let schema = ["order_id","cancel_reason"]
                verifyToken(ctx.req.headers.token, function(err, res){
                    if(err) {
                        ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.token_expired))
                    }else{
                        validations(ctx.req.body,schema, (err, res)=>{
                            if(err){
                                ctx.res.json(response(false, messages.keyMissStatusCode,err))
                            }
                            else
                                next()
                        })
                    }
                }) 
            }
        })  
    });

    Transcationitems.returnOrder = function(cancelData, req, res, cb){
        console.log(req.headers);
        
        let Transactions = app.models.final_transcation
        Transactions.findOne({where:{id:cancelData.order_id, users_id: req.headers.userid},
            include: [{
                "relation": "ordered_items",
                scope: {
                    fields: ["product_id", 'product_slug', 'ordered_quantity', 'product_uom', "thumbnail_product_image", "product_name","final_transcation_id", "user_order_product_id", "product_price","total_price","master_status_id","master_status_name"],
                },
            },{
                "relation":"user_details"
            }
        ]
        })
        .then((orderResponse)=>{
            if(orderResponse){
                orderResponse = (orderResponse).toJSON()
                if(orderResponse.master_status_id == '5'){
                    console.log(cancelData.order_id,JSON.stringify(orderResponse));
                    updateTransactionStatus(cancelData.order_id,7,'return',cancelData.cancel_reason,(err, resp)=>{
                        console.log(resp);
                        if(resp.status){
                            updateTransactionItems(cancelData.order_id,7,'return',cancelData.cancel_reason,(err, response)=>{
                                console.log(err, response);
                            })
                            triggerEmail("returnOrder",{to_email:orderResponse.user_details.user_email, subject:"Order Return Initiated"},cancelOrderMail(orderResponse), (err, resp)=>{
                                console.log(err, resp, "email triggering")
                            } )
                            res.json(response(true, messages.successStatusCode,messages.successMsg.orderCancelled))
                        }else{
                            res.json(response(false, messages.invalidStatusCode,resp.result))
                        }
                    })
                }else{
                    res.json(response(false, messages.invalidStatusCode,messages.errorMsg.cantReturnOrder))
                }
            }else{
                res.json(response(false, messages.invalidStatusCode,messages.errorMsg.noOrdersFound))
            }
        })
        .catch((orderErr)=>{
            console.log(orderErr);
            res.json(messages.systemErrorCallback)
        })

    }

    Transcationitems.remoteMethod("returnOrder",{
        http:{path: '/returnOrder', verb:"post"},
        accepts:[
            { arg: 'cancelData', type: 'object', required: true, http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Send valid json to return the order',
        returns:{arg:"result", type:"object" }
    });
    // End of returnOrder

    function updateTransactionStatus(order_id, status_id, status_name,cancel_reason,cb){
        console.log(status_id, status_name);
        
        let Transactions = app.models.final_transcation
        Transactions.update({id:order_id},{master_status_id : status_id,status_name: status_name,cancel_reason:cancel_reason})
        .then(updateResult=>{
            cb(null,{status: true, result:updateResult})
        })
        .catch(updateErr=>{
            cb(null,{status: false, result:updateErr})
        })

    }
    function updateTransactionItems(order_id, status_id, status_name,cancel_reason,cb){
        console.log(status_id, status_name);
        Transcationitems.update({final_transcation_id:order_id,master_status_name:{nin:["cancelled","placed","shipped","packed"]} },{master_status_id : status_id,master_status_name: status_name,cancel_reason:cancel_reason})
        .then(updateResult=>{
            cb(null,{status: true, result:updateResult})
        })
        .catch(updateErr=>{
            cb(null,{status: false, result:updateErr})
        })

    }

    const cancelOrderMail = (orderDetails)=>{
        return `<h2>Payment recieved for Rs:${JSON.stringify(orderDetails)} </h2>`
    }
    const cancelItemMail = (orderDetails)=>{
        return `<h2>Payment recieved for Rs:${JSON.stringify(orderDetails)} </h2>`
    }

};
