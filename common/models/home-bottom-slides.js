'use strict';
let messages = require('../messages')
module.exports = function(Homebottomslides) {
 //* Home Page Bottom Slides Start *//
    Homebottomslides.remoteMethod("getHomebottomslides",{
        http:{path: '/getHomebottomslides', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get show offers in json format',
        returns:{arg:"result", type:"object" }
    });
    Homebottomslides.beforeRemote('getHomebottomslides', function(ctx, result, next){	
        next();
    });
    Homebottomslides.getHomebottomslides = function( req, res, cb){
        Homebottomslides.find({
            fields:['id','title','description','images','background_image'],
            where:{
                "is_active":1
            },
        })
        .then(function(home_footer_sliders){
            let result = {"status":true, "statusCode":messages.successStatusCode};
            result["message"] = "Home Page Footer Slides"
            result["home_footer_sliders"] = home_footer_sliders
            cb(null, result)
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
// * Home Page Bottom Slides End *//
};
