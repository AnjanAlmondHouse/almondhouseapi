'use strict';
let messages = require('../messages');
let app = require('../../server/server');
let {jsonCopy, response} = require("../utils")

module.exports = function(Categories) {
    //* Show List Of Categories Start *//
    Categories.remoteMethod("getCategorylist",{
        http:{path: '/getCategorylist', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Category list in json format',
        returns:{arg:"result", type:"object" }
    });
    Categories.beforeRemote('getCategorylist', function(ctx, result, next){
        ctx.req.query.limit =ctx.req.query.limit ?ctx.req.query.limit :8
        ctx.req.query.sorting_type =ctx.req.query.sorting_type ?ctx.req.query.sorting_type :"ASC"
		next();	
    });
    Categories.getCategorylist = function( req, res, cb){
            let products = app.models.products;
            products.find({
                fields:['product_cat'],
                where: { is_active:1 },
            })
            .then(function(product_cat_list){
                var catslugs=[]
                product_cat_list.map(function(categoryslugs) {
                    categoryslugs.product_cat.map(function(cat_slugs) {
                        catslugs.push(cat_slugs.cat_slug)
                    })
                })
                Categories.find({
                    fields:['id','cat_name','cat_code','cat_slug','priority','cat_image'],
                    order: 'cat_name ASC',
                    where: {
                        "is_active":1 ,
                        "cat_slug":{"inq" : [...new Set(catslugs)]},
                    },
                })
                .then(function(category_list){
                let result = {"status":true, "statusCode":messages.successStatusCode};
                result["message"] = "Category List"
                result["categories"] = category_list
                cb(null, result)
            })
            .catch(function (error) {
                console.log(error)
                cb(null,messages.systemErrorCallback)
            })
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
    //* Show List Of Categories End *//
    
    //* Show Category Details Start *//
    Categories.remoteMethod("getCategorydetails",{
        http:{path: '/getCategorydetails', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get Category details in json format',
        returns:{arg:"result", type:"object" }
    });
    Categories.beforeRemote('getCategorydetails', function(ctx, result, next){	
        let schema = ["cat_slug"];
        validations(ctx.req.query,schema, (err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                next();
            }
        })     
    });
    Categories.getCategorydetails = function( req, res, cb){
        Categories.findOne({
            fields:['id','cat_name','cat_code','cat_slug','priority','cat_image','cat_short_description','cat_description','cat_meta_data'],
            where:{"cat_slug":req.query.cat_slug,is_active:1},
        })
        .then(function(category_details){
            let result = {"status":true, "statusCode":messages.successStatusCode};
            result["message"] = `Category is ${category_details.cat_name}`
            result["categories"] = category_details
            cb(null, result)
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
    //* Show Category Details End *//
};
