'use strict';
const {hashPassword,bcrypt,verifyToken,token,response, triggerEmail } = require('../utils');
let {invalidCredentialsCallback,invalidUsername,invalidPassword,tokenExpiredcallback,
    successCallback,keyMissCallBack, missingKeys,message,systemErrorCallback,errorMsg,
    resultCallbacks} = require('../messages')
let messages = require('../messages');
let {validations} = require('../validations')
let app = require('../../server/server');
var db = app.datasources.postgresql.connector;
var loopback = require("loopback");
var path = require('path');
module.exports = function(Users) {

    //*  Login functionality Start //*
    Users.beforeRemote('userLogin', function(ctx, result, next) {
        let headerSchema = ["sessionid"]
        validateheaders(ctx.req.headers,headerSchema,(validErr, validResp)=>{
            if(validErr){
                ctx.res.json(response(false,messages.keyMissStatusCode,validErr))
            }else{
                let schema = ["user_email","password"]
                validations(ctx.req.body,schema, (err, res)=>{
                    if(err){
                        
                        keyMissCallBack.result['message'] = err;
                        ctx.res.json(keyMissCallBack)
                    }
                    else
                        next()
                })
            }
        })
    });
    Users.userLogin = function( requestData,req,res, cb) {
        Users.findOne({fields:["user_id","user_name","user_password","user_email","user_mobile"],
                        where:{or: [{user_email:requestData.user_email.toLowerCase() }, {user_mobile: requestData.user_email.toLowerCase()}]}})
        .then((result)=>{
            if(result && result.user_password ){
                let comparision = bcrypt.compareSync(requestData.password, result.user_password)
                if(!comparision) {
                    resultCallbacks.invalidCredentialsCallback.message = invalidPassword
                    cb(null,resultCallbacks.invalidCredentialsCallback)
                }else{
                    const payload = { user: result.user_name };
                    result = JSON.parse(JSON.stringify(result))
                    successCallback['user_details'] = JSON.parse(JSON.stringify(result)) 
                    successCallback['user_details']['token'] = token(payload);

                    assignCartToUser(req, result, function(cartErr, cartResp){
                        if(cartErr){
                            cb(null, cartErr)
                        }else{
                            cb(null, cartResp )
                        }
                    })
                    
                }
            }else{
                resultCallbacks.invalidCredentialsCallback['message'] = invalidUsername
                cb(null,resultCallbacks.invalidCredentialsCallback)
            }
        })
        .catch((error)=>{
            console.log(error,"------>")
            cb(null, resultCallbacks.systemErrorCallback)
        })
        
    }
    Users.remoteMethod("userLogin",{
        http:{path: '/userLogin', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can check jwt token status',
        returns:{arg:"result", type:"object" }
    });
    //*  Login functionality End //*
    function assignCartToUser(req, result, cb) {
        let UserCart = app.models.user_cart;
                    // UserCart.update({users_id:req.headers.sessionid,session_id:req.headers.sessionid},
                    //                 {users_id:})
                    UserCart.findOne({where:{users_id:req.headers.sessionid,session_id:req.headers.sessionid, is_active:1}})
                    .then(cartFindRes=>{
                        if(cartFindRes){
                            console.log(cartFindRes,"------>")
                            UserCart.findOrCreate({where:{users_id:result.user_id, is_active: 1},include: [{
                                "relation": "cart_list"}]},
                                {
                                    user_name:result.user_name,
                                    users_id: result.user_id,
                                    session_id: cartFindRes.session_id,
                                    cart_total: cartFindRes.cart_total,
                                    total_tax: cartFindRes.total_tax,
                                    delivery_charge: cartFindRes.delivery_charge,
                                    master_status_id: cartFindRes.master_status_id,
                                    status_name: cartFindRes.status_name,
                                    is_active : 1,
                                    location:req.headers.region

                                })
                            .then(insertResp=>{
                                console.log("insertResp",insertResp)
                                if (!insertResp[1]) {
                                    UserCart.update({where:{users_id:result.user_id, is_active:1}},
                                        {cart_total:Number(cartFindRes.cart_total) + Number(insertResp[0].cart_total),
                                            total_tax: Number(cartFindRes.total_tax) + Number(insertResp[0].total_tax), 
                                            delivery_charge:  Number(cartFindRes.delivery_charge) + Number(insertResp[0].delivery_charge)
                                        })
                                        .then((cartUpdate)=>{
                                            console.log("cartUpdate",cartUpdate,"user_id",result.user_id);
                                            let UserCartItems = app.models.user_cart_items;

                                            let user_cart_response = insertResp[0].toJSON()
                                            console.log(user_cart_response,"user_cart_response",{user_id:req.headers.sessionid, users_cart_id: cartFindRes.users_cart_id})
                                                UserCartItems.find({where:{user_id:req.headers.sessionid, users_cart_id: cartFindRes.users_cart_id}})
                                                .then(oldCartItems=>{
                                                    console.log(oldCartItems,"oldCartItems1");
                                                    oldCartItems.map((obj,index)=>{
                                                        console.log(obj,"oldCartItems2",user_cart_response.cart_list);
                                                        let item = user_cart_response.cart_list.find(e=>e.product_slug==obj.product_slug && e.product_uom==obj.product_uom)
                                                        console.log("item"+index,item,index);
                                                        if(item){
                                                            db.query(`update user_cart_items set ordered_quantity = ordered_quantity + ${obj.ordered_quantity}, discount_amount = discount_amount + ${obj.discount_amount},
                                                            price_after_discount = price_after_discount + ${obj.price_after_discount},total_price = total_price + ${obj.total_price}, 
                                                            tax_total = tax_total + ${obj.tax_total} where id = ${item.user_cart_product_id}`,function(err,resp){
                                                                if(err){
                                                                    console.log(err,"sai err");
                                                                    
                                                                }else{
                                                                    console.log(resp, "sai resp");
                                                                }
                                                            })      
                                                        }else{
                                                            UserCartItems.update({user_id:req.headers.sessionid, users_cart_id: cartFindRes.users_cart_id,id:obj.id},
                                                                {user_id:result.user_id,users_cart_id:insertResp[0].id})
                                                            .then((cartItemsResp)=>{
                                                                console.log("cartItemsResp",cartItemsResp)
                                                            })
                                                            .catch((cartErr)=>{
                                                                console.log("cartErr2",cartErr)
                                                                cb(resultCallbacks.systemErrorCallback)
                                                            })
                                                        }
                                                        
                                                    })
                                                })
                                            // UserCartItems.update({user_id:req.headers.sessionid, users_cart_id: cartFindRes.users_cart_id},
                                            //                         {user_id:result.user_id,users_cart_id:insertResp[0].id})
                                            //     .then((cartItemsResp)=>{
                                            //         console.log("cartItemsResp",cartItemsResp)
                                                    UserCart.update({users_id:cartFindRes.users_id},{is_active:0})
                                                    .then((finalupdate)=>{
                                                        console.log("cartItemsResp",finalupdate)
                                                        cb(null,successCallback)
                                                    })
                                                    .catch(FinalErr=>{
                                                        console.log("cartErr2",FinalErr)
                                                        cb(resultCallbacks.systemErrorCallback)
                                                    })
                                                // })
                                                // .catch((cartErr)=>{
                                                //     console.log("cartErr2",cartErr)
                                                //     cb(resultCallbacks.systemErrorCallback)
                                                // })

                                        })
                                        .catch((cartErr)=>{
                                            console.log("cartErr",cartErr)
                                            cb(resultCallbacks.systemErrorCallback)
                                        })
                                }else{
                                    console.log("user id",result.user_id,{user_id:req.headers.sessionid, users_cart_id: cartFindRes.users_cart_id})
                                    let UserCartItems = app.models.user_cart_items;
                                    UserCartItems.update({user_id:req.headers.sessionid, users_cart_id: cartFindRes.users_cart_id},
                                                            {user_id:result.user_id,users_cart_id:insertResp[0].id})
                                        .then((cartItemsResp)=>{
                                            console.log(cartItemsResp,"-----")
                                            UserCart.update({id:cartFindRes.users_cart_id},{is_active:0})
                                            .then((finalupdate)=>{
                                                console.log("cartItemsResp111",finalupdate)
                                                cb(null,successCallback)
                                            })
                                            .catch(FinalErr=>{
                                                console.log("cartErr21111",FinalErr)
                                                cb(resultCallbacks.systemErrorCallback)
                                            })
                                        })
                                        .catch((cartErr)=>{
                                            console.log("cartErr2",cartErr)
                                            cb(resultCallbacks.systemErrorCallback)
                                        })
                                }
                            })
                        }else{
                            cb(null,messages.successCallback)
                        }
                    })
                    .catch(cartFindErr=>{
                        console.log("cartFindErr",cartFindErr)
                        cb( resultCallbacks.systemErrorCallback)
                    })
    }

    function removeDuplicate(users_cart_id) {
        let UserCartItems = app.models.user_cart_items;
        UserCartItems.find({where:{users_cart_id: users_cart_id} })
        .then()
    }


    //* Verifing Jwt token End //*
    Users.checkJwt= (requestData,req,res, cb)=>{
        verifyToken(requestData.token, function(err, res){
            if(err) {
                cb(null,tokenExpiredcallback)
            }else{
                cb(null,res)
            }
        })
    }
   
    Users.remoteMethod("checkJwt",{
        http:{path: '/checkJwt', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can login here by giving his credentials and get jwt token in response',
        returns:{arg:"result", type:"object" }
    });
    //*  Verifing Jwt token End //*

    //* User registration //*

    Users.beforeRemote('userRegistration', function(ctx, result, next){
        let schema = ["user_name","user_mobile","user_email","user_password","confirm_password"];
        validations(ctx.req.body,schema, (err, res)=>{
            if(err){
                keyMissCallBack.result['message'] = err;
                ctx.res.json(keyMissCallBack)
            }
            else{
                if(ctx.req.body.user_password != ctx.req.body.confirm_password){
                    keyMissCallBack['result'][message] = missingKeys.confirm_password
                    ctx.res.json(keyMissCallBack)
                }else{
                    hashPassword(ctx.req.body.user_password, function(err, resp){
                        try {
                            ctx.req.body.hashPassword = resp.password;
                            next()
                        } catch (error) {
                            console.log("Error",error);
                            ctx.res.json(systemErrorCallback)
                        }
                        
                    })
                }
                
            }
        })
    });

    Users.userRegistration= (requestData,req,res, cb)=>{
        Users.findOrCreate({where:{or:[{user_email: requestData.user_email.toLowerCase()},{user_mobile: requestData.user_mobile}]}},{user_name:requestData.user_name, 
                    user_password: requestData.hashPassword, 
                    user_email:requestData.user_email.toLowerCase(),
                    user_mobile: requestData.user_mobile,
                    status: 1})
        .then((userResp)=>{
            if(!userResp[1]){
                resultCallbacks.existsCallBack[message] = errorMsg.user_exists;
                cb(null, resultCallbacks.existsCallBack)
            }else{
                triggerEmail("signup",{to_email:requestData.user_email.toLowerCase(), subject:"Welcome to Vedik Organics!"},userSignUpMsg(requestData.user_email,requestData.user_name), (err, resp)=>{
                    console.log(err, resp, " useremail triggering")
                } )
                triggerEmail("signup",{to_email:"info@axlrdata.com", subject:"New Registration on Vedik Organics"},userSignUpMsgToVedikTeam(requestData.user_email,requestData.user_name,requestData.user_mobile), (err, resp)=>{
                    console.log(err, resp, " useremail triggering")
                } )
                let payload = { user: requestData.user_name };
                delete userResp[0].user_password;
                successCallback['user_details'] = userResp[0];
                successCallback['user_details']['user_id'] = userResp[0]['id'];
                delete successCallback['user_details']['id'] 
                successCallback['user_details']['token'] = token(payload)
                assignCartToUser(req,userResp[0], function(userCartErr, userCartResp){
                    if(userCartErr){
                        cb(null, userCartErr)
                    }else{
                        cb(null, JSON.parse(JSON.stringify(successCallback)))
                    }
                } )
            }
        })
        .catch((userError)=>{
            console.log("Error",userError)
            cb(null, messages.systemErrorCallback)
            
        })

    }
    Users.remoteMethod("userRegistration",{
        http:{path: '/userRegistration', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can register and then he automatically logs in and generates a token',
        returns:{arg:"result", type:"object" }
    });

    //* User registration //*

    const userSignUpMsg = (email,username)=>{

        var renderer = loopback.template(path.resolve(__dirname, '../../client/usersignup.html'));
        var html = renderer({ title : 'html', username : username});
        return html
        // return `<h2>New User is registered with email ${email}</h2>`
    }
    const userSignUpMsgToVedikTeam = (email,username,mobile)=>{

        var renderer = loopback.template(path.resolve(__dirname, '../../client/usersignupvedikteam.html'));
        var html = renderer({ title : 'html', username : username,email:email,mobile:mobile});
        return html
        // return `<h2>New User is registered with email ${email}</h2>`
    }
    const forgetPassword = (username,token)=>{

        var renderer = loopback.template(path.resolve(__dirname, '../../client/forgetpassword.html'));
        var html = renderer({ title : 'html', username : username,token:token});
        return html
        // return `<h2>New User is registered with email ${email}</h2>`
    }
    
    
    //* User Details Start *//
    Users.remoteMethod("userDetails",{
        http:{path: '/userDetails', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User Details in json format',
        returns:{arg:"result", type:"object" }
    });
    Users.beforeRemote('userDetails', function(ctx, result, next){	
        let headerSchema = ['token','userid']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        messages.tokenExpiredcallback['error'] = err
                        ctx.res.json(messages.tokenExpiredcallback)
                    } else {
                        let schema = ["user_id"];
                        validations(ctx.req.query,schema, (err, res)=>{
                            if(err){
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }
                            else{
                                next();
                            }
                        })
                    }
                })
            }
        }) 
    });
    Users.userDetails = function( req, res, cb){
        Users.findOne({
            where:{"id":req.query.user_id},
            fields:['user_id',"user_name","user_email","user_mobile",'user_first_name','user_last_name','user_gender','user_dob']
        })
        .then(function(userdetails){
            cb(null, {status: true, statusCode: messages.successStatusCode, message: messages.successMsg.userDetails, userdetails: userdetails })
        })
        .catch(function (error) {
            console.log(error)
            cb(null,systemErrorCallback)
        })
    }
    //* User Details End //*  

    //* Update User Details Start //*
    Users.remoteMethod("updateUserDetails",{
        http:{path: '/updateUserDetails', verb:"post"},
        accepts:[
            {arg:"updateuserdetails", type: "object", http:{source:'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Update User Details in json format',
        returns:{arg:"result", type:"object" }
    });
    Users.beforeRemote('updateUserDetails', function(ctx, result, next){	
        let headerSchema = ['token','userid']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        messages.tokenExpiredcallback['error'] = err
                        ctx.res.json(messages.tokenExpiredcallback)
                    } else {
                        let schema = ["user_id","user_name","user_email","user_mobile"];
                        validations(ctx.req.body,schema, (err, res)=>{
                            if(err){
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }
                            else{
                                Users.findOne({
                                    where:{"id":ctx.req.body.user_id},
                                })
                                .then(function(updateduserdetails){
                                    if(updateduserdetails != null){
                                        Users.findOne({
                                            where:{ 
                                                and:[
                                                {or :[
                                                    {"user_email":ctx.req.body.user_email},
                                                    {"user_mobile":ctx.req.body.user_mobile}
                                                ]},
                                                {"id":{"neq": ctx.req.body.user_id}}],
                                            },
                                        })
                                        .then(function(updateduserdetails){
                                            if(updateduserdetails) 
                                            {
                                                let checkemailormobile 
                                                if(updateduserdetails.user_mobile == ctx.req.body.user_mobile && updateduserdetails.user_email == ctx.req.body.user_email){
                                                    checkemailormobile= messages.errorMsg.userEmailMobileExist
                                                }else if(updateduserdetails.user_mobile == ctx.req.body.user_mobile){
                                                    checkemailormobile= messages.errorMsg.userMobileExist
                                                }else if(updateduserdetails.user_email == ctx.req.body.user_email){
                                                    checkemailormobile= messages.errorMsg.userEmailExist
                                                }
                                                ctx.res.json(response(false, messages.invalidStatusCode,checkemailormobile))
                                            }else{
                                                next()
                                            }
                                        })
                                        .catch(function (error1) {
                                            console.log(error1)

                                            ctx.res.json(response(false, messages.errorStatusCode,messages.systemError))
                                        })
                                    }else{
                                        ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.userNotFound))
                                    }
                                 })
                                .catch(function (error) {
                                    ctx.res.json(response(false, messages.errorStatusCode,messages.systemError))
                                })
                            }
                        })
                    }
                })
            }
        })
    });
    Users.updateUserDetails = function(updateuserdetails, req, res, cb){
        Users.update(
            {"id":updateuserdetails.user_id},{
            "user_name":updateuserdetails.user_name,
            "user_email":updateuserdetails.user_email,
            "user_mobile":updateuserdetails.user_mobile  
        })
        .then(function(Updateuser){
            Users.findOne({
                where:{"id":updateuserdetails.user_id},
                fields:['user_id',"user_name","user_email",'user_first_name','user_last_name','user_gender','user_dob',"user_mobile"]
            })
            .then(function(updateduserdetails){
                triggerEmail("updateUserDetails",{to_email:updateuserdetails.user_email, subject:"Update User Details"},updateUserMsg(updateuserdetails.user_email), (err, resp)=>{
                    console.log(err, resp, "email triggering")
                } )
                cb(null, {status: true, statusCode: messages.successStatusCode, message: messages.successMsg.userdetailsSuccess, Updated_user_details: updateduserdetails })
             })
            .catch(function (error) {
                cb(null,systemErrorCallback)
            })
        })
        .catch(function (error) {
            console.log(error)
            cb(null,systemErrorCallback)
        })
    }

    const updateUserMsg  = (email)=>{
        return `<h2>Your details have been updated</h2>`
    }
    //* Update User Details End //*
    //* Change User Password Start //*
    Users.remoteMethod("changeuserPassowrd",{
        http:{path: '/changeuserPassowrd', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Change User Password in json format',
        returns:{arg:"result", type:"object" }
    });

    Users.beforeRemote('changeuserPassowrd', function (ctx, result, next) {
        let requestData = ctx.req.body
        let headerSchema = ['token', 'userid']
        validateheaders(ctx.req.headers, headerSchema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        messages.tokenExpiredcallback['error'] = err
                        ctx.res.json(messages.tokenExpiredcallback)
                    } else {
                        let schema = ["user_id","old_password","new_password","confirm_password"];
                        validations(requestData, schema, (err, res) => {
                            if (err) {
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }
                            else {
                                Users.findOne({
                                    where:{"id":requestData.user_id},
                                    fields:['user_id',"user_name","user_email","user_mobile",'user_gender','user_password']
                                })
                                .then(function(userdetails){
                                    let comparision = bcrypt.compareSync(requestData.old_password, userdetails.user_password)
                                    if(!comparision) {
                                        ctx.res.json(response(false, messages.invalidStatusCode, messages.missingKeys.olduser_password))
                                    }else{
                                        if(requestData.new_password != requestData.confirm_password) {
                                            keyMissCallBack['result'][message] = missingKeys.confirm_password
                                            ctx.res.json(keyMissCallBack)
                                        }else{
                                            hashPassword(requestData.new_password, function(err, resp){
                                                try {
                                                    requestData.hashPassword = resp.password;
                                                    next()
                                                } catch (error) {
                                                    ctx.res.json(systemErrorCallback)
                                                }   
                                            })
                                        }
                                    }
                                })
                                .catch(function (error) {
                                    console.log(error)
                                    ctx.res.json(response(false, messages.errorStatusCode,messages.systemError))
                                })
                            }
                        })
                    }
                })
            }
        })
    });
    Users.changeuserPassowrd = function(requestData, req, res, cb){
        Users.update(
            {"id":requestData.user_id},{
            "user_password":requestData.hashPassword,  
        })
        .then(function(Updateuser){ 
            cb(null, {status: true, statusCode: messages.successStatusCode, message: messages.successMsg.changePassowrd })
        })
        .catch(function (error) {
            res.json(response(false, messages.errorStatusCode,messages.systemError))
        })
    }
    //* Change User Password End //*
    
    //* Forgot User Password Start //*
    Users.remoteMethod("forgotPassowrd",{
        http:{path: '/forgotPassowrd', verb:"post"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'forgot Password in json format',
        returns:{arg:"result", type:"object" }
    });
    Users.beforeRemote('forgotPassowrd', function (ctx, result, next) {
        if (ctx.req.body.user_email) {
            next();
        }else {
            ctx.res.json(response(false, messages.invalidStatusCode, messages.missingKeys.emailormobile))
        }
    })           
    Users.forgotPassowrd = function(req, res, cb){
        Users.findOne({
            where:{
                or: [{user_email:req.body.user_email },
                     {user_mobile: req.body.user_email}]
            },
            fields:['user_id',"user_name","user_email","user_mobile",'user_gender','user_password']
        })
        .then(function(userdetails){
            if(userdetails!=null){
                bcrypt.genSalt(20, function(err2, buf){
                    if(err2){
                        console.log('error', err2)
                    }else{
                        var tokenGen = buf.toString('hex');
                        var expireDate = Date.now() + 900000;
                        // console.log(global.token, expireDate)
                        global.token = tokenGen.replace(/\\|\//g,'') ;
                        console.log("ExpireData",expireDate);
                        const Token = app.models.token;
                        Token.create({
                            "user_id":userdetails.user_id,
                            "user_token":global.token,
                            "status": 0,
                            "expiredate" : expireDate
                        })
                        .then(function(inserttoken){
                            let link = 'https://vasu.life/forgotpassword/resetPassword/'+global.token
                            let Email = app.models.email;
                            Email.send({
                                to: userdetails.user_email,
                                from: "info@vedik.com",
                                subject: "Vedik Organics Account Password Reset",
                                text: "Some text",
                                html: forgetPassword(userdetails.user_name,link),
                                // html: `<h2>Hello,${userdetails.user_name}<br> We received a request to reset your password. Click the link below to choose a new one:</h2>
                                // <a href = "${link}"><h3 style="color: teal; text-align: center">Reset Password</h3></a>`,
                            },
                            function (err, result) {
                                if(err) {
                                    console.log('[!] Error', err);
                                    res.json(response(false, messages.errorStatusCode,messages.systemError))
                                }
                                    cb(null, {status: true, statusCode: messages.successStatusCode, message: messages.successMsg.PassowrdResetLink })
                            });
                        })
                        .catch(function (error) {
                            console.log(error)
                            res.json(response(false, messages.errorStatusCode,messages.systemError))
                        })
                    }
                })
            }else{
                res.json(response(false, messages.invalidStatusCode, messages.errorMsg.emailnotFound))
            }
        }) 
        .catch(function (error) {
            console.log(error)
            res.json(response(false, messages.errorStatusCode,messages.systemError))
        })
    }
    //* Forgot User Password End //*
    //* Reset User Password Start //*
    Users.remoteMethod("resetPassowrd",{
        http:{path: '/resetPassowrd', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Reset Password in json format',
        returns:{arg:"result", type:"object" }
    });
    Users.beforeRemote('resetPassowrd', function(ctx, result, next) {	
        let schema = ["token","password","confirm_password"];
        let requestData = ctx.req.body
        // console.log(requestData,'req')
        validations(requestData,schema, (err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                const Token = app.models.token;
                Token.findOne({where:{user_token: requestData.token}})
                .then(function(tokenresp){
                    if(tokenresp && tokenresp.expiredate < Date.now()){
                        ctx.res.json(response(false, messages.invalidStatusCode, messages.missingKeys.tokenexpired))
                    }else{
                        if(requestData.password != requestData.confirm_password) {
                            keyMissCallBack['result'][message] = missingKeys.confirm_password
                            ctx.res.json(keyMissCallBack)
                        }else{
                            hashPassword(requestData.password, function(err, resp){
                                try {
                                    Token.update(
                                        {"user_token":requestData.token},{
                                        "status":1 ,  
                                    })
                                    .then(function(Updateuser){ 
                                        console.log(Updateuser,'Updateuser')
                                        requestData.user_id = tokenresp.user_id
                                        requestData.hashPassword = resp.password;
                                        next()
                                    })
                                    .catch(function (error) {
                                        res.json(response(false, messages.errorStatusCode,messages.systemError))
                                    })
                                    
                                } catch (error) {
                                    console.log(error)
                                    ctx.res.json(systemErrorCallback)
                                }   
                            })
                        }
                    }
                })
            }
        })              
    });
    Users.resetPassowrd = function(requestData, req, res, cb) {
        // console.log(requestData,'requestData')
        Users.update(
            {"id":requestData.user_id},{
            "user_password":requestData.hashPassword,  
        })
        .then(function(Updateuser){ 
            Users.findOne({
                where:{"id":requestData.user_id},
                fields:['user_id',"user_name","user_email",'user_first_name','user_last_name','user_gender','user_dob',"user_mobile","user_password"]
            })
            .then(function(updateduserdetails){
                cb(null, {status: true, statusCode: messages.successStatusCode, message: messages.successMsg.changePassowrd, user_details: updateduserdetails })
             })
            .catch(function (error) {
                res.json(response(false, messages.errorStatusCode,messages.systemError))
            })
        })
        .catch(function (error) {
            res.json(response(false, messages.errorStatusCode,messages.systemError))
        })
    }
    //* Reset User Password End //*
};
