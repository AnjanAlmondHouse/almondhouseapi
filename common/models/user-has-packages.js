'use strict';
const messages = require('../messages');
let {verifyToken,response} = require('../utils');
let {validations} = require('../validations');
let app = require("../../server/server");
var async = require('async');
var db = app.datasources.postgresql.connector;
let { successMsg } = require('../messages');
module.exports = function(Userhaspackages) {
    //* Insert User Has Packages Start //*
    Userhaspackages.remoteMethod("insertUserPackages",{
        http:{path: '/insertUserPackages', verb:"post"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Insert User Packages Details in json format',
        returns:{arg:"result", type:"object" }
    });
    Userhaspackages.beforeRemote('insertUserPackages', function(ctx, result, next){
        let requestData = ctx.req.body
        let headerSchema = ['token', 'userid']
        validateheaders(ctx.req.headers, headerSchema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
                    } else {
                        let schema = ["user_id", "user_name", "package_total", "package_tax", "discount_percentage",
                            "discount_amount", "coupon_code", "user_address_id", "package_id", "package_name", "package_slug",
                            "package_cycle", "package_actual_count", "delivery_start_date", "package_items"]
                        validations(requestData, schema, (err, res) => {
                            if (err) {
                                ctx.res.json(response(false, messages.keyMissStatusCode, err))
                            }
                            else
                                next()
                        })
                    }
                })
            }
        })        
    });
    Userhaspackages.insertUserPackages = function(requestData,req, res, cb){
        Userhaspackages.findOrCreate({ where: { 'user_id': requestData.user_id,  is_active:1 }},{
            "user_id":requestData.user_id,
            "user_name":requestData.user_name,
            "status_id": 1,
            "status_name": "pending",
            "is_active": 1,
            "package_id":requestData.package_id,
            "package_name":requestData.package_name, 
            "package_slug":requestData.package_slug,
            "package_cycle":requestData.package_cycle,
            "package_actual_count":requestData.package_actual_count,
            "delivery_start_date":requestData.delivery_start_date
        })
        .then(function(userpackages){
            const UserPackageProduct = app.models.user_package_products;
            async.forEach(requestData.package_items, function (products, cb) {
                let userpackages_id = userpackages[0].id;
                let userid = userpackages[0].user_id;
                let discount_amount = Number(products.discount_percentage * products.product_price * products.ordered_quantity) / 100;
                let price_after_discount = (products.product_price * products.ordered_quantity) - discount_amount
                getTaxOfProduct(products.product_slug, products.product_price).then((tax_value) => {
                    let total_tax = tax_value * products.ordered_quantity
                    let cartItemsInsertData = {
                        user_has_packages_id: userpackages_id,
                        user_id: userid,
                        product_slug: products.product_slug,
                        product_uom: products.uom,
                        product_id:  products.product_id,
                        product_name: products.product_name,
                        product_price: products.product_price,
                        discount_percentage: products.discount_percentage,
                        ordered_quantity: products.ordered_quantity,
                        discount_amount: discount_amount,
                        price_after_discount: price_after_discount,
                        tax_total: total_tax,
                        total_price: price_after_discount + total_tax,
                        thumbnail_product_image: products.thumbnail_product_image
                    }
                    UserPackageProduct.findOrCreate({
                        where: {
                            user_has_packages_id: userpackages_id,
                            product_slug: products.product_slug,
                            product_uom: products.uom,
                            user_id: userid,
                        }
                    }, cartItemsInsertData)
                        .then((cartItemsInsertResp) => {
                            if (!cartItemsInsertResp[1]) {
                                if(products.ordered_quantity == 0){
                                    DeleteItemFromCart(userpackages[0], cartItemsInsertResp[0], products, function (delErr, delResp) {
                                        cb(delErr, delResp)
                                    })
                                }else{
                                    updateItemsInCart(userpackages[0], cartItemsInsertResp[0], products, function (err, resp) {
                                        // console.log(err,resp, "update cart items")
                                        if (err) {
                                            cb(err)
                                        } else {
                                            cb(false,resp )
                                        }
                                    })
                                }
                            } else {
                                updateCartValue(userpackages[0], null, cartItemsInsertData)
                                cb(false,cartItemsInsertData )
                            }
                        })
                        .catch((cartItemsInsertErr) => {
                            console.log("errot",cartItemsInsertErr)
                            cb(cartItemsInsertErr)
                        })
                })
                .catch((taxErr)=>{
                    console.log("taxErr")
                    console.log(taxErr)
                })
            }, function(err, response){
                let result = {"status":true, "statusCode":messages.successStatusCode};
                result["message"] = "User Package Saved"
                cb(null, result)
            })
          
        })
        .catch(function (error) {
            console.log(error)
            cb(null,messages.systemErrorCallback)
        })
    }
    function getTaxOfProduct(product_slug, price) {
        try {
            let products = app.models.products;
            let total_tax = 1;
            return new Promise(function (resolve, reject) {
                products.findOne({ where: { product_slug: product_slug } }, (err, productResp) => {
                    if (err) {
                        reject(0)
                    }
                    else {
                        // total_tax = productResp?productResp.taxes.reduce((a, b) => a + (Number(b.tax_percentage) * Number(price)) / 100, 0):0
                        // resolve(total_tax)
                        resolve(0)
                    }
                })
            })
        } catch (error) {
            console.log("error",error)
            return 0
        }
    }

    function updateCartValue(userpackages, oldProductData, newProductData) {
        console.log(userpackages.user_id,userpackages.id)
        console.log(`update user_has_packages set package_tax = package_tax + ${Number(newProductData.tax_total) - Number(oldProductData ? oldProductData.tax_total : 0)}, package_total = package_total + ${Number(newProductData.price_after_discount) - Number(oldProductData ? oldProductData.price_after_discount : 0)} where id = ${userpackages.id} and user_id = '${userpackages.user_id}' `)
        db.query(`update user_has_packages set package_tax = package_tax + ${Number(newProductData.tax_total) - Number(oldProductData ? oldProductData.tax_total : 0)}, package_total = package_total + ${Number(newProductData.price_after_discount) - Number(oldProductData ? oldProductData.price_after_discount : 0)}, coupon_code = null, coupon_amount = 0.00 where id = ${userpackages.id} and user_id = '${userpackages.user_id}' `, (err, resp)=>{
            console.log("update user package", err, resp);
            return true
        })
    }
    function updateItemsInCart(userpackages, oldProductData, requestData, cb) {
        let discount_amount = Number(requestData.discount_percentage * requestData.product_price * requestData.ordered_quantity) / 100;
        let price_after_discount = (requestData.product_price * requestData.ordered_quantity) - discount_amount
        getTaxOfProduct(requestData.product_slug, requestData.product_price).then((tax_value) => {
            let total_tax = tax_value * requestData.ordered_quantity
            let cartItemsUpdateData = {
                ordered_quantity: requestData.ordered_quantity,
                discount_amount: discount_amount,
                price_after_discount: price_after_discount,
                tax_total: total_tax,
                total_price: price_after_discount + total_tax,
            }
            const UserPackageProduct = app.models.user_package_products;
            UserPackageProduct.update({ user_has_packages_id: userpackages.id, id: oldProductData.id },
                cartItemsUpdateData)
                .then((updateRes) => {
                    updateCartValue(userpackages, oldProductData, cartItemsUpdateData)
                    cb(null, { message: successMsg.updateCart })
                })
                .catch((updateResp) => {
                    console.log("updateResp",updateResp)
                    cb(updateResp, null)
                })
        })
    }
    function DeleteItemFromCart(userpackages, oldProductData, requestData, cb){
        let discount_amount = Number(requestData.discount_percentage * requestData.product_price * requestData.ordered_quantity) / 100;
        let price_after_discount = (requestData.product_price * requestData.ordered_quantity) - discount_amount
        UserPackageProduct(requestData.product_slug, requestData.product_price).then((tax_value) => {
            let total_tax = tax_value * requestData.ordered_quantity
            let cartItemsUpdateData = {
                ordered_quantity: requestData.ordered_quantity,
                discount_amount: discount_amount,
                price_after_discount: price_after_discount,
                tax_total: total_tax,
                total_price: price_after_discount + total_tax,
            }
            const UserPackageProduct = app.models.user_package_products;
            UserPackageProduct.destroyAll({ user_has_packages_id: userpackages.id, id: oldProductData.id })
                .then((deleteResp) => {
                    console.log("deleteResp",deleteResp)
                    updateCartValue(userpackages, oldProductData, cartItemsUpdateData)
                    cb(null, { message: "removed from cart" })
                })
                .catch((deleteErr) => {
                    console.log("updateResp",deleteErr)
                    cb(deleteErr, null)
                })
        })
    }
    //* Insert User Has Packages End //*
};
