'use strict';
const { verifyToken, response, triggerEmail,jsonCopy } = require("../utils")
const { validations } = require("../validations")
const messages = require("../messages");
let app = require('../../server/server');
var path = require('path');
var loopback = require("loopback");

module.exports = function (Finaltranscation) {
    //* Show List Of User Orders Start *//
    Finaltranscation.remoteMethod("userOrders", {
        http: { path: '/userOrders', verb: "get" },
        accepts: [
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get User Order details in json format',
        returns: { arg: "result", type: "object" }
    });
    Finaltranscation.beforeRemote('userOrders', function (ctx, result, next) {
        let headerSchema = ['token','userid']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{            let schema = ["user_id"]
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
                    } else {
                        validations(ctx.req.query, schema, (err, res) => {
                            if (err) {
                                ctx.res.json(response(false, messages.keyMissStatusCode, err))
                            }
                            else{
                                let Users = app.models.users
                                Users.findOne({
                                    where:{id:ctx.req.query.user_id}
                                })
                                .then(userResp=>{
                                    if(userResp){
                                        ctx.req.query.user_mobile = userResp.user_mobile
                                        ctx.req.query.user_email = userResp.user_email
                                        console.log(userResp,"userResp");
                                        next()
                                    }else{
                                        ctx.res.json(response(false, messages.keyMissStatusCode, "No user found"))
                                    }
                                })
                                .catch(userErr=>{
                                    ctx.res.json(messages.systemErrorCallback)
                                })
                            }
                        })
                    }
                })
            }
        }) 
    });
    Finaltranscation.userOrders = function (req, res, cb) {
        Finaltranscation.find({
            fields: ['order_id', 'users_id', 'bill_number', "invoice_number", "ordered_date", "delivered_date", "expected_delivered_date", "status_name", "grand_total"],
            where: { 
                or:[{"users_id": req.query.user_id},
                {
                    and:[{"users_id":0},
                    {or:[{email:req.query.user_email},{number : req.query.user_mobile}]}
                    ]}
                ] },
            include: {
                "relation": "ordered_items",
                scope: {
                    fields: ["product_id", 'product_slug', 'orderd_quantity', 'product_uom', "thumbnail_product_image", "product_name", "user_order_product_id"],
                }
            },
            order: 'created_at DESC'
        })
            .then(function (userorderitems) {
                if (userorderitems.length > 0) {
                    let result = { "status": true, "statusCode": messages.successStatusCode };
                    result["message"] = `orders found`
                    result["orders"] = userorderitems
                    cb(null, result)
                } else {
                    let result = { "status": true, "statusCode": messages.successStatusCode };
                    result["message"] = messages.errorMsg.noOrdersFound;
                    result['orders'] = userorderitems;
                    cb(null, result)
                }
            })
            .catch(function (error) {
                console.log(error)
                cb(null, messages.systemErrorCallback)
            })
    }
    //* Show List Of User Orders End *//

    //* User Order By Bill Items List Start *//
    Finaltranscation.remoteMethod("userOrderDetails", {
        http: { path: '/userOrderDetails', verb: "get" },
        accepts: [
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get User Order details in json format',
        returns: { arg: "result", type: "object" }
    });
    Finaltranscation.beforeRemote('userOrderDetails', function (ctx, result, next) {
        let headerSchema = ['token', 'userid']
        // validateheaders(ctx.req.headers, headerSchema, (err, res) => {
        //     if (err) {
        //         messages.keyMissCallBack.result['message'] = err;
        //         ctx.res.json(messages.keyMissCallBack)
        //     }
        //     else {
                let schema = ["user_id", "order_id"]
        //         verifyToken(ctx.req.headers.token, function (err, res) {
        //             if (err) {
        //                 ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
        //             } else {
                        validations(ctx.req.query, schema, (err, res) => {
                            if (err) {
                                ctx.res.json(response(false, messages.keyMissStatusCode, err))
                            }
                            else
                                next()
                        })
        //             }
        //         })
        //     }
        // })
    });

    Finaltranscation.userOrderDetails = function (req, res, cb) {
        Finaltranscation.find({
            fields: ['order_id', 'users_id', "invoice_number", "ordered_date", "delivered_date", "expected_delivered_date", "status_name", "grand_total", "user_address_id", "coupon_code", "coupon_value", "delivery_charge", "cart_total"],
            where: {"order_id": req.query.order_id },
            include: [
                {
                    "relation": "delivery_details",
                    scope: {
                        fields: ["id", "address", 'city','user_name', 'state', 'landmark', "zip_code", "mobile", "country"],
                    }
                },
                {
                    "relation": "ordered_items",
                    scope: {
                        fields: ["product_id", 'product_slug', 'ordered_quantity', 'product_uom', "thumbnail_product_image", "product_name","final_transcation_id", "user_order_product_id", "product_price","total_price","master_status_id","master_status_name"],
                    }
                },
            ]
        })
            .then(function (userorderitems) {
                console.log(userorderitems)
                if (userorderitems.length > 0) {
                    let result = { "status": true, "statusCode": messages.successStatusCode };
                    result["message"] = `orders found`
                    result["orderDetails"] = userorderitems[0]
                    cb(null, result)
                } else {
                    let result = { "status": true, "statusCode": messages.successStatusCode };
                    result["message"] = messages.errorMsg.noOrdersFound;
                    result['orderDetails'] = {};
                    cb(null, result)
                }
            })
            .catch(function (error) {
                console.log(error)
                cb(null, messages.systemErrorCallback)
            })
    }
    //* User Order By Bill Items List End *//

    
    Finaltranscation.beforeRemote("paymentSummary", function (ctx, result, next) {
        let requestData = ctx.req.body
        // if (ctx.req.headers.token && ctx.req.headers.userid) {
            let schema = ["user_id", "transaction_id", "transaction_status", "payment_mode", "user_cart_id"]
            // verifyToken(ctx.req.headers.token, function (err, res) {
            //     if (err) {
            //         ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
            //     } else {
                    // validations(requestData, schema, (err, res) => {
                    //     if (err) {
                    //         ctx.res.json(response(false, messages.keyMissStatusCode, err))
                    //     }
                    //     else {
                            // if(ctx.req.headers.region == "india" && requestData.payment_mode == "COD"){
                            //     ctx.res.json(response(false, messages.keyMissStatusCode, "Cannot select COD for this location"))
                            // }else{
                                next()
                            // }
                    //     }
                    // })
            //     }
            // })
        // } else {
        //     ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        // }
    });

    Finaltranscation.paymentSummary = function (requestData, req, res, cb) {
        let Usercart = app.models.user_cart;
        Usercart.findOne({
            where: {
                // "users_id": requestData.user_id,
                "is_active": 1,
                "id": requestData.user_cart_id
            },
            include: [{ "relation": "cartAddress" },{
                "relation": "cart_list",}],
            fields: { "id": false, "created_at": false, "updated_at": false }
        })
            .then((cartResult) => {
                console.log("cartResult",cartResult,typeof(requestData.user_id),requestData.user_id)
                if (cartResult) {
                    if(cartResult.users_id == cartResult.session_id){
                        cartResult.users_id = 0
                    }
                    const cart = cartResult.toJSON();
                    const invoice = generate_invoice()
                    cartResult.users_id = cartResult.users_id;
                    cartResult["transaction_number"] = requestData.transaction_id;
                    cartResult["transaction_status"] = requestData.transaction_status;
                    cartResult["payment_mode"] = requestData.payment_mode;
                    cartResult["invoice_number"] = invoice
                    cartResult["status_name"] = "placed";
                    cartResult["master_status_id"] = 2;
                    cartResult["email"] = cart.cartAddress.email;
                    cartResult["number"] = cart.cartAddress.mobile;
                    cartResult['user_name'] = cart.cartAddress.user_name
                    cartResult['created_at'] = new Date().toLocaleString('en-US', {timeZone: 'Asia/Calcutta'});
                    Finaltranscation.create(cartResult)
                        .then((transcResp) => {
                            let Usercartitems = app.models.user_cart_items;
                            Usercartitems.find({
                                where: {
                                    users_cart_id: requestData.user_cart_id
                                },
                                fields: {
                                    created_at: false,
                                    updated_at: false,
                                    id: false,
                                    user_cart_product_id: false,
                                    users_cart_id: false
                                }
                            })
                                .then((cartProductsResp) => {
                                    cartProductsResp.map((a) => {
                                        a.final_transcation_id = transcResp.id;
                                        a.master_status_id = transcResp.master_status_id;
                                        a.master_status_name = transcResp.status_name;
                                        a["users_id"] =  cartResult.users_id
                                        return a
                                    })
                                    let TransactionItems = app.models.transcation_items;
                                    TransactionItems.create(cartProductsResp)
                                        .then((TransactionItemsResp) => {
                                            Usercart.update(
                                                {
                                                    // "users_id": requestData.user_id,
                                                    "is_active": 1,
                                                    "id": requestData.user_cart_id
                                                },
                                                {
                                                    "is_active": 0,
                                                    "master_status_id": 2,
                                                    "status_name": "placed"
                                                }
                                            )
                                                .then((cartUpdateResp) => {
                                                    Finaltranscation.findOne({
                                                        where: { "id": transcResp.id },
                                                        include: ["ordered_items","delivery_details"]
                                                    })
                                                        .then(async transItemsResp => {
                                                            console.log("TranscactionItemsResponse",transItemsResp);
                                                            var JSONParsetransItemsResp = jsonCopy(transItemsResp);
                                                            var DeliveryAddress = JSONParsetransItemsResp.delivery_details;
                                                            // console.log("------------------------------------");
                                                            // console.log("DeliveryAddressAnjan",JSONParsetransItemsResp.delivery_details);
                                                            // console.log("DeliveryAddressAnjan",JSONParsetransItemsResp.delivery_details.user_name);
                                                            // console.log("cartResult[]",cartResult["email"]);
                                                            // cartResult["email"]

                                                            let Products = app.models.products
                                                            let transItem = JSONParsetransItemsResp.ordered_items
                                                            const u = (transItem,region) => {
                                                                return new Promise((resolve)=>{
                                                                    Products.findOne({where:{product_slug:transItem.product_slug}},function(err,productsResp){
                                                                        productsResp.region.map(el=>{
                                                                            if(el.name == region){
                                                                                el.uoms.map(e=>{
                                                                                    if(e.uom == transItem.product_uom){
                                                                                        console.log(transItem.product_uom,"--")
                                                                                        e.available_qty = e.available_qty - transItem.ordered_quantity
                                                                                        console.log(e.available_qty)
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                        resolve(productsResp)
                                                                    })
                                                                })
                                                                
                                                            }
                                                            const updateProduct = (transItem,productsResp) =>{
                                                                return new Promise(resolve=>{
                                                                    Products.update({product_slug:transItem.product_slug},{region:productsResp.region},function(err,resp){
                                                                        resolve(resp)
                                                                    })
                                                                })
                                                            }
                                                            try {
                                                                for(var i in transItem){
                                                                    const product = await u(transItem[i],req.headers.region)
                                                                    if(product){
                                                                        const update = await updateProduct(transItem[i],product)
                                                                        console.log(update,"update",i)
                                                                    }
                                                                }
                                                            } catch (error) {
                                                                console.log("error",error)
                                                            }
                                                            triggerEmail("makepayment",{to_email:cartResult["email"], subject:"Your Vedik Organics Order Confirmation (Order no. VO-"+transItemsResp.order_id+")"},cartPayment(cartResult,invoice,DeliveryAddress,transItemsResp.delivery_charge,transItemsResp.order_id), (err, resp)=>{
                                                                // console.log(err, resp, "email triggering")
                                                            } )
                                                            triggerEmail("makepayment",{to_email:"info@vedik.com", subject:"New Order Received (Order no. VO-"+transItemsResp.order_id+")"},admincartPayment(cartResult,invoice,DeliveryAddress,transItemsResp.delivery_charge,transItemsResp.order_id), (err, resp)=>{
                                                                // console.log(err, resp, "email triggering")
                                                            } )
                                                            res.json({result:{
                                                                status: true,
                                                                statusCode: messages.successStatusCode,
                                                                message: messages.successMsg.paymentSuccess,
                                                                orderSummary: transItemsResp
                                                            }})
                                                        })
                                                        .catch((transErr) => {
                                                            console.log(transErr)
                                                            res.json(messages.systemErrorCallback)
                                                        })
                                                })
                                                .catch(cartUpdateErr => {
                                                    res.json(messages.systemErrorCallback)
                                                })

                                        })
                                        .catch((TransactionItemsErr) => {
                                            console.log(TransactionItemsErr)
                                            res.json(messages.systemErrorCallback)
                                        })
                                })
                                .catch(userCartErr => {
                                    console.log(userCartErr)
                                    res.json(messages.systemErrorCallback)
                                })
                      
                    })
                        .catch((transErr) => {
                            console.log(transErr)
                            res.json(messages.systemErrorCallback)
                        })
                    
                }
                else
                    res.json(response(false, messages.invalidStatusCode, messages.errorMsg.noCartFound))
            })
            .catch((cartErr) => {
                res.json(messages.systemErrorCallback)
            })
    }
    Finaltranscation.remoteMethod("paymentSummary", {
        http: { path: '/paymentSummary', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'object', http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        returns: { arg: "result", type: "object" }
    });

    const generate_invoice = () => {
        var text = "";
        
        // var possible = "ABCDEFGHIJKLMN0123456789OPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        // for (var i = 0; i < 4; i++)
        //     text += possible.charAt(Math.floor(Math.random() * possible.length));
        
        var milliseconds = Math.floor(Date.now());
        text += milliseconds;
        console.log(text, "invoice function")
        return text
    }
    const cartPayment = (cart, invoice,DeliveryAddress,delivery_charge,order_id)=>{
        cart = cart.toJSON();
        const datetime = new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Calcutta'
          });
        var renderer = loopback.template(path.resolve(__dirname, '../../client/orders.html'));
        var html = renderer({ title : 'html', data : cart, invoice:invoice,ShippingAddress:DeliveryAddress,delivery_charge:delivery_charge,order_id:order_id,datetime:datetime });
        return html
    }
    const admincartPayment = (cart,invoice,DeliveryAddress,delivery_charge,order_id)=>{
        cart = cart.toJSON()
        const datetime = new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Calcutta'
          });
        var renderer = loopback.template(path.resolve(__dirname, '../../client/adminEmail.html'));
        var html = renderer({ title : 'html', data : cart, invoice:invoice,ShippingAddress:DeliveryAddress,delivery_charge:delivery_charge ,order_id:order_id,datetime:datetime});
        return html
    }

    const reduceQty = async (transaction,region,productsResp,cb) =>{
        
        let transItem = transaction
       
        // ASYNC.forEach(transarray, function (transItem, callback){
            console.log(JSON.stringify(transItem))
            let Products = app.models.products;
            // Products.findOne({where:{product_slug:transItem.product_slug}})
            // .then(async productsResp=>{
            //     if(productsResp){
                    // console.log(productsResp,"productsResp")
                    productsResp = JSON.parse(JSON.stringify(productsResp))
                    let Uoms = productsResp.region
                    // console.log(JSON.stringify(Uoms))
                    Uoms.map(el=>{
                        if(el.name == region){
                            el.uoms.map(async e=>{
                                if(e.uom == transItem.product_uom){
                                    console.log(transItem.product_uom,"--")
                                    e.available_qty = e.available_qty - transItem.ordered_quantity
                                    console.log(e.available_qty)
                                }
                            })
                        }
                    })
                    Uoms = JSON.parse(JSON.stringify(Uoms))
                    console.log(JSON.stringify(Uoms),"--")
                    const PProducts = app.models.products
                    let upd = await PProducts.findOne({where:{product_slug:transItem.product_slug}})

                    // let upd = await PProducts.update({product_slug:transItem.product_slug},{region:Uoms})
                    console.log(upd,"upd")
                    // updateProduct(transItem,Uoms,async function(updateErr, updateResp){
                    // })
            //     }
            // })
            
            
        // },function(){
        //     console.log("done")
        // })
    }
    // async function reduceQty(transaction,region) {
    //     var async = require("async");
    //     let transarray = transaction.ordered_items
    //     async.forEach(transarray,function (transItem, callback) {
    //         let Products = app.models.products;
    //         Products.findOne({where:{product_slug:transItem.product_slug}},function(prodErr,productsResp){
    //             if(productsResp){
    //                 // console.log(productsResp,"productsResp")
    //                 productsResp = JSON.parse(JSON.stringify(productsResp))
    //                 let Uoms = productsResp.region
    //                 // console.log(JSON.stringify(Uoms))
    //                 Uoms.map(el=>{
    //                     if(el.name == region){
    //                         el.uoms.map(e=>{
    //                             if(e.uom == transItem.product_uom){
    //                                 console.log(transItem.product_uom,"--")
    //                                 e.available_qty = e.available_qty - transItem.ordered_quantity
    //                                 console.log(e.available_qty)
    //                             }
    //                         })
    //                     }
    //                 })
    //                 Uoms = JSON.parse(JSON.stringify(Uoms))
    //                 console.log(JSON.stringify(Uoms),"--")
    //                 setTimeout(async () => {
    //                     updateProduct(transItem,Uoms,async function(updateErr, updateResp){
    //                         console.log("updateErr, updateResp",updateErr, await updateResp)
    //                     })
    //                 }, 2000);
                    
    //                 console.log("-----")
    //             }
    //         })
            
    //     }, function () {
    //         console.log("done")
    //     })
    // }
    // // async function reduceStock(transItem, region,cb){
    // // }
    function updateProduct(transItem,Uoms,callback){
        console.log("****",transItem,JSON.stringify(Uoms),"****")
        let Products = app.models.products;
        Products.update({product_slug:transItem.product_slug},{$push:{region:Uoms}},{allowExtendedOperators: true })
        .then(resp=>{
            console.log(resp,"time")
            callback(null, resp)
        })
        .catch(err=>{
            callback(err,null)
            console.log(err)
        })
    }
};
