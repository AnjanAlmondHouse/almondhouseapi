'use strict';
const { response, verifyToken, jsonCopy } = require('../utils')
const utils = require('../utils')
let messages = require('../messages')
const { validations } = require('../validations')
var async = require('async');
let app = require('../../server/server');
var db = app.datasources.postgresql.connector;
var mongodb = app.datasources.mongovedik.connector
module.exports = function (Usercart) {

    //* cart products list //*
    Usercart.beforeRemote('getCartList', function (ctx, result, next) {
        let requestData = ctx.req.query;
        console.log("ctx.req.headers.token && ctx.req.headers.userid", ctx.req.headers)
        if (ctx.req.headers.userid) {
            let schema = ["user_id"]
            // verifyToken(ctx.req.headers.token, function (err, res) {
            //     if (err) {
            //         ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
            //     } else {
            validations(requestData, schema, (err, res) => {
                if (err) {
                    ctx.res.json(response(false, messages.keyMissStatusCode, err))
                }
                else
                    next()
                //     })
                // }
            })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        }
    });
    Usercart.getCartList = function (user_id, req, res, cb) {
        Usercart.findOne({
            where: { users_id: user_id, is_active: 1 }, fields: { created_at: false, updated_at: false, id: false },
            include: {
                "relation": "cart_list",
                scope: {
                }
            }
        })
            .then((response) => {
                response = JSON.parse(JSON.stringify(response))
                let Products = app.models.products 
                if(response && response.cart_list && response.cart_list.length > 0){
                    Products.find({
                        where:{product_slug :{inq:response.cart_list.map(e=>e.product_slug) }},
                        fields:["id","product_slug","product_name","region"]
                    })
                    .then(productsResp=>{
                        productsResp = JSON.parse(JSON.stringify(productsResp))
                        console.log("response",JSON.stringify(response));
                        
                        response.cart_list.map(ele=>{
                            ele['ordered_quantity'] = Number(ele['ordered_quantity'])
                            let temp = {}
                            let findproduct = productsResp.find(e=>e.product_slug == ele.product_slug)
                            if(findproduct){
                                findproduct.region.map(region=>{
                                    if(region.name == req.headers.region){
                                        let uuoms = region.uoms.find(uom=>uom.uom == ele.product_uom)
                                        if(uuoms){
                                            temp['available_qty']= Number(uuoms.available_qty)
                                            temp['min_order_qty'] = Number(uuoms.min_order_qty)
                                            temp['max_order_qty'] = Number(uuoms.max_order_qty) 
                                        }
                                    }
                                }) 
                            }
                            ele['available_qty']=temp.available_qty
                            ele['min_order_qty'] = temp.min_order_qty
                            ele['max_order_qty'] = temp.max_order_qty 
                        })
                        cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.successMsg.cartFound, cart: response })
                    })
                    .catch(error=>{
                        console.log("error",error);
                        res.json(response(false, messages.errorStatusCode, messages.systemError))
                    })
                }else{
                    cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.successMsg.cartFound, cart: response })
                }
            })
            .catch((error) => {
                console.log(error)
                res.json(response(false, messages.errorStatusCode, messages.systemError))
            })
    }
    Usercart.remoteMethod("getCartList", {
        http: { path: '/getCartList', verb: "get" },
        accepts: [
            { arg: 'user_id', type: 'string', required: true, http: { source: 'query' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can get cart list',
        returns: { arg: "result", type: "object" }
    });

    //* cart products list //*

    //* Apply coupon //*
    Usercart.beforeRemote('applyCoupon', function (ctx, result, next) {
        let requestData = ctx.req.body
        if (ctx.req.headers.token && ctx.req.headers.userid) {
            let schema = ["user_id", "coupon_code", "cart_id", "cart_total"]
            verifyToken(ctx.req.headers.token, function (err, res) {
                if (err) {
                    ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
                } else {
                    validations(requestData, schema, (err, res) => {
                        if (err) {
                            ctx.res.json(response(false, messages.keyMissStatusCode, err))
                        }
                        else {
                            let masterCoupons = app.models.master_coupons;
                            masterCoupons.findOne(
                                {
                                    where: { coupon_code: requestData.coupon_code, is_active: 1 },
                                    fields: ["id", "coupon_title", "coupon_code", "coupon_description", "coupon_percentage",
                                        "coupon_value", "min_amount_to_apply", "max_coupon_value", "start_date",
                                        "expire_date"]
                                })
                                .then(result => {
                                    if (!result) {
                                        let returnData = { result: { status: false, statusCode: 400, message: messages.errorMsg.noCoupons, couponData: result } }
                                        ctx.res.json(returnData)
                                    } else {
                                        ctx.req.body.coupon_id = result.id;
                                        if (utils.utsTime() > result.start_date && utils.utsTime() < result.expire_date) {
                                            if (Number(requestData.cart_total) > Number(result.min_amount_to_apply)) {
                                                var coupon_value;
                                                if (result.coupon_percentage) {
                                                    coupon_value = ((result.coupon_percentage) * (requestData.cart_total)) / 100
                                                } else {
                                                    coupon_value = Number(result.coupon_value)
                                                }
                                                if (coupon_value > result.max_coupon_value) {
                                                    coupon_value = Number(result.max_coupon_value)
                                                }
                                                ctx.req.body.coupon_value = coupon_value
                                                next();
                                            } else {
                                                ctx.res.json(response(false, messages.invalidStatusCode, `Minimum cart value of INR ${result.min_amount_to_apply} is required to apply the coupon` ))
                                            }
                                        } else {
                                            ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.couponExpired))
                                        }
                                    }
                                })
                                .catch(error => {
                                    ctx.res.json(error)
                                })
                        }
                    })
                }
            })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, "Please login to apply coupon"))
        }
    });

    Usercart.applyCoupon = function (requestData, req, res, cb) {
        let whereCondition = { id: requestData.cart_id, users_id: requestData.user_id, coupon_code: null }
        Usercart.findOne({ where: whereCondition })
            .then((cartResult) => {
                if (cartResult) {
                    Usercart.update(whereCondition,
                        {
                            coupon_code: requestData.coupon_code,
                            master_coupons_id: requestData.coupon_id,
                            coupon_value: requestData.coupon_value,
                            grand_total: Number(cartResult.cart_total) + Number(cartResult.total_tax) - Number(requestData.coupon_value)
                        })
                        .then(updateRes => {
                            res.json(response(true, messages.successStatusCode, messages.successMsg.couponApplied))
                        })
                        .catch(updateErr => {
                            res.json(messages.systemErrorCallback)
                        })
                } else {
                    res.json(response(false, messages.invalidStatusCode, messages.errorMsg.couponAlreadyApplied))
                }
            })
            .catch((cartErr) => {
                res.json(messages.systemErrorCallback)
            })
    }

    Usercart.remoteMethod("applyCoupon", {
        http: { path: '/applyCoupon', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'object', required: true, http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Apply coupon to get discount',
        returns: { arg: "result", type: "object" }
    });
    //* Apply coupon //*

    //* Remove coupon //*
    Usercart.beforeRemote('removeCoupon', function (ctx, result, next) {
        let requestData = ctx.req.body
        if (ctx.req.headers.token && ctx.req.headers.userid) {
            let schema = ["user_id", "cart_id"]
            verifyToken(ctx.req.headers.token, function (err, res) {
                if (err) {
                    ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
                } else {
                    validations(requestData, schema, (err, res) => {
                        if (err) {
                            ctx.res.json(response(false, messages.keyMissStatusCode, err))
                        }
                        else {
                            Usercart.findOne({
                                where: {
                                    "users_id": requestData.user_id,
                                    "id": requestData.cart_id,
                                    "coupon_code": { neq: null },
                                    "coupon_value": { gt: 0 }
                                }
                            })
                                .then((cartResult) => {
                                    if (cartResult)
                                        next()
                                    else
                                        ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.noCouponApplied))
                                })
                        }
                    })
                }
            })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        }
    });

    Usercart.removeCoupon = function (requestData, req, res, cb) {
        let updateQuery = `update user_cart set coupon_code = null, coupon_value = 0.00, master_coupons_id = null, grand_total = grand_total + coupon_value`
        db.query(updateQuery, function (updateErr, updateResult) {
            if (updateErr)
                res.json(messages.systemErrorCallback)
            else {
                res.json(response(true, messages.successStatusCode, messages.successMsg.couponRemoved))
            }
        })
    }

    Usercart.remoteMethod("removeCoupon", {
        http: { path: '/removeCoupon', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'object', required: true, http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Remove the applied coupon',
        returns: { arg: "result", type: "object" }
    });
    //* Remove coupon //*

    //* Add delivery charge //*
    Usercart.beforeRemote('addDeliveryCharge', function (ctx, result, next) {
        let requestData = ctx.req.body
        if (ctx.req.headers.token && ctx.req.headers.userid) {
            let schema = ["user_id", "cart_id", "zip_code"]
            verifyToken(ctx.req.headers.token, function (err, res) {
                if (err) {
                    ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
                } else {
                    validations(requestData, schema, (err, res) => {
                        if (err) {
                            ctx.res.json(response(false, messages.keyMissStatusCode, err))
                        }
                        else {
                            Usercart.findOne({
                                where: {
                                    "users_id": requestData.user_id,
                                    "id": requestData.cart_id,
                                }
                            })
                                .then((cartResult) => {
                                    if (cartResult)
                                        next()
                                    else
                                        ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.noCartFound))
                                })
                                .catch((cartErr) => {
                                    ctx.res.json(messages.systemErrorCallback)
                                })
                        }
                    })
                }
            })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        }
    });

    Usercart.addDeliveryCharge = function (requestData, req, res, cb) {
        res.json(requestData)
    }

    Usercart.remoteMethod("addDeliveryCharge", {
        http: { path: '/addDeliveryCharge', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'object', required: true, http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add delivery charge',
        returns: { arg: "result", type: "object" }
    });
    //* Add delivery charge //*

    //* make payment Conformation screen all cart items and cart value //*
    Usercart.beforeRemote("paymentConfirmation", function (ctx, result, next) {
        let requestData = ctx.req.query
        if (ctx.req.headers.userid) {
            let schema = ["user_id"]
            // verifyToken(ctx.req.headers.token, function (err, res) {
            //     if (err) {
            //         ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
            //     } else {
            validations(requestData, schema, (err, res) => {
                if (err) {
                    ctx.res.json(response(false, messages.keyMissStatusCode, err))
                }
                else {
                    Usercart.findOne({
                        where: {
                            "users_id": requestData.user_id,
                            "is_active": 1
                        }
                    })
                        .then((cartResult) => {
                            if (cartResult)
                                next()
                            else
                                ctx.res.json({ result: { status: true, statusCode: messages.invalidStatusCode, message: messages.errorMsg.noCartFound, cart: null } })
                        })
                        .catch((cartErr) => {
                            ctx.res.json(messages.systemErrorCallback)
                        })
                }
                //     })
                // }
            })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        }
    });

    Usercart.paymentConfirmation = function (user_id, req, res, cb) {
        Usercart.findOne({
            where: { users_id: user_id, is_active: 1 }, fields: { created_at: false, updated_at: false, id: false },
            include: [{
                "relation": "cart_list",
                "scope": {
                    fields: ["id", 'user_cart_product_id', "product_id", "product_name", "product_uom", "product_price", "ordered_quantity",
                        "discount_percentage", "thumbnail_product_image", "product_slug", "discount_amount", "product_image",
                        "price_after_discount", "total_price", "tax_total", "user_id", "category_slug", "category_name", "product_selling_price"],
                    include: ["Products"]
                }
            },
            { "relation": "cartAddress" },{"relation":"Billingaddress"}]
        })
            .then(async (cartResult) => {
                cartResult = JSON.parse(JSON.stringify(cartResult))
                cartResult = await getProductsMinMax(cartResult,req.headers.region)
                taxation(cartResult, function (taxErr, taxResult) {
                    // let taxTotal = taxResult.reduce((a,b)=> a + b.tax_total, 0)
                    let user_address_id
                    if (cartResult && cartResult.cartAddress && cartResult.cartAddress.is_active == 1) {
                        user_address_id = cartResult.user_address_id
                        deliveryCharge(cartResult,cartResult.cartAddress.zip_code,function(err, deliveryResp){
                                console.log(err, deliveryResp)
                                if(err){
                                    res.json(response(false, messages.invalidStatusCode, err))
                                }else{
                                    let taxTotal = (cartResult.cart_list).reduce((a, b) => a + Number(b.tax_total), 0);
                                    console.log(taxTotal, "tax_total", cartResult.cart_total, cartResult.discount_amount, cartResult.coupon_value ? cartResult.coupon_value : 0, taxTotal, deliveryResp ? deliveryResp : 0)
                                    let grandTotal = Number(cartResult.cart_total) - Number(cartResult.discount_amount) - Number(cartResult.coupon_value ? cartResult.coupon_value : 0) + Number(deliveryResp ? deliveryResp : 0)
                                    console.log(grandTotal, "grandTotal", cartResult.cart_list);
                                    let cart_total = (cartResult.cart_list).reduce((a, b) => a + Number(b.price_after_discount), 0)
                                    console.log(cart_total, "cart_total--cart_total", cartResult.cartAddress)
                                    let delCharge = deliveryResp ? deliveryResp : 0
                                    let updateObject = { "grand_total": grandTotal, "total_tax": taxTotal, "cart_total": cart_total, "user_address_id": user_address_id,"delivery_charge":delCharge }
                                    Usercart.update(
                                        {
                                            id: cartResult.id,
                                            "users_id": user_id,
                                            "is_active": 1
                                        }, updateObject)
                                        .then((cartUpdateResp) => {
                                            cartResult.cart_list.map((a) => delete a.Products)
                                            cartResult.grand_total = grandTotal.toFixed(2);
                                            cartResult.total_tax = taxTotal.toFixed(2);
                                            cartResult.taxResult = taxResult;
                                            cartResult.delivery_charge = deliveryResp ? deliveryResp : 0
                                            cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.successMsg.cartFound, cart: cartResult })
                                        })
                                        .catch((cartErr) => {
                                            res.json(messages.systemErrorCallback)
                                        })
                                }
                            })
                    } else {
                        user_address_id = null;
                        cartResult.cartAddress = null
                        let taxTotal = (cartResult.cart_list).reduce((a, b) => a + Number(b.tax_total), 0);
                        console.log(taxTotal, "tax_total", cartResult.cart_total, cartResult.discount_amount, cartResult.coupon_value ? cartResult.coupon_value : 0, taxTotal, cartResult.delivery_charge ? cartResult.delivery_charge : 0)
                        let grandTotal = Number(cartResult.cart_total) - Number(cartResult.discount_amount) - Number(cartResult.coupon_value ? cartResult.coupon_value : 0) + Number(cartResult.delivery_charge ? cartResult.delivery_charge : 0)
                        console.log(grandTotal, "grandTotal", cartResult.cart_list);
                        let cart_total = (cartResult.cart_list).reduce((a, b) => a + Number(b.price_after_discount), 0)
                        let delCharge = cartResult.delivery_charge ? cartResult.delivery_charge : 0
                        console.log(cart_total, "cart_total--cart_total", cartResult.cartAddress)
                    let updateObject = { "grand_total": grandTotal, "total_tax": taxTotal, "cart_total": cart_total, "user_address_id": user_address_id ,"delivery_charge":delCharge}
                    Usercart.update(
                        {
                            id: cartResult.id,
                            "users_id": user_id,
                            "is_active": 1
                        }, updateObject)
                        .then((cartUpdateResp) => {
                            cartResult.cart_list.map((a) => delete a.Products)
                            cartResult.grand_total = grandTotal.toFixed(2);
                            cartResult.total_tax = taxTotal.toFixed(2);
                            cartResult.taxResult = taxResult;
                            cartResult.delivery_charge = cartResult.delivery_charge ? cartResult.delivery_charge : 0
                            cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.successMsg.cartFound, cart: cartResult })
                        })
                        .catch((cartErr) => {
                            res.json(messages.systemErrorCallback)
                        })
                    }
                })
            })
            .catch((error) => {
                console.log(error)
                res.json(response(false, messages.errorStatusCode, messages.systemError))
            })
    }
    Usercart.remoteMethod("paymentConfirmation", {
        http: { path: '/paymentConfirmation', verb: "get" },
        accepts: [
            { arg: 'user_id', type: 'string', http: { source: 'query' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add delivery charge',
        returns: { arg: "result", type: "object" }
    });

    const getProductsMinMax = async (response,location) => {
        try {
            let Products = app.models.products 
            if(response && response.cart_list && response.cart_list.length > 0){
                let productsResp = await Products.find({
                    where:{product_slug :{inq:response.cart_list.map(e=>e.product_slug) }},
                    fields:["id","product_slug","product_name","region"]
                })
                productsResp = JSON.parse(JSON.stringify(productsResp))
                response.cart_list.map(ele=>{
                    ele['ordered_quantity'] = Number(ele['ordered_quantity'])
                    let temp = {}
                    let findproduct = productsResp.find(e=>e.product_slug == ele.product_slug)
                    if(findproduct){
                        findproduct.region.map(region=>{
                            if(region.name == location){
                                let uuoms = region.uoms.find(uom=>uom.uom == ele.product_uom)
                                if(uuoms){
                                    temp['available_qty']= Number(uuoms.available_qty)
                                    temp['min_order_qty'] = Number(uuoms.min_order_qty)
                                    temp['max_order_qty'] = Number(uuoms.max_order_qty)
                                }
                            }
                        }) 
                    }
                    ele['available_qty']=temp.available_qty
                    ele['min_order_qty'] = temp.min_order_qty
                    ele['max_order_qty'] = temp.max_order_qty 
                })
                return response
            }else{
                return response
            }
        } catch (error) {
            return response
        }
    }
    //* make payment Conformation screen all cart items and cart value //*
    function taxation(cartResult, cb) {
        console.log(cartResult);
        try {
            var final_tax = [];
            var temp_tax_names = []
            async.forEach(cartResult.cart_list, function (cartItem, callback) {
                console.log("cartItem", cartItem);
                let Products = app.models.products;
                Products.findOne({ where: { product_slug: cartItem.product_slug } }, (err, cartResp) => {
                    if (err) {
                        console.log(err, "-----err")
                        cb(null, [])
                    } else {
                        cartResp = jsonCopy(cartResp)
                        let taxes = cartResp ? cartResp.taxes : []
                        for (var j in taxes) {
                            if (temp_tax_names.indexOf(taxes[j].tax_name) > -1) {
                                final_tax.map((element) => {
                                    if (element.tax_name == taxes[j].tax_name) {
                                        element.tax_total += (Number(cartItem.product_selling_price) - (Number(cartItem.product_selling_price) * Number(cartItem.discount_percentage) / 100)) * Number(cartItem.ordered_quantity) * (Number(taxes[j].tax_percentage) / 100)
                                        return element
                                    }
                                })
                            } else {
                                var tax = {};
                                tax.tax_id = taxes[j].tax_id
                                tax.tax_name = taxes[j].tax_name;
                                tax.tax_percentage = taxes[j].tax_percentage;
                                tax.tax_total = (Number(cartItem.product_selling_price) - (Number(cartItem.product_selling_price) * Number(cartItem.discount_percentage) / 100)) * Number(cartItem.ordered_quantity) * (Number(taxes[j].tax_percentage) / 100)
                                final_tax.push(tax);
                                temp_tax_names.push(tax.tax_name)
                            }
                        }
                        callback()
                    }
                })
            }, function (err, resp) {
                cb(null, final_tax)
            })

        } catch (error) {
            console.log(error)
            cb(null, [])
        }

    }
    function isDeliverable(cartResult, zipCode, cb) {
        // try {
        //     let tempZipCodes = [];
        //     //  product slug and product name
        //     let productArray = [];
        //     async.forEach(cartResult.cart_list, function (cartItem, callback) {
        //         console.log("cartItem", cartItem);
        //         let Products = app.models.products;
        //         Products.findOne({ where: { product_slug: cartItem.product_slug } }, (err, cartResp) => {
        //             if (err) {
        //                 console.log(err, "-----err")
        //                 callback(err)
        //             } else {
        //                 cartResp = jsonCopy(cartResp)
        //                 console.log(cartResp.pincodes.find((a) => a.pincode == zipCode), "cc")
        //                 let pinCodeFind = cartResp.pincodes.find((a) => a.pincode == zipCode)
        //                 if (pinCodeFind) {
        //                     console.log("if", pinCodeFind)
        //                     tempZipCodes.push(pinCodeFind)
        //                 } else {
        //                     console.log("else")
        //                     productArray.push(cartResp.product_name)
        //                 }
        //                 callback()
        //             }
        //         })
        //     }, function (err) {
        //         console.log(productArray, "-->")
        //         if (tempZipCodes.length == cartResult.cart_list.length) {
        //             cb(null, tempZipCodes)
        //         } else {
        //             cb(productArray + " " + messages.errorMsg.notDeliverable, null)
        //         }
        //     })
        // } catch (error) {
        //     console.log(error, "uuu")
        //     cb(messages.systemError, null)
        // }
        console.log(cartResult, zipCode)
        let productArray = [];
        let pincodesVsDeliverycharge = app.models.zipcode_vs_deliverycharge;
        pincodesVsDeliverycharge.findOne({
            where:{
                zip_code:zipCode
            }
        })
        .then((deliveryCharge) => {
        if (deliveryCharge) {
            cb(null, deliveryCharge.zip_code)
        } else {
            async.forEach(cartResult.cart_list, function (cartItem, callback) {
                console.log("cartItem", cartItem);
                let Products = app.models.products;
                Products.findOne({ where: { product_slug: cartItem.product_slug } }, (err, cartResp) => {
                    if (err) {
                        console.log(err, "-----err")
                        callback(err)
                    } else {
                        cartResp = jsonCopy(cartResp)
                        productArray.push(cartResp.product_name)
                        callback()
                    } 
                })
            },function(err) {
                if(err){
                    return cb(err,null)
                }
                if(productArray.length > 0){
                    return cb(productArray + " " + messages.errorMsg.notDeliverable, null)
                }
                cb(null, true)
            })
            }
            })
        .catch((err) => {
            console.log(error, "uuu")
            cb(messages.errorMsg.notDeliverable, null)
        })
    }

    //* Delivery charge update //*
    function deliveryCharge(result,zipCode,cb){
    let pincodesVsDeliverycharge = app.models.zipcode_vs_deliverycharge;
    console.log(zipCode,"pincode")
    pincodesVsDeliverycharge.findOne({
        where:{
            zip_code:zipCode
        }
    })
    .then((deliveryCharge) => {
        if(deliveryCharge)
        cb(null, deliveryCharge.delivery_charge)
        else
        cb(null, 0.00)
    })
    .catch((cartErr) => {
        cb(null, '')
    })
   } 
   //* Conformation Payment Amount //*
    Usercart.beforeRemote("confirmPaymentAmount", function (ctx, result, next) {
        let requestData = ctx.req.body
        if (ctx.req.headers.userid) {
            let schema = ["user_id", "cart_id", "amount_to_be_paid"]
            // verifyToken(ctx.req.headers.token, function (err, res) {
            //     if (err) {
            //         ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
            //     } else {
                    // validations(requestData, schema, (err, res) => {
                    //     if (err) {
                    //         ctx.res.json(response(false, messages.keyMissStatusCode, err))
                    //     }
                    //     else {
                        if(requestData.is_guest_user == 1){
                            console.log("response",requestData)
                            checkUserDetails(requestData,function(err,success){
                                if(err){
                                    return ctx.res.json(response(false, messages.keyMissStatusCode, err))
                                }else{
                                    next()
                                }
                            })
                        }else{
                            next()
                        }
                            
                    //     }
                    // })
            //     }
            // })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        }
    });
    Usercart.confirmPaymentAmount = function(requestData,req,res,cb){
        Usercart.findOne({
            where: {
                // "users_id": requestData.user_id,
                "is_active": 1,
                "id": requestData.cart_id
            }
        })
            .then((cartResult) => {
                if (cartResult) {
                    cartResult.toJSON();
                    console.log(cartResult.grand_total == requestData.amount_to_be_paid,"condition")
                    if (cartResult.grand_total == requestData.amount_to_be_paid) {
                        res.json({
                            result: {
                                status: true,
                                statusCode: messages.successStatusCode,
                                message: messages.successMsg.proceedToPay,
                                amount_bo_be_paid: cartResult.grand_total
                            }
                        })
                    } else {
                        res.json(response(false, messages.invalidStatusCode, messages.errorMsg.paymentTotalErr))
                    }
                }
                else
                    res.json(response(false, messages.invalidStatusCode, messages.errorMsg.noCartFound))
            })
            .catch((cartErr) => {
                res.json(messages.systemErrorCallback)
            })
    }
    Usercart.remoteMethod("confirmPaymentAmount", {
        http: { path: '/confirmPaymentAmount', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'object', http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add delivery charge',
        returns: { arg: "result", type: "object" }
    });
    //* Conformation Payment Amount //*

    function checkUserDetails(requestData, cb){
        let Users = app.models.users
        Users.findOne({ where: { or:[{user_email: requestData.email },{user_mobile: requestData.mobile}]}} )
        .then((userResult) => {
            if (userResult) {
                return cb(messages.errorMsg.userEmailMobileExist,null)
            }
            else{
                Usercart.findOne({where:{id:requestData.cart_id}})
                .then(response=>{
                    response = JSON.parse(JSON.stringify(response))
                    let cartAddress  = app.models.cart_address
                    cartAddress.update({id:response.user_address_id},{user_name:requestData.user_name,mobile:requestData.mobile,email:requestData.email})
                    cb(null, true)
                })
                .catch(error=>{
                    console.log(error,"error");
                    cb(messages.systemError,null)
                })
            }
        })
        .catch(error=>{
            console.log("error",error);
            cb(messages.systemError,null)
        })
    }
    
    //* User Delivery Address Selection //*
    Usercart.beforeRemote("selectDeliveryAddress", function (ctx, result, next) {
        let requestData = ctx.req.body
        if (ctx.req.headers.userid) {
            let schema = ["user_id", "cart_id", "user_address_id"]
            // verifyToken(ctx.req.headers.token, function (err, res) {
            //     if (err) {
            //         ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
            //     } else {
                    validations(requestData, schema, (err, res) => {
                        if (err) {
                            ctx.res.json(response(false, messages.keyMissStatusCode, err))
                        }
                        else {
                            Usercart.findOne({
                                where: {
                                    // "users_id": requestData.user_id,
                                    "is_active": 1,
                                    "id": requestData.cart_id
                                }
                            })
                                .then((cartResult) => {
                                    if (cartResult) {
                                        let Useraddress = app.models.user_address;
                                        Useraddress.findOne({
                                            where: {
                                                id: requestData.user_address_id,
                                                is_active: 1
                                            }
                                        })
                                            .then((addressResp) => {
                                                if (addressResp) {
                                                    console.log(addressResp,"addressResp")
                                                    let Cartaddress = app.models.cart_address;
                                                    var data = {
                                                        "user_name": addressResp.user_name,
                                                        "address": addressResp.address,
                                                        "mobile": addressResp.mobile,
                                                        "landmark": addressResp.landmark,
                                                        "address_name": addressResp.address_name,
                                                        "city": addressResp.city,
                                                        "state": addressResp.state,
                                                        "country": addressResp.country,
                                                        "address_type": addressResp.address_type,
                                                        "is_active": addressResp.is_active,
                                                        "is_primary": addressResp.is_primary,
                                                        "alternative_mobile": addressResp.alternative_mobile,
                                                        "first_name":addressResp.first_name,
                                                        "last_name":addressResp.last_name,
                                                        "email":addressResp.email,
                                                        "zip_code": addressResp.zip_code
                                                    }
                                                    Cartaddress.create(data)
                                                    .then((cartAddressresp)=>{
                                                        if(cartAddressresp){
                                                            console.log(cartAddressresp,"cartAddress")
                                                            Usercart.update({ users_id: requestData.user_id, is_active: 1 }, { user_address_id: cartAddressresp.id })
                                                                .then(cartAddUpdate => {
                                                                    next()
                                                                })
                                                                .catch(cartAddErr => {
                                                                    console.log(cartAddErr,"cartAddErr");
                                                                    ctx.res.json(messages.systemErrorCallback)
                                                                })
                                                        }
                                                        else
                                                        ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.addrNotFound))
                                                    })
                                                    .catch((cartAddresserr) => {
                                                        ctx.res.json(messages.systemErrorCallback)
                                                    })
                                                } else {
                                                    ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.addrNotFound))
                                                }
                                             })
                                            .catch((addressErr) => {
                                                ctx.res.json(messages.systemErrorCallback)
                                            })
                                    }
                                    else
                                        ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.noCartFound))
                                })
                                .catch((cartErr) => {
                                    ctx.res.json(messages.systemErrorCallback)
                                })
                    //     }
                    // })
                }
            })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        }
    });
    Usercart.selectDeliveryAddress = function (requestData, req, res, cb) {
        // res.json(requestData)
                Usercart.findOne({
                    where: { users_id: requestData.user_id, is_active: 1 }, fields: { created_at: false, updated_at: false, id: false },
                    include: [{
                        "relation": "cart_list",
                        "scope": {
                            fields: ["id", 'user_cart_product_id', "product_name", "product_uom", "product_price", "ordered_quantity",
                                "discount_percentage", "thumbnail_product_image", "product_slug", "discount_amount",
                                "price_after_discount", "total_price", "tax_total"],
                            include: ["Products"]
                        }
                    },
                    { "relation": "cartAddress" }]
                })
                    .then((cartResult) => {
                        cartResult = jsonCopy(cartResult)
                        console.log("cartResult.Useraddress", cartResult)
                        if (!cartResult.cartAddress)
                            res.json(response(false, messages.invalidStatusCode, messages.errorMsg.noAddrSelected))
                        else {
                            cartResult = JSON.parse(JSON.stringify(cartResult))
                            isDeliverable(cartResult, cartResult.cartAddress.zip_code, function (err, deliveryResp) {
                                console.log(err, deliveryResp)
                                if (err) {
                                    Usercart.update({ users_id: requestData.user_id, is_active: 1 }, { user_address_id: null })
                                    res.json(response(false, messages.invalidStatusCode, err))
                                } else {
                                    console.log(deliveryResp,requestData,"requestData");
                                    if(requestData.is_primary == 1){
                                        let userAddress = app.models.user_address;
                                        userAddress.update({id:requestData.user_address_id},{is_primary : 1})
                                        .then(updateAddrResp=>{
                                            console.log("updateAddrResp",updateAddrResp);
                                            userAddress.update({user_id:requestData.user_id,"id": { neq: requestData.user_address_id}},{is_primary : 0})
                                            .then(updateAddrResp=>{
                                                console.log(updateAddrResp);
                                                res.json(response(true, messages.successStatusCode, messages.successMsg.deliveryAddrAssigned))
                                            })
                                            .catch(updateErr=>{
                                                res.json(messages.systemErrorCallback)
                                            })
                                        })
                                        .catch(updateErr=>{
                                            res.json(messages.systemErrorCallback)
                                        })
                                    }else{
                                        res.json(response(true, messages.successStatusCode, messages.successMsg.deliveryAddrAssigned))
                                    }
                                }
                            })
                        }
                    })
                    .catch((error) => {
                        console.log(error)
                        res.json(messages.systemErrorCallback)
                    })
    }
    Usercart.remoteMethod("selectDeliveryAddress", {
        http: { path: '/selectDeliveryAddress', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'object', http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add delivery charge',
        returns: { arg: "result", type: "object" }
    });











    //* Get User Cart Details //*
    Usercart.beforeRemote('getCartDetails', function (ctx, result, next) {
        let requestData = ctx.req.query
        if (ctx.req.headers.userid) {
            let schema = ["user_id"]
            // verifyToken(ctx.req.headers.token, function (err, res) {
            //     if (err) {
            //         ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
            //     } else {
            // validations(requestData, schema, (err, res) => {
            //     if (err) {
                //     ctx.res.json(response(false, messages.keyMissStatusCode, err))
                // }
                // else
                    next()
            // })
            //     }
            // })
        } else {
            ctx.res.json(response(false, messages.keyMissStatusCode, messages.missingKeys.missingHeaders))
        }
    });
    Usercart.getCartDetails = function (user_id, req, res, cb) {
        console.log(user_id)
        Usercart.findOne({
            where: { users_id: user_id, is_active: 1 }, fields: ["id", "cart_total", "grand_total", "users_id"],
            include: {
                "relation": "cart_list",
                fields: ["id"]
            }
        })
            .then((response) => {


                if (response) {
                    response = jsonCopy(response)
                    console.log(response.cart_list)
                    let cartResponse = {
                        id: response.id,
                        cart_total: response.cart_total,
                        grand_total: response.grand_total,
                        products_count: response.cart_list.length,
                        "users_id": response.users_id
                    }
                    cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.successMsg.cartFound, cart: cartResponse })
                } else {
                    cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.successMsg.cartFound, cart: response })
                }
            })
            .catch((error) => {
                console.log(error)
                res.json(response(false, messages.errorStatusCode, messages.systemError))
            })
    }
    Usercart.remoteMethod("getCartDetails", {
        http: { path: '/getCartDetails', verb: "get" },
        accepts: [
            { arg: 'user_id', type: 'string', required: true, http: { source: 'query' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User can get cart details',
        returns: { arg: "result", type: "object" }
    });
    //* Get User Cart Details //*
    //* RELATED CART PRODUCTS DETAILS START *//
    Usercart.remoteMethod("relatedCartProducts", {
        http: { path: '/relatedCartProducts', verb: "get" },
        accepts: [
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Related cart product details in json format',
        returns: { arg: "result", type: "object" }
    });
    Usercart.beforeRemote('relatedCartProducts', function (ctx, result, next) {
        let headerSchema = ['region']
        validateheaders(ctx.req.headers, headerSchema, (err, res) => {
            if (err) {
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else {
                next()
            }
        })
    });
    Usercart.relatedCartProducts = function (req, res, cb) {
        Usercart.findOne({
            where: { users_id: req.query.user_id, is_active: 1 },
            include: [{
                "relation": "cart_list",
                "scope": {
                    fields: ['product_id', 'ordered_quantity', 'user_id', 'product_slug', 'product_name', "product_uom", "product_cat"],
                }
            }]
        })
            .then(function (relatedCartProducts) {
                if (relatedCartProducts != null) {
                    let relatedcartProducts = jsonCopy(relatedCartProducts)
                    var slugs = []
                    relatedcartProducts.cart_list.map(function (products_slugs) {
                        slugs.push(products_slugs.product_slug)
                    })
                    console.log(slugs)
                    let Relatedproducts = app.models.related_products
                    Relatedproducts.find({
                        fields: ['relatedproducts', "product_slug"],
                        where: {
                            "product_slug": { "inq": slugs },
                        },
                    })
                        .then(function (products_list) {
                            let Wishlist = app.models.wishlist
                            Wishlist.find({
                                where: { user_id: req.query.user_id },
                                fields: { created_at: false, updated_at: false, products_id: false }
                            }, (err, wish_list) => {
                                if (err) {
                                    res.json(response(false, messages.systemError, messages.successMsg.removedWishlist))
                                } else {
                                    var listslugs = []
                                    wish_list.map(function (products_slugs) {
                                        listslugs.push(products_slugs.product_slug)
                                    })
                                    let productslist = jsonCopy(products_list)
                                    let cartitems = jsonCopy(relatedCartProducts)
                                    let items = []

                                    productslist.map(function (products, index) {
                                        products.relatedproducts.map(function (RelatedProducts, index) {
                                            if (!slugs.includes(RelatedProducts.product_slug)) {
                                                RelatedProducts.product_category = RelatedProducts.product_cat[0]
                                                RelatedProducts.regions = RelatedProducts.region.find((a) => { if (a.name == req.headers.region) return a })
                                                delete RelatedProducts.product_sku
                                                delete RelatedProducts.region
                                                delete RelatedProducts.product_cat
                                                cartitems ? cartitems.cart_list.map((obj) => {
                                                    if (obj.product_slug == RelatedProducts.product_slug) {
                                                        RelatedProducts.regions.uoms.find((uuom) => { if (uuom.uom == obj.product_uom) { uuom.cart_quantity = obj.ordered_quantity } })
                                                    }
                                                }) : null
                                                if (listslugs.includes(RelatedProducts.product_slug)) {
                                                    RelatedProducts.added_to_wish_list = true
                                                } else {
                                                    RelatedProducts.added_to_wish_list = false
                                                }
                                                items.push(RelatedProducts)
                                            }
                                        })
                                    })
                                    let itemdata = items.filter((v, i, a) => a.findIndex(t => (t.product_slug === v.product_slug)) === i)
                                    cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.successMsg.suggestedCartProFound, SuggestedCartproducts: itemdata.sort(() => .5 - Math.random()).slice(0, 15) })
                                }
                            })
                        })
                        .catch(function (error) {
                            console.log(error)
                            cb(null, messages.systemErrorCallback)
                        })
                } else {
                    cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.errorMsg.noItemsFound, SuggestedCartproducts: relatedCartProducts })
                }
            })
            .catch(function (error) {
                console.log(error)
                cb(null, messages.systemErrorCallback)
            })
    }
    //* RELATED CART PRODUCTS DETAILS END *//
};
