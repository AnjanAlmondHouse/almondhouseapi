'use strict';
let messages = require('../messages');
let app = require('../../server/server');
let db = app.datasources.mongovedik.connector
let {jsonCopy,verifyToken, response} = require("../utils")
let {validations,validateheaders} = require('../validations')
var async = require('async');
module.exports = function(Finalpackagetransaction) {
    //* User Ordered Packages Products Start *//
    Finalpackagetransaction.remoteMethod("userorderpackageproducts",{
        http:{path: '/userorderpackageproducts', verb:"get"},
        accepts:[
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'User Order Packages Products in json format',
        returns:{arg:"result", type:"object" }
    });
    Finalpackagetransaction.beforeRemote('userorderpackageproducts', function(ctx, result, next){	
        let headerSchema = ['token','userid']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }else{
                verifyToken(ctx.req.headers.token, function(err, res){
                    if(err) {
                        ctx.res.json(response(false, messages.invalidStatusCode,messages.errorMsg.token_expired))
                    }else{
                        let schema = ["user_id","order_id"];
                        console.log(ctx.req.query)
                        validations(ctx.req.query,schema, (err, res)=>{
                            if(err){
                                messages.keyMissCallBack.result['message'] = err;
                                ctx.res.json(messages.keyMissCallBack)
                            }else{
                                next();
                            }
                        })
                    }
                })
            }
        })
    });

    Finalpackagetransaction.userorderpackageproducts = function( req, res, cb){
        Finalpackagetransaction.findOne({ 
            fields: { created_at: false, updated_at: false, id: false },
            where:{"user_id" : req.query.user_id,'user_package_cart_id':req.query.order_id},
            include:[
                {"relation": "package_cart_list",
                    scope:{
                        include:["Products"],
                        fields:['user_package_cart_product_id','products_id','product_slug','user_id',"product_name","product_uom","product_price","ordered_quantity","discount_percentage","thumbnail_product_image","discount_amount","price_after_discount","total_price","tax_total"]
                    },
                },
                {"relation" : "Useraddress"}
            ]
        })
        .then(function(UserOrderPackageProducts){
            // if (!UserOrderPackageProducts.Useraddress)
            //         res.json(response(false, messages.invalidStatusCode, messages.errorMsg.noAddrSelected))
            // else {
                UserOrderPackageProducts = JSON.parse(JSON.stringify(UserOrderPackageProducts))
                packagetaxation(UserOrderPackageProducts,function(taxErr, taxResult){
                    let taxTotal = (UserOrderPackageProducts.package_cart_list).reduce((a,b)=> a + Number(b.tax_total), 0);
                    let grandTotal = Number(UserOrderPackageProducts.user_package_cart_total) - Number(UserOrderPackageProducts.discount_amount) - Number(UserOrderPackageProducts.coupon_value?UserOrderPackageProducts.coupon_value:0) + taxTotal + Number(UserOrderPackageProducts.delivery_charge?UserOrderPackageProducts.delivery_charge:0)
                    let cart_total = (UserOrderPackageProducts.package_cart_list).reduce((a,b)=>a + b.price_after_discount,0)
                    let updateObject = {"grand_total":grandTotal,"total_tax":taxTotal}
                    Finalpackagetransaction.update({
                        id: UserOrderPackageProducts.id,
                        "users_id": req.query.user_id,
                        "is_active": 1
                    }, updateObject)
                    .then((PackagecartUpdateResp) => {
                        UserOrderPackageProducts.package_cart_list.map((a) => delete a.Products)
                        UserOrderPackageProducts.grand_total = grandTotal.toFixed(2);
                        UserOrderPackageProducts.total_tax = taxTotal.toFixed(2);
                        UserOrderPackageProducts.taxResult = taxResult;
                        cb(null, { status: true, statusCode: messages.successStatusCode, message: messages.successMsg.packagecartFound, Packagecart: UserOrderPackageProducts })
                    })
                    .catch((cartErr) => {
                        res.json(messages.systemErrorCallback)
                    })
                })
            // }
        })
        .catch(function (error) {
            console.log(error)
            let result = {"status":true, "statusCode":messages.successStatusCode};
            result["message"] = messages.errorMsg.noOrdersFound;
            result['orders'] = {};
            cb(null,result) 
       })
    }

    function packagetaxation(cartResult,cb) {
        try {
            var final_tax = [];
            var temp_tax_names = []
            async.forEach(cartResult.package_cart_list, function (cartItem, callback){
                let Products = app.models.products;
                Products.findOne({where:{product_slug : cartItem.product_slug}}, (err, cartResp)=>{
                    if(err){
                        console.log(err,"-----err")
                        cb(null, [])
                    }else{
                        cartResp = jsonCopy(cartResp)
                        let taxes = cartResp?cartResp.taxes : []
                        for (var j in taxes) {
                            if (temp_tax_names.indexOf(taxes[j].tax_name) > -1) {
                                final_tax.map((element) => {
                                    if (element.tax_name == taxes[j].tax_name) {
                                        element.tax_total += Number(cartItem.product_price) * Number(cartItem.ordered_quantity) * (Number(taxes[j].tax_percentage) / 100)
                                        return element
                                    }
                                })
                            } else {
                                var tax = {};
                                tax.tax_id = taxes[j].tax_id
                                tax.tax_name = taxes[j].tax_name;
                                tax.tax_percentage = taxes[j].tax_percentage;
                                tax.tax_total = Number(cartItem.product_price) * Number(cartItem.ordered_quantity) * (Number(tax.tax_percentage) / 100)
                                final_tax.push(tax);
                                temp_tax_names.push(tax.tax_name)
                            }
                        }
                        callback()
                    }
                })
            },function(err, resp) {
                cb(null,final_tax)
            })
        } catch (error) {
            console.log(error)
            cb(null,[])
        } 
    }

    //* User Ordered Packages Products End *//

    //* Conformation Package Payment Amount Start *//
    Finalpackagetransaction.remoteMethod("confirmPackagePaymentAmount", {
        http: { path: '/confirmPackagePaymentAmount', verb: "post" },
        accepts: [
            { arg: 'requestData', type: 'object', http: { source: 'body' } },
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Add delivery charge',
        returns: { arg: "result", type: "object" }
    });
    Finalpackagetransaction.beforeRemote("confirmPackagePaymentAmount", function (ctx, result, next) {
        let requestData = ctx.req.body
        let headerSchema = ['token','userid']
        validateheaders(ctx.req.headers,headerSchema,(err, res)=>{
            if(err){
                messages.keyMissCallBack.result['message'] = err;
                ctx.res.json(messages.keyMissCallBack)
            }
            else{
                let schema = ["user_id","user_package_cart_id","amount_to_be_paid"]
                verifyToken(ctx.req.headers.token, function (err, res) {
                    if (err) {
                        ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.token_expired))
                    } else {
                        validations(requestData, schema, (err, res) => {
                            if (err) {
                                ctx.res.json(response(false, messages.keyMissStatusCode, err))
                            }
                            else {
                                Finalpackagetransaction.findOne({
                                    where: {"users_id": requestData.user_id,"is_active": 1,"id" : requestData.user_package_cart_id}
                                })
                                .then((PackagecartResult) => {
                                    if (PackagecartResult){
                                        PackagecartResult.toJSON();
                                        if(PackagecartResult.grand_total == requestData.amount_to_be_paid){
                                            ctx.res.json({result:{status:true,statusCode:messages.successStatusCode,message: messages.successMsg.proceedToPay,amount_bo_be_paid: PackagecartResult.grand_total}})    
                                        }else{
                                            ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.paymentTotalErr))
                                        }
                                    }
                                    else
                                        ctx.res.json(response(false, messages.invalidStatusCode, messages.errorMsg.noCartFound))
                                })
                                .catch((cartErr) => {
                                    ctx.res.json(messages.systemErrorCallback)
                                })
                            }
                        })
                    }
                })
            }
        })
    });
    //* Conformation Package Payment Amount End *//
};
