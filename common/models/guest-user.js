'use strict';
let {invalidCredentialsCallback,invalidUsername,invalidPassword,tokenExpiredcallback,
    successCallback,keyMissCallBack, missingKeys,message,systemErrorCallback,errorMsg,
    resultCallbacks} = require('../messages')
let messages = require('../messages');
let app = require('../../server/server');
const { verifyToken, response, triggerEmail } = require("../utils")
const { validations } = require("../validations")

module.exports = function(Guestuser) {
    Guestuser.remoteMethod("guestUserDetails",{
        http:{path: '/guestUserDetails', verb:"get"},
        accepts:[
            { arg: 'requestData', type: 'object', required: true, http:{source: 'body'}},
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Guest user details',
        returns:{arg:"result", type:"object" }
    });

    Guestuser.beforeRemote('guestUserDetails', function(ctx, result, next){
        next()
    });

    Guestuser.guestUserDetails = function( requestData,req,res, cb){
        Guestuser.create({guest_email:req.body.email, guest_number:req.body.number})
        .then((result)=>{
            cb(null, {status: true, statusCode: messages.successStatusCode, message: messages.successMsg.userDetails })
        })
        .catch((error)=>{
            console.log(error,"------>")
            cb(null, resultCallbacks.systemErrorCallback)
        })
    }


    Guestuser.remoteMethod("guestUserorders",{
        http: { path: '/guestUserorders', verb: "post" },
        accepts: [
            { arg: 'req', type: 'object', required: true, http: { source: 'req' } },
            { arg: 'res', type: 'object', required: true, http: { source: 'res' } }
        ],
        description: 'Get User Order details in json format',
        returns: { arg: "result", type: "object" }
    });

    Guestuser.beforeRemote('guestUserorders', function(ctx, result, next){
        next()
    });

    Guestuser.guestUserorders = function( req,res, cb){
        console.log("requestData",req.body)
        let FinalTransactions = app.models.final_transcation;
      if(req.body.source == "invoice_number"){
          FinalTransactions.find({
              where:{
                  id:(req.body.input).trim()
              },
              order: 'created_at DESC',
              include: [{
                "relation": "ordered_items",
              }]
          })
          .then((result1)=>{
            if (result1.length > 0) {
                let result = { "status": true, "statusCode": messages.successStatusCode };
                result["message"] = `orders found`
                result["orders"] = result1
                cb(null, result)
            } else {
                let result = { "status": true, "statusCode": messages.successStatusCode };
                result["message"] = messages.errorMsg.noOrdersFound;
                result['orders'] = result1;
                cb(null, result)
            }
        })
        .catch((error)=>{
            console.log(error,"------>")
            let result = { "status": true, "statusCode": messages.successStatusCode };
                result["message"] = messages.errorMsg.noOrdersFound;
                result['orders'] = [];
                cb(null, result)
        })   
     }else if (req.body.source == "email"){
        //  let Useraddress = app.models.user_address;
        FinalTransactions.find({
            where: {
                email: req.body.input
            },
            order: 'created_at DESC',
            include: [{
                "relation": "ordered_items",
              }]})
        .then((resp)=>{
            if (resp.length > 0) {
                let result = { "status": true, "statusCode": messages.successStatusCode };
                result["message"] = `orders found`
                result["orders"] = resp
                cb(null, result)
            } else {
                let result = { "status": true, "statusCode": messages.successStatusCode };
                result["message"] = messages.errorMsg.noOrdersFound;
                result['orders'] = resp;
                cb(null, result)
            }
        })
        .catch(function (error) {
            console.log(error)
            cb(null, messages.systemErrorCallback)
        })
     }else{
        FinalTransactions.find({
            where: {
                number: req.body.input
            },
            order: 'created_at DESC',
            include: [{
                "relation": "ordered_items",
              }]})
        .then((resp)=>{
            if (resp.length > 0) {
                let result = { "status": true, "statusCode": messages.successStatusCode };
                result["message"] = `orders found`
                result["orders"] = resp
                cb(null, result)
            } else {
                let result = { "status": true, "statusCode": messages.successStatusCode };
                result["message"] = messages.errorMsg.noOrdersFound;
                result['orders'] = resp;
                cb(null, result)
            }
        })
        .catch(function (error) {
            console.log(error)
            cb(null, messages.systemErrorCallback)
        })
     }
    }
};
