const getProductsRegionwise = (productsdata,region)=>{
    productsdata.map(function(products_regions, index){ 
        products_regions.regions = products_regions.region.find((a)=>{if(a.name == region) return a })
        delete products_regions.region
    })
    return productsdata;
}
function taxation(cartResult) {
    try {
        var final_tax = [];
        var temp_tax_names = []
        cartResult.cart_list.map((cartItem) => {
            let taxes = cartItem.Products ? cartItem.Products.taxes : []
            for (var j in taxes) {
                if (temp_tax_names.indexOf(taxes[j].tax_name) > -1) {
                    final_tax.map((element) => {
                        if (element.tax_name == taxes[j].tax_name) {
                            element.tax_total += Number(cartItem.product_price) * Number(cartItem.ordered_quantity) * (Number(taxes[j].tax_percentage) / 100)
                            return element
                        }
                    })
                } else {
                    var tax = {};
                    tax.tax_id = taxes[j].tax_id
                    tax.tax_name = taxes[j].tax_name;
                    tax.tax_percentage = taxes[j].tax_percentage;
                    tax.tax_total = Number(cartItem.product_price) * Number(cartItem.ordered_quantity) * (Number(tax.tax_percentage) / 100)
                    final_tax.push(tax);
                    temp_tax_names.push(tax.tax_name)
                }
            }
        })
        return final_tax
    } catch (error) {

    }
    return []
}

  const elasticsearch = {
    // "url":"http://localhost:9200",
    "url":"https://search.vasu.life",
    // "port":"9200",
    "elasticsearchIndices": {
        "Products": {
          "index":"products",
          "type":"Products",
          "collectionName":"products"
        }
    }
  }
module.exports= {getProductsRegionwise, taxation,elasticsearch}